glObjEnv.decorator = {
  hOpen: window.XMLHttpRequest.prototype.open,
  hSend: window.XMLHttpRequest.prototype.send,
  fProcess: false,
  init: function(instance) {
    instance = instance || 1;
    if (instance >= 15) {
      
      return;
    }
    var self = this;
    if (typeof ga == "undefined" || typeof ga.getAll !== "function") {
      window.setTimeout(function() {
        self.init(++instance);
      }, 750);
      return;
    }
    window.XMLHttpRequest.prototype.open = function(
      method,
      url,
      async,
      user,
      password
    ) {
      if (url.match(/\/ws\/login/)) {
        self.fProcess = true;
      }
      return self.hOpen.apply(this, arguments);
    };
    window.XMLHttpRequest.prototype.send = function(data) {
      if (self.fProcess) {
        data += self.decorate();
      }
      self.fProcess = false;
      return self.hSend.apply(this, arguments);
    };
    console.log("decorator init done");
  },
  decorate: function() {
    var tracker = null;
    var linker = null;
    if (typeof ga == "undefined" || typeof ga.getAll !== "function") {
      return "";
    }
    var trackers = ga.getAll();
    if (!trackers.length) {
      return "";
    }
    tracker = trackers[0];
    if (
      typeof window.gaplugins !== "object" ||
      typeof window.gaplugins.Linker !== "function"
    ) {
      return "";
    }
    linker = new window.gaplugins.Linker(tracker);
    if (typeof linker.decorate !== "function") {
      return "";
    }
    var decorated = linker.decorate("https://www.abc.com/");
    if (typeof decorated !== "string") {
      return "";
    }
    decorated = decorated.replace("https://www.abc.com/?", "&");
    return decorated;
  },
  decorateMe: function() {
    if (typeof ga == "undefined" || typeof ga.getAll !== "function") {
      return;
    }
    glObjEnv.dlUrl += this.decorate();
  }
};
document.addEventListener("DOMContentLoaded", function(ev) {
  glObjEnv.decorator.init();
});
var ipxRegAPI = (function() {
  var ipxApi = {
    data: {},
    valid: [],
    errMsgs: {},
    validPreloads: [],
    presets: {},
    msg: {
      init: "%method: regAPI not yet initialized",
      unknown: "%method: property '%prop' unknown",
      invalid: "%method: value of property '%prop' invalid: >%val<",
      triggered: "%method: triggered",
      errValidateEmail: "Please enter a valid email",
      errValidatePwd: "Please enter a valid password",
      errValidation: "Validation failed. Please try again",
      sucValidation: "Validation successful",
      ownGender: "I'm m/w",
      gender: "Looking for m/w",
      size1: "Body size",
      dayOfBirth1: "Day",
      monthOfBirth1: "Month",
      yearOfBirth1: "Year",
      familyStatus1: "Family status",
      shape1: "Body shape",
      hairColor1: "Hair color",
      hairLength1: "Hair length",
      eyesColor1: "Eye color"
    },
    name: "",
    type: "",
    debug: false,
    fConsole: true,
    fLoading: false,
    fPrefilled: {},
    hooks: {},
    geoAPI: "/sites/findedeinabenteuer/modules/ipx_regapi/apiGeo.php",
    regUrl: "/sites/findedeinabenteuer/modules/ipx_regapi/reg.php",
    regScammer: "/reg/scmr/",
    idReg: 1000000000,
    cid: "",
    fChkEmailExists: false,
    logData: {},
    regResponse: {},
    fInvalidateParamsOnWrongUpdate: true,
    regLastStepBackendPinged: false,
    fGenderPrefilled: false,
    fOwnGenderPrefilled: false,
    fUseAboutUs: true,
    ccMap: {},
    validatePwdRule: function() {
      return /^[a-zA-Z0-9 !"#$%&\'()*+,-.\/:;<=>?@\[\]\^_`{|}~\\]{6,2000}$/;
    },
    fUseCookie: true,
    gEnv: {},
    init: function(cc, version) {
      this.write(this.msg.triggered + " (%stamp)", {
        method: "init",
        stamp: new Date().getTime()
      });
      if (this.isEmpty(this.gEnv) || !this.isObject(this.gEnv)) {
        this.write("%method: objEnv could not be loaded. Abort", {
          method: "init"
        });
        return false;
      }
      (this.data.countries = this.gEnv.countryMap),
        (this.data.trCountries = {}),
        (this.data.regions = []);
      this.data.userScamTracking = {
        emailCheckFailed: false,
        fastRegCheckFailed: false,
        greetingCheckFailed: false,
        nicknameCheckFailed: false
      };
      this.ccMap = this.gEnv.countryNeighbourMap;
      this.validPreloads = [
        "ownGender",
        "gender",
        "size1",
        "dayOfBirth1",
        "monthOfBirth1",
        "yearOfBirth1",
        "familyStatus1",
        "shape1",
        "hairColor1",
        "hairLength1",
        "eyesColor1",
        "aboutUs"
      ];
      if (!cc || !this.data.countries.hasOwnProperty(cc)) {
        this.write("%method: country '%cc' not defined. Abort", {
          method: "init",
          cc: cc
        });
        return false;
      }
      
      this.data.cc = this.gEnv.language;
      this.data.whitelabel = this.getWhitelabel();
      this.write("Whitelabel detection: " + this.data.whitelabel);
      if (this.hasClass("body", "nocookie")) {
        this.fUseCookie = false;
        this.write(
          "%method: reg data won't be written to cookie for this reg",
          { method: "init" }
        );
      } else {
        this.write("%method: reg data will be saved to cookie for this reg", {
          method: "init"
        });
      }
      this.data.idReg = this.gEnv.idReg;
      this.data.regId = this.gEnv.idReg;
      switch (version) {
        case "lp":
          this.valid = ["ownGender", "gender"];
          break;
        case "2step":
          if (this.showGeoLocationField()) {
            this.valid = [
              "ownGender",
              "gender",
              "dayOfBirth1",
              "monthOfBirth1",
              "yearOfBirth1",
              "familyStatus1",
              "size1",
              "shape1",
              "hairColor1",
              "hairLength1",
              "eyesColor1",
              "email",
              "password",
              "regId",
              "promoCode",
              "aboutUs",
              "cityLocation"
            ];
          } else {
            this.valid = [
              "ownGender",
              "gender",
              "dayOfBirth1",
              "monthOfBirth1",
              "yearOfBirth1",
              "familyStatus1",
              "size1",
              "shape1",
              "hairColor1",
              "hairLength1",
              "eyesColor1",
              "country",
              "region",
              "email",
              "password",
              "regId",
              "promoCode",
              "aboutUs"
            ];
          }
          if (this.showSubregions()) {
            this.valid.push("subRegion");
          }
          var now = new Date();
          this.birthlimits = {
            bottom: now.getFullYear() - 90,
            top: now.getFullYear() - 18
          };
          if (version == "2step" && this.showGeoLocationField()) {
            var queryData = {};
            queryData.cc = this.data.cc;
            queryData.whitelabel = this.data.whitelabel.toUpperCase();
            var service = this.regUrl + "?op=gdgl";
            this.write(this.msg.triggered, { method: "getGdgl" });
            this.talk(
              service,
              this._getGdgl,
              { method: "getGdgl", q: queryData },
              true
            );
          }
          break;
        default:
          this.write("%method: version '%vs' not defined. Abort", {
            method: "init",
            vs: version
          });
          break;
      }
      this.type = version;
      if (!this.defined(glErrMsgs) || !this.setErrMsgs(glErrMsgs)) {
        this.write("%method: error messages could not be loaded. Abort", {
          method: "init"
        });
        return false;
      }
      return true;
    },
    setGlobalEnvironment: function(gEnv) {
      if (this.isEmpty(gEnv) || !this.isObject(gEnv)) {
        return false;
      }
      this.gEnv = gEnv;
      return true;
    },
    _getGdgl: function(response, objParams) {
      var response = JSON.parse(response);
      if (!response.success) {
        this.write("%method: _getGdgl " + response.data, {
          method: "_getGdgl",
          param: objParams
        });
        return false;
      }
      ipxRegAPI.setDefaultUserLocation({
        success: true,
        data: {
          cityLocationId: response.data.cityLocationId,
          title: response.data.title
        }
      });
    },
    setDebug: function(fDebug) {
      this.debug = fDebug;
      var elConsole = document.getElementById("console");
      if (!elConsole) return;
      elConsole.style.display = fDebug ? "none" : "none";
    },
    setName: function(title) {
      this.name = title;
    },
    setErrMsgs: function(objMessages) {
      if (this.isObject(objMessages) && !this.isEmpty(objMessages)) {
        this.errMsgs = objMessages;
        return true;
      }
      return false;
    },
    getErrMsg: function(fieldName) {
      if (this.errMsgs.hasOwnProperty(fieldName)) {
        return this.errMsgs[fieldName];
      } else {
        return "";
      }
    },
    setPresets: function(objPresets) {
      this.write(this.msg.triggered, { method: "setPresets" });
      objPresets = objPresets || {};
      if (objPresets) objPresets.defaults = objPresets.defaults || {};
      if (!objPresets || !this.isObject(objPresets)) {
        this.write("%method: passed presets not valid. Abort", {
          method: "setPresets"
        });
        return false;
      }
      if (objPresets.defaults) {
        if (!this.isObject(objPresets)) objPresets.defaults = {};
      }
      this.presets = objPresets;
      this.write("%method: presets set successfully", { method: "setPresets" });
      window.objPrefills = window.objPrefills || this.loadStep() || null;
      this.prefillFromPassedData();
    },
    prefillFromPassedData: function(param) {
      var fResult = false;
      var param = param || null;
      var objPrefills = this.loadStep() || window.objPrefills || null;
      var map = {
        ownGender: "og",
        gender: "sg",
        dayOfBirth1: "dob",
        monthOfBirth1: "mob",
        yearOfBirth1: "yob",
        familyStatus1: "fs",
        country: "cc",
        region: "rg",
        subRegion: "srg",
        size1: "sz",
        shape1: "sp",
        hairColor1: "hc",
        hairLength1: "hl",
        eyesColor1: "ec",
        promoCode: "pc",
        aboutUs: "au",
        extStyles: "ex",
        email: "em",
        password: "pa",
        cityName: "cn",
        cityLocation: "cl"
      };
      var mappedItem = false;
      this.write(this.msg.triggered + (param ? " (%param)" : ""), {
        method: "prefillFromPassedData",
        param: param
      });
      if (!objPrefills) {
        this.write("%method: no valid data object passed", {
          method: "prefillFromPassedData"
        });
        return false;
      }
      if (param) {
        if (this.isEmpty(objPrefills[map[param]])) {
          this.write("%method: no valid data for '%param'. Abort", {
            method: "prefillFromPassedData",
            param: param
          });
          return false;
        }
        if (param == "country") {
          for (var key in this.data.countries) {
            if (this.data.countries[key] == objPrefills[map[param]]) {
              fResult = this.data.countries[key];
            }
          }
        } else if (param == "region") {
          if (this.setParam(param, objPrefills[map[param]])) {
            fResult = objPrefills[map[param]];
          }
        } else {
          fResult = this.setParam(param, objPrefills[map[param]]);
        }
        if (fResult) {
          this.write("%method: param '%param' set to '%val'", {
            method: "prefillFromPassedData",
            param: param,
            val: objPrefills[map[param]]
          });
          this.fPrefilled[param] = true;
        }
        objPrefills[map[param]] = "";
        return fResult;
      }
      this.write("%method: no explicit param given, looping through all", {
        method: "prefillFromPassedData"
      });
      for (var item in this.valid) {
        if (this.valid[item] == "country" || this.valid[item] == "region")
          continue;
        mappedItem = map[this.valid[item]];
        if (this.isEmpty(objPrefills[mappedItem])) continue;
        if (this.valid[item] == "email" || this.valid[item] == "password") {
          fResult = this.setParam(
            this.valid[item],
            ipxBase64.decode(objPrefills[mappedItem])
          );
        } else {
          fResult = this.setParam(this.valid[item], objPrefills[mappedItem]);
        }
        if (fResult) {
          this.presets.defaults[this.valid[item]] = objPrefills[mappedItem];
          this.fPrefilled[this.valid[item]] = true;
          if (this.valid[item] == "gender") this.fGenderPrefilled = true;
          if (this.valid[item] == "ownGender") this.fOwnGenderPrefilled = true;
        }
        objPrefills[mappedItem] = "";
      }
      this.write("%method: ran through all passed data", {
        method: "prefillFromPassedData"
      });
    },
    loadStep: function() {
      var tmpreg = this.readCookie("ipxreg");
      if (!tmpreg) return false;
      return JSON.parse(ipxBase64.decode(tmpreg));
    },
    saveFieldsToCookie: function(arrFields) {
      if (!this.fUseCookie) return;
      var fields = this.writeArray(arrFields);
      this.write(this.msg.triggered + " (%fields)", {
        method: "saveFieldsToCookie",
        fields: fields
      });
      var map = {
        ownGender: "og",
        gender: "sg",
        dayOfBirth1: "dob",
        monthOfBirth1: "mob",
        yearOfBirth1: "yob",
        familyStatus1: "fs",
        country: "cc",
        region: "rg",
        subRegion: "srg",
        size1: "sz",
        shape1: "sp",
        hairColor1: "hc",
        hairLength1: "hl",
        eyesColor1: "ec",
        promoCode: "pc",
        aboutUs: "au",
        extStyles: "ex",
        email: "em",
        password: "pa",
        cityName: "cn",
        cityLocation: "cl"
      };
      var objCookie = this.loadStep() || {};
      var fWrite = false;
      for (var field in arrFields) {
        var item = arrFields[field];
        if (!this.inHashKeys(item, map) || !this.data[item]) continue;
        if (item == "email" || item == "password") {
          objCookie[map[item]] = ipxBase64.encode(this.data[item]);
        } else {
          objCookie[map[item]] = this.data[item];
        }
        fWrite = true;
      }
      if (!fWrite) return;
      this.setCookie("ipxreg", ipxBase64.encode(JSON.stringify(objCookie)), 30);
      this.write("%method: cookie written successfully", {
        method: "saveFieldsToCookie"
      });
    },
    trBulk: function(objMsgs) {
      this.write(this.msg.triggered, { method: "trBulk" });
      for (var type in objMsgs) {
        if (!objMsgs.hasOwnProperty(type)) continue;
        if (this.isEmpty(this.msg[type])) {
          this.write("%method: '%type' does not exist, added (= '%val')", {
            method: "trBulk",
            type: type,
            val: objMsgs[type]
          });
        } else {
          this.write(
            "%method: '%type' exists, translated/overidden (= '%val')",
            { method: "trBulk", type: type, val: objMsgs[type] }
          );
        }
        this.msg[type] = objMsgs[type];
      }
    },
    tr: function(type, trType) {
      this.write(this.msg.triggered + "(%type)", { method: "tr", type: type });
      if (this.isObject(type)) {
        this.write("%method: object passed - use trBulk instead!", {
          method: "tr"
        });
        return this.trBulk(type);
      }
      trType = trType || false;
      if (!trType) {
        if (!this.isEmpty(this.msg[type])) return this.msg[type];
        else return type;
      } else this.msg[type] = trType;
      return true;
    },
    setVar: function(name, value) {
      this.write(this.msg.triggered + " (%name = %val)", {
        method: "setVar",
        name: name,
        val: value
      });
      switch (name) {
        case "regid":
          if (value > 10000000) {
            this.setParam("regId", value);
          }
          break;
        case "promocode":
          if (this.setParam("promoCode", value))
            this.write("%method: promoCode = " + value, { method: "setVar" });
          break;
        case "validate_email_backend":
          this.fChkEmailExists = value;
          this.write("%method: fChkEmailExists = " + value, {
            method: "setVar"
          });
          break;
        case "aboutus":
          this.fUseAboutUs = value;
          if (value == false) {
            var idx = this.valid.indexOf("aboutUs");
            if (idx > -1) {
              this.valid.splice(idx, 1);
            }
          }
          break;
      }
    },
    activateGtcOptIn: function(branch) {
      branch = branch || false;
      if (!branch) {
        return false;
      }
      for (var i = 0; i < this.gEnv.gtcOptInBranches.length; i++) {
        var tmpBranch = this.gEnv.gtcOptInBranches[i];
        if (branch == tmpBranch || branch == tmpBranch.toLowerCase()) {
          return true;
        }
      }
      return false;
    },
    xSetParams: function(objData) {
      this.setParams(objData);
    },
    setParams: function(objData) {
      this.write(this.msg.triggered, { method: "setParams" });
      if (this.valid.length < 1) {
        this.write(this.msg.init, { method: "setParams" });
        return false;
      }
      for (var prop in objData) {
        if (!objData.hasOwnProperty(prop)) {
          this.write(this.msg.unknown, { method: "setParams", prop: prop });
          continue;
        }
        if (this.setParam(prop, objData[prop]) == false) {
          this.write("%method: '%prop' cannot be set. Continue", {
            method: "setParams",
            prop: prop
          });
          continue;
        }
      }
      return true;
    },
    xSetParam: function(param, value) {
      this.setParam(param, value);
    },
    setParam: function(param, value) {
      this.write(this.msg.triggered + " (%param = %val)", {
        method: "setParam",
        param: param,
        val: value
      });
      if (this.valid.length < 1) {
        this.write(this.msg.init, { method: "setParam" });
        return false;
      }
      if (!this.isValid(param)) {
        this.write(this.msg.unknown, { method: "setParam", prop: param });
        return false;
      }
      if (param == "email") {
        value = value.toLowerCase();
      }
      if (this.validateParam(param, value) == false) {
        return false;
      }
      if (param == "country") {
        if (!parseInt(value)) value = this.data.countries[value];
        this.data.regions.length = 0;
      }
      this.data[param] = value;
      this.write("%method: param '%param' (%val) successfully set", {
        method: "setParam",
        param: param,
        val: value
      });
      this.triggerHooks("postSetParam", { status: "success", name: param });
      return true;
    },
    issetParam: function(param) {
      if (param == "prefilldata") {
        var objPrefills = window.objPrefills || null;
        return objPrefills ? true : false;
      }
      return this.fPrefilled[param];
    },
    getParam: function(param) {
      this.write(this.msg.triggered + " (%param)", {
        method: "getParam",
        param: param
      });
      if (!this.data.hasOwnProperty(param)) {
        this.write(this.msg.unknown, { method: "getParam", prop: param });
        return false;
      }
      if (!this.data[param]) return false;
      return this.data[param];
    },
    testParam: function(param) {
      this.write(this.msg.triggered, { method: "testParam" });
      if (this.isEmpty(this.data[param])) {
        this.write("%method: param '%prop' not among valid parameters", {
          method: "testParam",
          prop: param
        });
        return false;
      }
      return true;
    },
    testParams: function(arrParams) {
      var myParams = this.writeArray(arrParams);
      this.write(this.msg.triggered + " (%myparams)", {
        method: "testParams",
        myparams: myParams
      });
      var res = "";
      for (var i = 0; i < arrParams.length; i++) {
        if (!this.inArray(arrParams[i], this.valid)) {
          continue;
        }
        if (this.isEmpty(this.data[arrParams[i]])) {
          res = this.printf(this.msg.invalid, {
            method: "testParams",
            prop: this.tr(arrParams[i]),
            val: this.data[arrParams[i]]
          });
          if (!this.data.passvalidate && this.data.passvalidate != undefined) {
            this.triggerHooks("postValidateParam", {
              name: arrParams[i],
              status: "error",
              result: res,
              errorMsg: glErrMsgs["passwordInvalid"]
            });
          } else {
            this.triggerHooks("postValidateParam", {
              name: arrParams[i],
              status: "error",
              result: res,
              errorMsg: glErrMsgs[arrParams[i].replace(/[^a-z]/gi, "")]
            });
          }
          return false;
        }
      }
      this.saveFieldsToCookie(arrParams);
      return true;
    },
    validateParams: function(objParams) {
      this.write(this.msg.triggered, { method: "validateParams" });
      for (var prop in objParams) {
        if (!objParams.hasOwnProperty(prop)) continue;
        if (!this.inArray(prop, this.valid)) {
          this.write(
            "%method: we don't need to validate param '%prop'. Skipping",
            { method: "validateParams", prop: prop }
          );
          continue;
        }
        if (this.validateParam(prop, objParams[prop]) == false) {
          this.write(this.msg.invalid, {
            method: "validateParams",
            prop: prop,
            val: objParams[prop]
          });
          return false;
        }
      }
      return true;
    },
    xValidateParam: function(name, value) {
      if (!this.validateParam(name, value)) {
      } else {
      }
    },
    validateParam: function(name, value) {
      var errMsg = glErrMsgs[name];
      if (name == "password" && value.length > 0 && value.length < 6) {
        this.data.passvalidate = false;
      } else if (
        (name == "password" && value.length > 5) ||
        (name == "password" && value.length == 0)
      ) {
        this.data.passvalidate = true;
      }
      this.write(this.msg.triggered + " (%name = %val)", {
        method: "validateParam",
        name: name,
        val: value
      });
      if (value == "" || value == null || value == "undefined") {
        this.write("%method: value is empty. Abort", {
          method: "validateParam"
        });
        if (this.fInvalidateParamsOnWrongUpdate) this.invalidateParam(name);
        this.triggerHooks("postValidateParam", {
          name: name,
          status: "error",
          result: this.printf("Field '%name' has empty value", { name: name }),
          errorMsg: errMsg
        });
        return false;
      }
      var re = null;
      var res = "";
      switch (name) {
        case "gender":
          if (value == 1 || value == 2) return true;
          break;
        case "ownGender":
          if (value == 1 || value == 2) return true;
          break;
        case "country":
          if (parseInt(value)) {
            for (var prop in this.data.countries) {
              if (!this.data.countries.hasOwnProperty(prop)) continue;
              if (this.data.countries[prop] == value) return true;
            }
          } else {
            if (this.data.countries.hasOwnProperty(value)) return true;
          }
          break;
        case "region":
          re = /^[0-9]{1,6}$/;
          if (!re.test(value)) {
            this.write(
              "%method: region value not a valid 'digits only' value. Abort",
              { method: "validateParam" }
            );
            res = "Region value not a valid 'digits only' value";
            break;
          }
          if (!this.data.regions.length) {
            this.write("%method: no regions found (yet). Abort", {
              method: "validateParam"
            });
            res = "No regions found";
            break;
          }
          for (var i = 0; i < this.data.regions.length; i++) {
            if (this.data.regions[i].id == value) return true;
          }
          this.write("%method: region not found (#%val).", {
            method: "validateParam",
            val: value
          });
          res = "Region not found";
          break;
        case "subRegion":
          re = /^[a-z0-9]{1,24}$/;
          if (!re.test(value)) {
            this.write(
              "%method: subRegion value not a valid 'digits and letters only' value. Abort",
              { method: "validateParam" }
            );
            res = "Subregion value not a valid 'digits only' value";
            break;
          }
          if (!this.data.subregions) {
            this.write("%method: no subregions found (yet). Abort", {
              method: "validateParam"
            });
            res = "No subregions found";
            break;
          }
          for (
            var i = 0;
            i < this.data.subregions[this.data.region].length;
            i++
          ) {
            if (this.data.subregions[this.data.region][i].id == value)
              return true;
          }
          this.write("%method: subRegion not found (#%val).", {
            method: "validateParam",
            val: value
          });
          res = "Subregion not found";
          break;
        case "dayOfBirth1":
          if (this.isNumeric(value) && value >= 1 && value <= 31) return true;
          break;
        case "monthOfBirth1":
          if (this.isNumeric(value) && value >= 1 && value <= 12) return true;
          break;
        case "yearOfBirth1":
          if (!this.isObject(this.birthlimits)) {
            this.write("%method: limits are not set for prop '%prop'", {
              method: "validateParam",
              prop: name
            });
          } else {
            if (
              this.isNumeric(value) &&
              value >= this.birthlimits.bottom &&
              value <= this.birthlimits.top
            ) {
              return true;
            }
          }
          break;
        case "birthday":
          res = this.validateBirthday(value);
          if (typeof res === "string") {
            errMsg = res;
          } else {
            return true;
          }
          break;
        case "familyStatus1":
          if (this.isNumeric(value) && value >= 1 && value <= 3) return true;
          break;
        case "size1":
          if (this.isNumeric(value) && value >= 1 && value <= 74) return true;
          break;
        case "shape1":
          if (this.isNumeric(value) && value >= 1 && value <= 3) return true;
          break;
        case "hairColor1":
          if (this.isNumeric(value) && value >= 1 && value <= 7) return true;
          break;
        case "hairLength1":
          if (this.isNumeric(value) && value >= 1 && value <= 4) return true;
          break;
        case "eyesColor1":
          if (this.isNumeric(value) && value >= 1 && value <= 9) return true;
          break;
        case "aboutUs":
          re = /^[a-z]+$/;
          if (re.test(value)) return true;
          break;
        case "email":
          if (this.validateEmail(value)) return true;
          break;
        case "password":
          re = this.validatePwdRule();
          if (re.test(value)) return true;
          if (value != "") {
            res = "Password invalid, please use alphanumeric chars only";
            errMsg = glErrMsgs["passwordInvalid"];
          }
          break;
        case "promoCode":
          re = /^[a-zA-Z\-0-9]{2,10}$/;
          if (this.isEmpty(value) || re.test(value)) return true;
          break;
        case "cityLocation":
          re = /^[a-z0-9]{24}$/;
          if (this.isEmpty(value) || re.test(value)) return true;
          break;
        case "regId":
          re = /^[0-9]{5,10}$/;
          if (this.isEmpty(value) || re.test(value)) return true;
          break;
        default:
          this.write(this.msg.unknown, { method: "validateParam", prop: name });
          break;
      }
      this.write(this.msg.invalid, {
        method: "validateParam",
        prop: name,
        val: value
      });
      if (this.fInvalidateParamsOnWrongUpdate) this.invalidateParam(name);
      if (res == "")
        res = this.printf(this.msg.invalid, {
          method: "validateParam",
          prop: name,
          val: value
        });
      this.triggerHooks("postValidateParam", {
        name: name,
        status: "error",
        result: res,
        errorMsg: errMsg
      });
      return false;
    },
    xSetBirthday: function(birthday) {
      var validated = this.validateBirthday(birthday);
      if (validated == true) {
        this.write("%method: birthday successfully set", {
          method: "xSetBirthday"
        });
        this.triggerHooks("postSetParam", {
          name: "birthday",
          status: "success"
        });
      } else {
        this.triggerHooks("postValidateParam", {
          name: "birthday",
          status: "error",
          result: validated
        });
      }
    },
    geoLocationMinChars: function() {
      var min_one_char_countries = ["ja"];
      if (this.inArray(this.data.cc, min_one_char_countries)) {
        return 1;
      } else {
        return 3;
      }
    },
    showGeoLocationField: function() {
      if (this.inArray(this.data.cc, this.gEnv.geoLocationFieldCountries)) {
        return true;
      } else {
        return false;
      }
    },
    setDefaultUserLocation: function(locationData) {
      this.write(this.msg.triggered, { method: "setDefaultUserLocation" });
      this.triggerHooks("defaultUserLocation", {
        name: "defaultUserLocationObject",
        status: "success",
        result: locationData
      });
      return false;
    },
    updateUserLocation: function(inputTxt) {
      this.write(this.msg.triggered, { method: "updateUserLocation" });
      var re = /^[A-Za-z0-9\u002D\uFE63\uFF0D\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0370-\u0374\u0376\u0377\u037A-\u037D\u037F\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u052F\u0531-\u0556\u0559\u0561-\u0587\u05D0-\u05EA\u05F0-\u05F2\u0620-\u064A\u066E\u066F\u0671-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0710\u0712-\u072F\u074D-\u07A5\u07B1\u07CA-\u07EA\u07F4\u07F5\u07FA\u0800-\u0815\u081A\u0824\u0828\u0840-\u0858\u08A0-\u08B4\u0904-\u0939\u093D\u0950\u0958-\u0961\u0971-\u0980\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BD\u09CE\u09DC\u09DD\u09DF-\u09E1\u09F0\u09F1\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A59-\u0A5C\u0A5E\u0A72-\u0A74\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABD\u0AD0\u0AE0\u0AE1\u0AF9\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3D\u0B5C\u0B5D\u0B5F-\u0B61\u0B71\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BD0\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C39\u0C3D\u0C58-\u0C5A\u0C60\u0C61\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBD\u0CDE\u0CE0\u0CE1\u0CF1\u0CF2\u0D05-\u0D0C\u0D0E-\u0D10\u0D12-\u0D3A\u0D3D\u0D4E\u0D5F-\u0D61\u0D7A-\u0D7F\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0E01-\u0E30\u0E32\u0E33\u0E40-\u0E46\u0E81\u0E82\u0E84\u0E87\u0E88\u0E8A\u0E8D\u0E94-\u0E97\u0E99-\u0E9F\u0EA1-\u0EA3\u0EA5\u0EA7\u0EAA\u0EAB\u0EAD-\u0EB0\u0EB2\u0EB3\u0EBD\u0EC0-\u0EC4\u0EC6\u0EDC-\u0EDF\u0F00\u0F40-\u0F47\u0F49-\u0F6C\u0F88-\u0F8C\u1000-\u102A\u103F\u1050-\u1055\u105A-\u105D\u1061\u1065\u1066\u106E-\u1070\u1075-\u1081\u108E\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u1380-\u138F\u13A0-\u13F5\u13F8-\u13FD\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u16F1-\u16F8\u1700-\u170C\u170E-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176C\u176E-\u1770\u1780-\u17B3\u17D7\u17DC\u1820-\u1877\u1880-\u18A8\u18AA\u18B0-\u18F5\u1900-\u191E\u1950-\u196D\u1970-\u1974\u1980-\u19AB\u19B0-\u19C9\u1A00-\u1A16\u1A20-\u1A54\u1AA7\u1B05-\u1B33\u1B45-\u1B4B\u1B83-\u1BA0\u1BAE\u1BAF\u1BBA-\u1BE5\u1C00-\u1C23\u1C4D-\u1C4F\u1C5A-\u1C7D\u1CE9-\u1CEC\u1CEE-\u1CF1\u1CF5\u1CF6\u1D00-\u1DBF\u1E00-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u2071\u207F\u2090-\u209C\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2183\u2184\u2C00-\u2C2E\u2C30-\u2C5E\u2C60-\u2CE4\u2CEB-\u2CEE\u2CF2\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D80-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2E2F\u3005\u3006\u3031-\u3035\u303B\u303C\u3041-\u3096\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312D\u3131-\u318E\u31A0-\u31BA\u31F0-\u31FF\u3400-\u4DB5\u4E00-\u9FD5\uA000-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA61F\uA62A\uA62B\uA640-\uA66E\uA67F-\uA69D\uA6A0-\uA6E5\uA717-\uA71F\uA722-\uA788\uA78B-\uA7AD\uA7B0-\uA7B7\uA7F7-\uA801\uA803-\uA805\uA807-\uA80A\uA80C-\uA822\uA840-\uA873\uA882-\uA8B3\uA8F2-\uA8F7\uA8FB\uA8FD\uA90A-\uA925\uA930-\uA946\uA960-\uA97C\uA984-\uA9B2\uA9CF\uA9E0-\uA9E4\uA9E6-\uA9EF\uA9FA-\uA9FE\uAA00-\uAA28\uAA40-\uAA42\uAA44-\uAA4B\uAA60-\uAA76\uAA7A\uAA7E-\uAAAF\uAAB1\uAAB5\uAAB6\uAAB9-\uAABD\uAAC0\uAAC2\uAADB-\uAADD\uAAE0-\uAAEA\uAAF2-\uAAF4\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uAB30-\uAB5A\uAB5C-\uAB65\uAB70-\uABE2\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D\uFB1F-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE70-\uFE74\uFE76-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC\s]+$/;
      if (re.test(inputTxt)) {
        var queryData = {};
        queryData.cc = this.data.cc;
        queryData.whitelabel = this.data.whitelabel.toUpperCase();
        queryData.queryTerm = inputTxt;
        var service = this.regUrl + "?op=gsgl";
        this.write(this.msg.triggered, { method: "getGsgl" });
        this.talk(
          service,
          this._updateUserLocation,
          { method: "updateUserLocation", q: queryData },
          true
        );
      } else {
        this.triggerHooks("queryUserLocation", {
          name: "queryUserLocationObject",
          status: response.success,
          result: response.data
        });
      }
    },
    _updateUserLocation: function(response, objParams) {
      var response = JSON.parse(response);
      if (response.data.length > 0) {
        this.triggerHooks("queryUserLocation", {
          name: "queryUserLocationObject",
          status: response.success,
          result: response.data
        });
      } else {
        this.triggerHooks("queryUserLocation", {
          name: "queryUserLocationObject",
          status: false,
          result: response.data
        });
      }
    },
    validateBirthday: function(birthday) {
      var bdayParts = [];
      var objBday = {};
      var re = null;
      this.write(this.msg.triggered, { method: "validateBirthday" });
      re = /^\d{4,4}-\d\d?-\d\d?$/;
      if (this.isEmpty(birthday)) {
        this.write("%method: birthday_empty (%param)", {
          method: "validateBirthday",
          param: birthday
        });
        return this.getErrMsg("wrongDate");
      }
      if (!re.test(birthday)) {
        this.write("%method: birthday_invalid_format (%param)", {
          method: "validateBirthday",
          param: birthday
        });
        return this.getErrMsg("wrongDate");
      }
      var optimizedBirthday = birthday.replace(/-/g, "/");
      var myBirthday = new Date(optimizedBirthday);
      var currentDate = new Date().toJSON().slice(0, 10) + " 01:00:00";
      var myAge = ~~((Date.now(currentDate) - myBirthday) / 31557600000);
      bdayParts = birthday.split("-");
      objBday = { year: bdayParts[0], month: bdayParts[1], day: bdayParts[2] };
      this.data["dayOfBirth1"] = objBday.day;
      this.data["monthOfBirth1"] = objBday.month;
      this.data["yearOfBirth1"] = objBday.year;
      var valid = true;
      if (
        objBday.month == 4 ||
        objBday.month == 6 ||
        objBday.month == 9 ||
        objBday.month == 11
      ) {
        if (objBday.day > 30) {
          valid = false;
        }
      }
      if (objBday.month == 2) {
        if (objBday.year % 4 == 0) {
          if (objBday.day > 29) {
            valid = false;
          }
        } else if (objBday.day > 28) {
          valid = false;
        }
      }
      if (myAge >= 91 || !valid) {
        this.write("%method: birthday_invalid (%param)", {
          method: "validateBirthday",
          param: birthday
        });
        return this.getErrMsg("wrongDate");
      } else if (myAge < 18) {
        this.write("%method: birthday_invalid_underage (%param)", {
          method: "validateBirthday",
          param: birthday
        });
        return this.getErrMsg("underAge");
      }
      this.data["birthday"] = birthday;
      return true;
    },
    validateEmail: function(email) {
      var service = "";
      service = this.regScammer + "email/" + email;
      this.write(this.msg.triggered, { method: "validateEmail" });
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!re.test(email) || email.length > 254) return false;
      if (this.fChkEmailExists) {
        service = this.regUrl + "?op=check_email&email=" + email;
        this.write("%method: ajax call to '%ws' triggered", {
          method: "validateEmail",
          ws: service
        });
        if (
          !this.talk(service, this._validateEmail, { method: "validateEmail" })
        )
          return false;
      }
      return true;
    },
    _validateEmail: function(response, objParams) {
      var res = response.split(",");
      if (res[1] == "y") {
        this.data.userScamTracking.emailCheckFailed = true;
      } else {
        this.data.userScamTracking.emailCheckFailed = false;
      }
      this.write(
        "%method: result (email exists?) = " + (res[0] == 1 ? "yes" : "no"),
        { method: "_validateEmail" }
      );
      if (res[0] == 1) {
        this.triggerHooks("postValidateEmailExists", {
          status: "error",
          result: res[0],
          errorMsg: glErrMsgs["emailExists"]
        });
        this.invalidateParam("email");
      } else {
        this.triggerHooks("postValidateEmailOk", {
          status: "success",
          result: res[0]
        });
      }
    },
    _checkScammer: function(response, objParams) {
      if (objParams.name == "name") {
        switch (response) {
          case "F":
            if (this.data.ownGender == 2) {
              this.data.userScamTracking.nicknameCheckFailed = true;
            } else {
              this.data.userScamTracking.nicknameCheckFailed = false;
            }
            break;
          case "M":
            if (this.data.ownGender == 1) {
              this.data.userScamTracking.nicknameCheckFailed = true;
            } else {
              this.data.userScamTracking.nicknameCheckFailed = false;
            }
            break;
        }
      }
    },
    generateSize1Values: function() {
      var setOfInchesLangs = ["en-CA", "fr-CA", "en-US", "en-UK", "en-uk"];
      var szValues = [];
      if (ipxRegAPI.inArray(ipxRegAPI.data.cc, setOfInchesLangs)) {
        szValues = [
          { code: 1, id: "size1_1", translation: "<4' 7\"" },
          { code: 2, id: "size1_1", translation: "4' 7\"" },
          { code: 5, id: "size1_1", translation: "4' 8\"" },
          { code: 7, id: "size1_1", translation: "4' 9\"" },
          { code: 9, id: "size1_1", translation: "4' 10\"" },
          { code: 12, id: "size1_1", translation: "4' 11\"" },
          { code: 14, id: "size1_1", translation: "5' 0\"" },
          { code: 17, id: "size1_1", translation: "5' 1\"" },
          { code: 19, id: "size1_1", translation: "5' 2\"" },
          { code: 22, id: "size1_1", translation: "5' 3\"" },
          { code: 25, id: "size1_1", translation: "5' 4\"" },
          { code: 27, id: "size1_1", translation: "5' 5\"" },
          { code: 29, id: "size1_1", translation: "5' 6\"" },
          { code: 32, id: "size1_1", translation: "5' 7\"" },
          { code: 35, id: "size1_1", translation: "5' 8\"" },
          { code: 37, id: "size1_1", translation: "5' 9\"" },
          { code: 39, id: "size1_1", translation: "5' 10\"" },
          { code: 42, id: "size1_1", translation: "5' 11\"" },
          { code: 44, id: "size1_1", translation: "6' 0\"" },
          { code: 47, id: "size1_1", translation: "6' 1\"" },
          { code: 49, id: "size1_1", translation: "6' 2\"" },
          { code: 52, id: "size1_1", translation: "6' 3\"" },
          { code: 55, id: "size1_1", translation: "6' 4\"" },
          { code: 57, id: "size1_1", translation: "6' 5\"" },
          { code: 59, id: "size1_1", translation: "6' 6\"" },
          { code: 62, id: "size1_1", translation: "6' 7\"" },
          { code: 65, id: "size1_1", translation: "6' 8\"" },
          { code: 74, id: "size1_1", translation: ">6' 8\"" }
        ];
      } else {
        var shim = 0;
        for (var i = 1; i < 75; i++) {
          var tr = 138 + i - shim + " cm";
          switch (i) {
            case 1:
              tr = "<140 cm";
              break;
            case 66:
              shim = 1;
              continue;
            case 74:
              tr = ">210 cm";
              break;
          }
          szValues.push({ code: i, id: "size1_" + i, translation: tr });
        }
      }
      return szValues;
    },
    checkPwdOnTyping: function(passwordVal, emailVal) {
      var response = {
        minChar: false,
        charVal: false,
        notEmail: false,
        empty: true
      };
      if (passwordVal.length > 0) {
        response.empty = false;
        if (passwordVal.length > 5) {
          response.minChar = true;
        } else {
          response.minChar = false;
        }
        re = this.validatePwdRule();
        if (
          passwordVal.match(/((?=.*\d)(?=.*[a-z])(?=.*[\W]).{5,1994})/g) &&
          re.test(passwordVal)
        ) {
          response.charVal = true;
        } else {
          response.charVal = false;
        }
        if (
          response.minChar &&
          emailVal.length > 5 &&
          passwordVal == emailVal
        ) {
          response.notEmail = false;
        } else {
          response.notEmail = true;
        }
        console.log(response);
      } else {
        response.empty = true;
        response.minChar = false;
        response.charVal = false;
        response.notEmail = false;
      }
      return response;
    },
    invalidateParam: function(param) {
      this.write(this.msg.triggered + " (param = %param)", {
        method: "invalidateParam",
        param: param
      });
      if (this.isEmpty(this.data[param])) {
        this.write("%method: param '%param' not set yet", {
          method: "invalidateParam",
          param: param
        });
        return false;
      } else this.data[param] = null;
      this.write("%method: param '%param' invalidated", {
        method: "invalidateParam",
        param: param
      });
      return true;
    },
    xSubmitReg: function(regForm, objData) {
      this.submitReg(regForm, objData);
    },
    submitReg: function(regForm, objData) {
      this.write(this.msg.triggered, { method: "submitReg" });
      objData = objData || this.cloneObj(this.data);
      regForm = regForm || null;
      if (this.valid.length < 1) {
        this.write(this.msg.init, { method: "submitReg" });
        return false;
      }
      if (this.isEmpty(objData.regId)) {
        this.data.regId = this.idReg;
        objData.regId = this.idReg;
      }
      if (!this.isEmpty(this.gEnv.gtcVersion)) {
        objData.gtcVersion = this.gEnv.gtcVersion;
      }
      if (!this.isEmpty(this.gEnv.dataProtectionVersion)) {
        objData.dataProtectionVersion = this.gEnv.dataProtectionVersion;
      }
      if (!this.isEmpty(this.logData)) {
        objData.log = this.logData;
      }
      var fOldChkEmailExists = this.fChkEmailExists;
      this.fChkEmailExists = false;
      for (var i in this.valid) {
        if (this.isEmpty(objData[this.valid[i]])) {
          this.write(
            "%method: property '%prop' missing, try to take from internal data hive",
            { method: "submitReg", prop: this.valid[i] }
          );
          if (!this.isEmpty(this.data[this.valid[i]])) {
            this.write("%method: success", { method: "submitReg" });
            if (!this.validateParam(this.valid[i], this.data[this.valid[i]])) {
              this.triggerHooks("postSubmit", {
                status: "error",
                result: "Given parameter invalid: " + this.valid[i]
              });
              return false;
            }
            objData[this.valid[i]] = this.data[this.valid[i]];
            continue;
          }
          this.write(
            "%method: property '%prop' missing, try to take from passed form tag",
            { method: "submitReg", prop: this.valid[i] }
          );
          if (regForm && this.parseFormData(regForm, this.valid[i])) {
            if (this.isEmpty(this.data[this.valid[i]])) {
              this.write("%method: property '%prop' empty. Abort", {
                method: "submitReg",
                prop: this.valid[i]
              });
              this.triggerHooks("postSubmit", {
                status: "error",
                result: "Parameter empty: " + this.valid[i]
              });
              return false;
            }
            this.write("%method: success", { method: "submitReg" });
            if (!this.validateParam(this.valid[i], this.data[this.valid[i]])) {
              this.triggerHooks("postSubmit", {
                status: "error",
                result: "Given parameter invalid: " + this.valid[i]
              });
              return false;
            }
            objData[this.valid[i]] = this.data[this.valid[i]];
            continue;
          }
          if (
            this.inArray(this.valid[i], [
              "promoCode",
              "cid",
              "affiliateID",
              "visitorID"
            ])
          ) {
            continue;
          }
          this.write("%method: property '%prop' missing", {
            method: "submitReg",
            prop: this.valid[i]
          });
          this.triggerHooks("postSubmit", {
            status: "error",
            result: "Parameter missing: " + this.valid[i]
          });
          return false;
        } else {
          if (!this.validateParam(this.valid[i], objData[this.valid[i]])) {
            this.triggerHooks("postSubmit", {
              status: "error",
              result: "Given parameter invalid: " + this.valid[i]
            });
            return false;
          }
        }
      }
      objData.cc = this.data.cc;
      if (objData.countries) delete objData.countries;
      if (objData.trCountries) delete objData.trCountries;
      if (objData.regions) delete objData.regions;
      if (objData.subregions) delete objData.subregions;
      objData.googleAnalyticsLinker = this.gEnv.decorator
        .decorate()
        .replace(/&_ga=([^&]+).*$/, "$1");
      service = this.regUrl + "?op=reg";
      objData.method = "submitReg";
      this.write("%method: ajax call to '%ws' triggered", {
        method: "submitReg",
        ws: service
      });
      this.talk(service, this._submitReg, objData, true);
      this.fChkEmailExists = fOldChkEmailExists;
      return true;
    },
    _submitReg: function(response, objParams) {
      this.write(this.msg.triggered, { method: "_submitReg" });
      this.write(JSON.stringify(response));
      this.regResponse = JSON.parse(response);
      if (this.regResponse.registrationStatus == "SUCCESS") {
        if (typeof HybridAppInterface !== "undefined") {
          if (!this.regResponse.userId) {
            this.regResponse.userId = "-";
          }
          var objJson = JSON.stringify({
            event: "REGISTRATION",
            userId: this.regResponse.userId
          });
          HybridAppInterface.emit(objJson);
          this.write("%method: HybridAppInterface recognized: tracking reg", {
            method: "_submitReg"
          });
        } else {
          this.write(
            "%method: HybridAppInterface not defined: APP tracking not executed",
            { method: "_submitReg" }
          );
        }
      } else {
        this.triggerHooks("postSubmitReturn", {
          status: "error",
          result: this.regResponse,
          errorMsg: glErrMsgs["serviceUnavailable"]
        });
      }
      window.setTimeout(function() {
        jQuery(".gotoApp,.gotoproduct,#success_slide .btn-next").click();
      }, 90 * 1000);
      this.gEnv.decorator.decorateMe();
      this.triggerHooks("postSubmitReturn", {
        status: "success",
        result: this.regResponse
      });
      return true;
    },
    grabPageFromUrl: function() {
      var path = window.location.pathname;
      var page = path.split("/").pop();
      var len =
        page.lastIndexOf(".") != -1 ? page.lastIndexOf(".") : page.length;
      return page.slice(0, len);
    },
    grabParamFromUrl: function(param) {
      this.write(
        "%method: trying to grab param '%param' from window.location",
        { method: "grabParamFromUrl", param: param }
      );
      var chkParams =
        param + "|" + param.toLowerCase() + "|" + param.toUpperCase();
      var regex = new RegExp("[\\?&](?:" + chkParams + ")=([^&#]*)");
      var result = regex.exec(window.location.href);
      if (result == null) {
        this.write("%method: '%param' not found in window.location", {
          method: "grabParamFromUrl",
          param: param
        });
        return "";
      }
      this.write(
        "%method: '%param' found in and taken from window.location (val = %val)",
        { method: "grabParamFromUrl", param: param, val: result[1] }
      );
      return result[1];
    },
    grabParamFromCookie: function(name) {
      this.write("%method: trying to grab param '%param' from cookies", {
        method: "grabParamFromCookie",
        param: name
      });
      return this.readCookie(name);
    },
    parseFormData: function(regForm, param) {
      this.write(this.msg.triggered, { method: "parseFormData" });
      param = param || null;
      var elTag = document.getElementById(regForm);
      if (!elTag) {
        this.write("%method: tag '%form' not defined in document", {
          method: "parseFormData",
          form: regForm
        });
        return false;
      }
      if (!param) {
        for (var i in this.valid) {
          var prop = this.valid[i];
          if (!elTag[prop]) {
            this.write("%method: param '%param' not found. Continue", {
              method: "parseFormData",
              param: prop
            });
            continue;
          }
          var val = this.getFormValue(elTag[prop]);
          if (!val) {
            this.write("%method: value for param '%param' empty. Continue", {
              method: "parseFormData",
              param: prop
            });
            continue;
          }
          if (!this.validateParam(prop, val)) continue;
          this.write(
            "%method: value for param '%param' found and set. Continue",
            { method: "parseFormData", param: prop }
          );
          this.data[prop] = val;
        }
      } else {
        if (!elTag[param]) {
          this.write("%method: param '%param' not found", {
            method: "parseFormData",
            param: param
          });
          return false;
        }
        var val = this.getFormValue(elTag[param]);
        if (!val) {
          this.write("%method: value for param '%param' empty", {
            method: "parseFormData",
            param: param
          });
          return false;
        }
        this.data[param] = elTag[param].value;
      }
      return true;
    },
    tracking: function(objTracking) {
      this.write(this.msg.triggered + " (event = %ev)", {
        method: "tracking",
        ev: objTracking.event
      });
      var regData = "";
      if (objTracking.event == "lp") {
        if (typeof iframe != "undefined" && this.isFunction(iframe)) {
          iframe();
        } else {
          this.write("%method: tracking script does not exist in page", {
            method: "tracking"
          });
          return false;
        }
        this.write("%method: updated successfully", { method: "tracking" });
        return true;
      }
      objTracking.step = objTracking.step || 0;
      objTracking.total = objTracking.total || 0;
      if (
        !objTracking.event ||
        (objTracking.event != "regFinish" &&
          (!objTracking.step || !objTracking.total))
      ) {
        this.write("%method: reg steps are missing, needed. Abort.", {
          method: "tracking"
        });
        return false;
      }
      objTracking.title =
        "reg_" +
        (this.notEmpty(this.name) ? this.name : this.grabPageFromUrl());
      objTracking.regId = this.data.regId;
      objTracking.whitelabel = this.data.whitelabel;
      objTracking.kAbout = this.notEmpty(this.data.aboutUs)
        ? this.data.aboutUs
        : "";
      this.trPingBackend(objTracking);
      regData = this.trackingBuildRegData(objTracking);
      if (this.isEmpty(regData)) {
        this.write("%method: regData empty. Tracking aborted", {
          method: "tracking"
        });
        return false;
      }
      document.bReg = 1;
      document.regdata = regData;
      if (typeof iframe != "undefined" && this.isFunction(iframe)) {
        iframe();
      } else {
        this.write("%method: tracking script does not exist in page", {
          method: "tracking"
        });
        return false;
      }
      this.write("%method: updated successfully", { method: "tracking" });
      return true;
    },
    trackingBuildRegData: function(objTrData) {
      var regData = {
        event: objTrData.event,
        reg_title: objTrData.title,
        reg_id: objTrData.regId,
        whitelabel: objTrData.whitelabel,
        knowabout: objTrData.kAbout,
        pagename: objTrData.event == "regFinish" ? "regfinish" : "reg",
        step: objTrData.step,
        last_step: objTrData.total
      };
      var stepItems = {
        usergender: "ownGender",
        useremail: "email",
        userday1: "dayOfBirth1",
        usermonth1: "monthOfBirth1",
        useryear1: "yearOfBirth1",
        userfamily1: "familyStatus1",
        searchgender: "gender"
      };
      var responseItems = {
        userid: "userId",
        userReference: "",
        userstatus: "memberStatus",
        probScore: "purchaseProbabilityScore"
      };
      var myOffset = null;
      for (offset in stepItems) {
        myOffset = stepItems[offset] == "" ? offset : stepItems[offset];
        regData[offset] = this.notEmpty(this.data[myOffset])
          ? this.data[myOffset]
          : "";
      }
      if (this.isEmpty(this.regResponse))
        return ipxBase64.encode(JSON.stringify(regData));
      for (offset in responseItems) {
        myOffset = responseItems[offset] == "" ? offset : responseItems[offset];
        regData[offset] = this.notEmpty(this.regResponse[myOffset])
          ? this.regResponse[myOffset]
          : "";
      }
      return ipxBase64.encode(JSON.stringify(regData));
    },
    trPingBackend: function(trData) {
      var service = this.regUrl + "?op=tracking";
      if (trData.event == "regStarted" || trData.step == 1) {
        service +=
          "&path=client&screenH=" + screen.height + "&screenW=" + screen.width;
      } else if (trData.event == "regFinish") {
        var tmpCid = this.cid || "";
        var tmpRegId = trData.regId || "";
        var tmpUid =
          this.regResponse && this.regResponse.userReference
            ? this.regResponse.userReference
            : "";
        service +=
          "&path=finish&g=" +
          this.data.ownGender +
          "&y=" +
          this.data.yearOfBirth1 +
          "&m=" +
          this.data.monthOfBirth1 +
          "&d=" +
          this.data.dayOfBirth1 +
          "&mycid=" +
          tmpCid +
          "&regid=" +
          tmpRegId +
          "&uid=" +
          tmpUid;
      } else if (
        trData.step > 0 &&
        trData.step == trData.total &&
        this.regLastStepBackendPinged == false
      ) {
        this.regLastStepBackendPinged = true;
        service += "&path=last";
      } else {
        this.write(
          "%method: backend call not needed for current step. Skipping",
          { method: "trPingBackend", ws: service }
        );
        return false;
      }
      if (
        !this.talk(
          service,
          this._trPingBackend,
          { service: service, method: "trPingBackend", trDetails: trData },
          true
        )
      )
        return false;
      this.write("%method: ajax call triggered to '%ws'", {
        method: "trPingBackend",
        ws: service
      });
    },
    _trPingBackend: function(response) {
      this.write(this.msg.triggered, { method: "_trPingBackend" });
      var objResponse = JSON.parse(response);
      if (this.notEmpty(objResponse.cid) && this.cid == "") {
        this.cid = objResponse.cid;
        this.write("%method: cid (%cid) written from valid tracking response", {
          method: "_trPingBackend",
          cid: objResponse.cid
        });
      }
      this.write("%method: positive ajax response from path %path", {
        method: "_trPingBackend",
        path: objResponse.path
      });
      return true;
    },
    showSubregions: function() {
      return this.defined(this.gEnv.showSubregions)
        ? this.gEnv.showSubregions
        : false;
    },
    getUserCountry: function(clb, ip) {
      var service = this.geoAPI + "?service=getUserCountry";
      ip = ip || false;
      this.write("%method: ajax call to '%ws' triggered", {
        method: "getUserCountry",
        ws: service
      });
      if (
        !this.talk(
          service,
          this._getUserCountry,
          { method: "getUserCountry", ip: ip, cb: clb, req: window.location },
          true
        )
      )
        return false;
      return true;
    },
    _getUserCountry: function(response, objParams) {
      this.write(this.msg.triggered, { method: "getUserCountry" });
      objParams = objParams || null;
      var objUserOrigin = JSON.parse(response);
      if (objParams && objParams.cb) {
        objParams.cb.call(this, objUserOrigin);
      }
    },
    xSetCountry: function(ccOrId, container) {
      this.setCountry(ccOrId, container);
    },
    setCountry: function(ccOrId, container) {
      this.write(this.msg.triggered + " (cc:%cc)", {
        method: "setCountry",
        cc: ccOrId
      });
      ccOrId = ccOrId || null;
      container = container || null;
      if (!this.isValid("country")) {
        this.write(
          "%method: not valid for initialized reg version (%vs) - probably geoLoc enabled",
          { method: "setCountry", vs: this.type }
        );
        return false;
      }
      if (!ccOrId) {
        this.write("%method: no valid country parameter passed. Abort", {
          method: "setCountry"
        });
        return false;
      }
      if (this.fLoading) {
        this.write("%method: regions still loading from previous call. Abort", {
          method: "setCountry"
        });
        return false;
      }
      if (!this.setParam("country", ccOrId)) {
        this.write(
          "%method: something went wrong with saving country parameter. Abort",
          { method: "setCountry" }
        );
        return false;
      }
      if (container) {
        this.setRegionsByCountry(container);
      }
      return true;
    },
    xSetRegion: function(idRegion) {
      this.setRegion(idRegion);
    },
    setRegion: function(idRegion) {
      if (!idRegion) return false;
      if (!this.setParam("region", idRegion)) return false;
      return true;
    },
    xSetSubRegion: function(idSubRegion) {
      this.setSubRegion(idSubRegion);
    },
    setSubRegion: function(idSubRegion) {
      console.log(idSubRegion);
      if (!idSubRegion) return false;
      if (!this.setParam("subRegion", idSubRegion)) return false;
      return true;
    },
    setRegionsByCountry: function(container) {
      var country = this.data.country || null;
      var service = "";
      container = container || null;
      this.write(this.msg.triggered, { method: "setRegionsByCountry" });
      if (!this.isValid("region")) {
        this.write(
          "%method: not valid for initialized reg version (%vs) - probably geoLoc enabled",
          { method: "setRegionsByCountry", vs: this.type }
        );
        return false;
      }
      if (!country) {
        this.write("%method: country unknown. Abort", {
          method: "setRegionsByCountry"
        });
        return false;
      }
      service =
        this.geoAPI +
        "?service=getRegions&lang=" +
        this.data.cc +
        "&cc=" +
        country;
      this.write("%method: ajax call to '%ws' triggered", {
        method: "setRegionsByCountry",
        ws: service
      });
      if (
        !this.talk(service, this._setRegionsByCountry, {
          method: "setRegionsByCountry",
          container: container
        })
      )
        return false;
      return true;
    },
    _setRegionsByCountry: function(response, objParams) {
      this.write(this.msg.triggered, { method: "_setRegionsByCountry" });
      objParams = objParams || null;
      var container = objParams ? objParams.container || null : null;
      this.write("%method: container = '%rgCon'", {
        method: "_setRegionsByCountry",
        rgCon: container
      });
      var objGeo = JSON.parse(response);
      for (var i = 0; i < objGeo.regions.length; i++) {
        this.data.regions[i] = {
          id: objGeo.regions[i].id,
          text: objGeo.regions[i].text
        };
      }
      this.data.idUserRegion = this.isEmpty(objGeo.idUserRegion)
        ? false
        : objGeo.idUserRegion;
      this.write("%method: idUserRegion = '%rgUsr'", {
        method: "_setRegionsByCountry",
        rgUsr: this.data.idUserRegion
      });
      var usrRegionFromPassedData = this.prefillFromPassedData("region");
      if (usrRegionFromPassedData) {
        this.data.idUserRegion = usrRegionFromPassedData;
        this.write(
          "%method: overrode idUserRegion with passed data to '%rgUsr'",
          { method: "_setRegionsByCountry", rgUsr: this.data.idUserRegion }
        );
      }
      this.triggerHooks("postAjaxRegionRaw", this.data.regions);
      if (container) {
        this.loadRegionsToContainer(container);
      }
      return true;
    },
    loadRegionsAndSubregions: function() {
      var service = "";
      this.write(this.msg.triggered, { method: "loadRegionsAndSubregions" });
      if (!this.isValid("region")) {
        this.write(
          "%method: region not valid for initialized reg version (%vs)",
          { method: "loadRegionsAndSubregions", vs: this.type }
        );
        return false;
      }
      if (!this.data.country) {
        this.write("%method: country unknown. Abort", {
          method: "loadRegionsAndSubregions"
        });
        return false;
      }
      service =
        this.geoAPI +
        "?service=getRegions&lang=" +
        this.data.cc +
        "&cc=" +
        this.data.country;
      this.write("%method: ajax call to '%ws' triggered", {
        method: "loadRegionsAndSubregions",
        ws: service
      });
      if (
        !this.talk(service, this._loadRegionsAndSubregions, {
          method: "loadRegionsAndSubregions"
        })
      )
        return false;
      return true;
    },
    _loadRegionsAndSubregions: function(response) {
      this.write(this.msg.triggered, { method: "_loadRegionsAndSubregions" });
      var objGeo = JSON.parse(response);
      objGeo.regions = objGeo.regionsWithSubregions;
      delete objGeo.regionsWithSubregions;
      this.data.subregions = {};
      var idRegion;
      for (var i = 0; i < objGeo.regions.length; i++) {
        idRegion = objGeo.regions[i].id;
        this.data.regions[i] = {
          id: objGeo.regions[i].id,
          text: objGeo.regions[i].text
        };
        this.data.subregions[idRegion] = [];
        if (objGeo.regions[i].subregions.length) {
          for (var j = 0; j < objGeo.regions[i].subregions.length; j++) {
            this.data.subregions[idRegion].push({
              id: objGeo.regions[i].subregions[j].id,
              text: objGeo.regions[i].subregions[j].defaultName
            });
          }
        } else {
          if (!this.isEmpty(objGeo.regions[i].allSubregionId)) {
            this.data.subregions[idRegion].push({
              id: objGeo.regions[i].subregions[j].id,
              text: ipxQuestionnaire.getRegText("sub_region_default_all")
            });
          }
        }
      }
      var regTag = "region";
      var subRegTag = "subRegion";
      if (
        document.getElementById("region") == null &&
        document.getElementById("subRegion") == null
      ) {
        var regTag = "region_gamor";
        var subRegTag = "subRegion_gamor";
      }
      this.loadRegionsToContainer(regTag);
      this.loadSubRegionsToContainer(subRegTag);
      this.addHook("postSetParam", function(e) {
        if (e.name == "region") {
          ipxRegAPI.loadSubRegionsToContainer(subRegTag);
        }
      });
      this.triggerHooks("postAjaxSubRegionRaw", {
        regions: this.data.regions,
        subregions: this.data.subregions,
        idUserRegion: false,
        idUserSubRegion: false
      });
      return true;
    },
    xUpdateGeoContainers: function(objData) {
      this.updateGeoContainers(objData);
    },
    updateGeoContainers: function(objData) {
      var elTag = null;
      this.write(this.msg.triggered, { method: "updateGeoContainers" });
      if (!this.isValid("country") || !this.isValid("region")) {
        this.write(
          "%method: not valid for initialized reg version (%vs) - probably geoLoc enabled",
          { method: "updateGeoContainers", vs: this.type }
        );
        return false;
      }
      if (objData.hasOwnProperty("countries")) {
        if (!this.data.country) {
          this.write("%method: no country given so far, nothing to update", {
            method: "updateGeoContainers"
          });
          return false;
        }
        elTag = document.getElementById(objData.countries);
        if (!elTag) {
          this.write("%method: DOM tag '%tag' undefined. Won't update", {
            method: "loadCountryToContainer",
            tag: objData.countries
          });
          return false;
        }
        if (!this.setFormValue(elTag, this.data.country)) {
          this.write(
            "%method: form value could not be set for %tag. Won't update",
            { method: "loadCountryToContainer", tag: objData.countries }
          );
        }
      }
      if (objData.hasOwnProperty("regions")) {
        if (!this.data.region) {
          this.write("%method: no region given so far, nothing to update", {
            method: "updateGeoContainers"
          });
          return false;
        }
        elTag = document.getElementById(objData.regions);
        if (!elTag) {
          this.write("%method: DOM tag '%tag' undefined. Won't update", {
            method: "loadCountryToContainer",
            tag: objData.regions
          });
          return false;
        }
        if (!this.setFormValue(elTag, this.data.regions)) {
          this.write(
            "%method: form value could not be set for %tag. Won't update",
            { method: "loadCountryToContainer", tag: objData.regions }
          );
        }
      }
      return true;
    },
    loadTranslatedCountriesToContainer: function(ccContainer, regioContainer) {
      var service = "";
      this.write(this.msg.triggered, {
        method: "loadTranslatedCountriesToContainer"
      });
      if (!this.isValid("country")) {
        this.write(
          "%method: not valid for initialized reg version (%vs) - probably geoLoc enabled",
          { method: "loadTranslatedCountriesToContainer", vs: this.type }
        );
        return false;
      }
      service = this.geoAPI + "?service=getCountries&cc=" + this.data.cc;
      this.write("%method: ajax call to '%ws' triggered", {
        method: "loadTranslatedCountriesToContainer",
        ws: service
      });
      if (
        !this.talk(service, this._loadTranslatedCountriesToContainer, {
          method: "loadTranslatedCountriesToContainer",
          ccContainer: ccContainer,
          regioContainer: regioContainer
        })
      )
        return false;
      return true;
    },
    _loadTranslatedCountriesToContainer: function(response, objParams) {
      this.write(this.msg.triggered, {
        method: "_loadTranslatedCountriesToContainer"
      });
      objParams = objParams || null;
      var ccContainer = objParams ? objParams.ccContainer || null : null;
      var regioContainer = objParams ? objParams.regioContainer || null : null;
      this.write("%method: containers = '%ccCon', '%rgCon'", {
        method: "_loadTranslatedCountriesToContainer",
        ccCon: ccContainer,
        rgCon: regioContainer
      });
      var objGeo = JSON.parse(response);
      var code;
      for (code in objGeo) {
        this.data.trCountries[code] = objGeo[code];
      }
      this.triggerHooks(
        "postAjaxCountryRaw",
        this.sortCountries(this.data.trCountries)
      );
      return this.loadCountriesToContainer(ccContainer, regioContainer);
    },
    loadCountriesToContainer: function(ccContainer, regioContainer) {
      this.write(this.msg.triggered, { method: "loadCountriesToContainer" });
      ccContainer = ccContainer || null;
      regioContainer = regioContainer || null;
      fLoadRegions = false;
      if (!this.isValid("country")) {
        this.write(
          "%method: not valid for initialized reg version (%vs) - probably geoLoc enabled",
          { method: "loadCountriesToContainer", vs: this.type }
        );
        return false;
      }
      var countries = this.data.trCountries || this.data.countries;
      if (!countries) {
        this.write("%method: no countries, nothing to export", {
          method: "loadCountriesToContainer"
        });
        return false;
      }
      var myCountry =
        this.data.country || this.data.countries[this.data.cc] || null;
      var ccTop = this.ccMap[myCountry] || [];
      var ccOverride = this.prefillFromPassedData("country");
      if (ccOverride) {
        myCountry = ccOverride;
        this.data.country = myCountry;
      }
      if (this.showSubregions()) {
        this.write(
          "%method: load (sub)regions shown instead of country, abort loading country container",
          { method: "loadCountriesToContainer" }
        );
        if (!this.data.country) this.setCountry(myCountry);
        this.loadRegionsAndSubregions();
        return true;
      }
      var elTag =
        ccContainer != null ? document.getElementById(ccContainer) : null;
      if (!elTag) {
        this.write("%method: DOM tag '%tag' undefined", {
          method: "loadCountriesToContainer",
          tag: ccContainer
        });
        return false;
      }
      while (elTag.firstChild) elTag.removeChild(elTag.firstChild);
      var i = 0;
      if (ccTop) {
        var elTop;
        for (elTop in ccTop) {
          elTag[i++] = new Option(
            countries[ccTop[elTop]],
            ccTop[elTop],
            false,
            ccTop[elTop] == myCountry
          );
          if (ccTop[elTop] == myCountry) fLoadRegions = true;
        }
      }
      var sortable = [];
      var idx;
      for (idx in countries) {
        if (this.inArray(idx, ccTop)) continue;
        sortable.push([idx, countries[idx]]);
      }
      sortable.sort(function(a, b) {
        return a[1].localeCompare(b[1]);
      });
      var j = 0,
        offset = 0;
      for (; j < sortable.length; j++) {
        offset = sortable[j][0];
        elTag[i++] = new Option(
          countries[offset],
          offset,
          false,
          offset == myCountry
        );
        if (offset == myCountry) fLoadRegions = true;
      }
      var elTag2 =
        regioContainer != null ? document.getElementById(regioContainer) : null;
      if (!elTag2) {
        this.write("%method: DOM tag '%tag' undefined", {
          method: "loadCountriesToContainer",
          tag: regioContainer
        });
      } else {
        this.setAttribute(
          elTag,
          "onchange",
          "ipxRegAPI.setCountry(this.value,'" + regioContainer + "')"
        );
      }
      if (fLoadRegions) {
        if (!this.data.country) this.setCountry(this.data.cc, regioContainer);
        else this.setRegionsByCountry(regioContainer);
      }
      this.triggerHooks("postAjaxCountry");
      return true;
    },
    loadRegionsToContainer: function(container) {
      var elTag = null;
      this.write(this.msg.triggered, { method: "loadRegionsToContainer" });
      if (!this.isValid("region")) {
        this.write(
          "%method: not valid for initialized reg version (%vs) - probably geoLoc enabled",
          { method: "loadRegionsToContainer", vs: this.type }
        );
        return false;
      }
      if (!this.data.regions || this.data.regions.length < 1) {
        this.write("%method: no regions given so far, nothing to export", {
          method: "loadRegionsToContainer"
        });
        return false;
      }
      elTag = document.getElementById(container);
      if (!elTag) {
        this.write("%method: DOM tag '%tag' undefined", {
          method: "loadRegionsToContainer",
          tag: container
        });
        return false;
      }
      var fSetUserRegion = false,
        fPresetDone = false;
      while (elTag.firstChild) elTag.removeChild(elTag.firstChild);
      for (var i = 0; i < this.data.regions.length; i++) {
        if (container == "region_gamor") {
          elTag.innerHTML +=
            "<li data-value='" +
            this.data.regions[i].id +
            "'>" +
            this.data.regions[i].text +
            "</li>";
        } else {
          if (this.data.idUserRegion == this.data.regions[i].id)
            fSetUserRegion = true;
          elTag[i] = new Option(
            this.data.regions[i].text,
            this.data.regions[i].id,
            false,
            fSetUserRegion && !fPresetDone
          );
        }
        if (fSetUserRegion) fPresetDone = true;
      }
      if (fSetUserRegion) this.setRegion(this.data.idUserRegion);
      else this.setRegion(this.data.regions[0].id);
      if (container != "region_gamor") {
        this.setAttribute(
          elTag,
          "onchange",
          "ipxRegAPI.xSetRegion(this.value)"
        );
      }
      this.triggerHooks("postAjaxRegion");
      return true;
    },
    loadSubRegionsToContainer: function(container) {
      var elTag = null;
      this.write(this.msg.triggered, { method: "loadSubRegionsToContainer" });
      if (!this.isValid("region")) {
        this.write(
          "%method: not valid for initialized reg version (%vs) - probably geoLoc enabled",
          { method: "loadSubRegionsToContainer", vs: this.type }
        );
        return false;
      }
      if (!this.data.subregions || this.data.subregions.length < 1) {
        this.write("%method: no regions given so far, nothing to export", {
          method: "loadSubRegionsToContainer"
        });
        return false;
      }
      elTag = document.getElementById(container);
      if (!elTag) {
        this.write("%method: DOM tag '%tag' undefined", {
          method: "loadSubRegionsToContainer",
          tag: container
        });
        return false;
      }
      var fSetUserSubRegion = false,
        fPresetDone = false;
      while (elTag.firstChild) elTag.removeChild(elTag.firstChild);
      for (var i = 0; i < this.data.subregions[this.data.region].length; i++) {
        if (container == "subRegion_gamor") {
          elTag.innerHTML +=
            "<li data-value='" +
            this.data.subregions[this.data.region][i].id +
            "'>" +
            this.data.subregions[this.data.region][i].text +
            "</li>";
        } else {
          if (
            this.data.idUserRegion ==
            this.data.subregions[this.data.region][i].id
          )
            fSetUserSubRegion = true;
          elTag[i] = new Option(
            this.data.subregions[this.data.region][i].text,
            this.data.subregions[this.data.region][i].id,
            false,
            fSetUserSubRegion && !fPresetDone
          );
        }
        if (fSetUserSubRegion) fPresetDone = true;
      }
      if (fSetUserSubRegion) this.setSubRegion(this.data.idUserSubRegion);
      else this.setSubRegion(this.data.subregions[this.data.region].id);
      if (fSetUserSubRegion) this.setSubRegion(this.data.idUserSubRegion);
      else this.setSubRegion(this.data.subregions[this.data.region][0].id);
      if (container != "subRegion_gamor") {
        this.setAttribute(
          elTag,
          "onchange",
          "ipxRegAPI.xSetSubRegion(this.value)"
        );
      }
      this.triggerHooks("postAjaxRegion");
      return true;
    },
    loadOwnGenderToContainer: function(container) {
      this.loadToContainer("ownGender", container);
      return true;
    },
    loadGenderToContainer: function(container) {
      this.loadToContainer("gender", container);
      return true;
    },
    loadSizesToContainer: function(container) {
      this.loadToContainer("size1", container);
      return true;
    },
    loadDayOfBirthToContainer: function(container) {
      this.loadToContainer("dayOfBirth1", container);
      return true;
    },
    loadMonthOfBirthToContainer: function(container) {
      this.loadToContainer("monthOfBirth1", container);
      return true;
    },
    loadYearOfBirthToContainer: function(container) {
      this.loadToContainer("yearOfBirth1", container);
      return true;
    },
    loadFamilyStatusToContainer: function(container) {
      this.loadToContainer("familyStatus1", container);
      return true;
    },
    loadShapeToContainer: function(container) {
      this.loadToContainer("shape1", container);
      return true;
    },
    loadHairColorToContainer: function(container) {
      this.loadToContainer("hairColor1", container);
      return true;
    },
    loadHairLengthToContainer: function(container) {
      this.loadToContainer("hairLength1", container);
      return true;
    },
    loadEyesColorToContainer: function(container) {
      this.loadToContainer("eyesColor1", container);
      return true;
    },
    loadStuffToContainer: function(objMultiple, fSetParams) {
      objMultiple = objMultiple || null;
      fSetParams = fSetParams || false;
      this.write(
        this.msg.triggered +
          (!objMultiple ? " (no param, scanning page tags)" : ""),
        { method: "loadStuffToContainer" }
      );
      if (!objMultiple || !this.isObject(objMultiple)) {
        for (var valid in this.validPreloads) {
          if (!this.inArray(this.validPreloads[valid], this.valid)) continue;
          if (
            this.loadToContainer(
              this.validPreloads[valid],
              this.validPreloads[valid]
            )
          ) {
            this.write("%method: list loaded into tag '%tag'", {
              method: "loadStuffToContainer",
              tag: this.validPreloads[valid]
            });
          }
        }
        this.presetFromUrl();
        return true;
      }
      for (var prop in objMultiple) {
        if (!this.inArray(prop, this.validPreloads)) {
          this.write(
            "%method: prop '%prop' not a valid prop to preload. Skipping",
            { method: "loadStuffToContainer", prop: prop }
          );
          continue;
        }
        if (!this.loadToContainer(prop, objMultiple[prop], fSetParams)) {
          return false;
        }
      }
      this.presetFromUrl();
      return true;
    },
    loadToContainer: function(what, container, fSetParams) {
      fSetParams = fSetParams || false;
      var elTag = null;
      this.write(this.msg.triggered + " (%what)", {
        method: "loadToContainer",
        what: what
      });
      what = what || null;
      container = container || null;
      var caption = "";
      var limits = [];
      fPreset = false;
      if (this.fUseAboutUs) {
        var noPresetIsFatal = ["size1", "aboutUs"];
      } else {
        var noPresetIsFatal = ["size1"];
      }
      if (!what || !container) {
        this.write("%method: parameter or container not defined. Abort", {
          method: "loadToContainer"
        });
        return false;
      }
      elTag = document.getElementById(container);
      if (!elTag || !this.isInputSelect(elTag)) {
        this.write("%method: DOM tag '%tag' undefined. Abort", {
          method: "loadToContainer",
          tag: container
        });
        return false;
      }
      if (!this.presets[what] || !this.isObject(this.presets[what])) {
        if (this.inArray(what, noPresetIsFatal)) {
          this.write("%method: preset not found for '%what'. Abort", {
            method: "loadToContainer",
            what: what
          });
          return false;
        } else {
          this.write(
            "%method: preset not found for '%what'. Continue without",
            { method: "loadToContainer", what: what }
          );
        }
      } else {
        fPreset = true;
        var myX = 0,
          myBottom = 0,
          myTop = this.lenObject(this.presets[what]) - 1;
        for (var myItem in this.presets[what]) {
          if (!myX++) myBottom = myItem;
          myTop = myItem;
        }
        limits = [myBottom, myTop];
      }
      var selDefault = this.presets.defaults[what] || null;
      switch (what) {
        case "ownGender":
          if (!fPreset) {
            limits = [1, 2];
            selDefault = 2;
          } else selDefault = selDefault || 2;
          break;
        case "gender":
          if (!fPreset) {
            caption = this.tr("gender");
            limits = [1, 2];
            selDefault = 1;
          } else selDefault = selDefault || 1;
          break;
        case "size1":
          if (!fPreset) {
            caption = this.tr("size1");
            limits = [140, 210];
          }
          break;
        case "dayOfBirth1":
          if (!fPreset) {
            caption = this.tr("dayOfBirth1");
            limits = [1, 31];
          }
          break;
        case "monthOfBirth1":
          if (!fPreset) {
            caption = this.tr("monthOfBirth1");
            limits = [1, 12];
          }
          break;
        case "yearOfBirth1":
          caption = this.tr("yearOfBirth1");
          limits = [this.birthlimits.bottom, this.birthlimits.top];
          break;
        case "familyStatus1":
          if (!fPreset) {
            caption = this.tr("familyStatus1");
            imits = [1, 3];
          }
          break;
        case "shape1":
          if (!fPreset) {
            caption = this.tr("shape1");
            limits = [1, 3];
          }
          break;
        case "hairColor1":
          if (!fPreset) {
            caption = this.tr("hairColor1");
            limits = [1, 7];
          }
          break;
        case "hairLength1":
          if (!fPreset) {
            caption = this.tr("hairLength1");
            limits = [1, 4];
          }
          break;
        case "eyesColor1":
          if (!fPreset) {
            caption = this.tr("eyesColor1");
            limits = [1, 9];
          }
          break;
        default:
          if (!fPreset) {
            this.write(
              "%method: parameter '%param' not defined and no preset. Abort",
              { method: "loadToContainer", param: what }
            );
            return false;
          }
      }
      this.write("%method: limits set successfully (%bottom,%top)", {
        method: "loadToContainer",
        bottom: limits[0],
        top: limits[1]
      });
      var j = 0;
      if (caption != "") {
        if (!selDefault) selDefault = 0;
        elTag[j++] = new Option(caption, 0, false, selDefault == 0);
        this.write("%method: first option set to caption (%cap)", {
          method: "loadToContainer",
          cap: caption
        });
      }
      if (fPreset) {
        if (what == "aboutUs") {
          var arrKeys = this.getSortedKeysFromObj(this.presets[what], "rnd");
          for (var i = 0; i < arrKeys.length; i++) {
            var prop = arrKeys[i];
            if (selDefault == null && !j) selDefault = prop;
            elTag[j++] = new Option(
              this.presets[what][prop],
              prop,
              false,
              prop == selDefault
            );
          }
        } else {
          for (var prop in this.presets[what]) {
            if (selDefault == null && what != "size1" && !j) {
              selDefault = prop;
            }
            elTag[j++] = new Option(
              this.presets[what][prop],
              prop,
              false,
              prop == selDefault
            );
          }
        }
      } else {
        if (selDefault == null && what == "yearOfBirth1")
          selDefault = limits[1] - 10;
        else selDefault = selDefault != null ? selDefault : limits[0];
        if (what == "yearOfBirth1" || what == "size1") {
          for (var i = limits[1]; i >= limits[0]; i--) {
            elTag[j++] = new Option(i, i, false, i == selDefault);
          }
        } else {
          for (var i = limits[0]; i <= limits[1]; i++) {
            elTag[j++] = new Option(i, i, false, i == selDefault);
          }
        }
      }
      if (fSetParams) this.setParam(what, selDefault);
      this.setAttribute(
        elTag,
        "onchange",
        "ipxRegAPI.xSetParam('" + what + "',this.value)"
      );
      return true;
    },
    getSortedKeysFromObj: function(obj, mode) {
      mode = mode || "asc";
      var fZero = false;
      if (!Object.keys) {
        Object.keys = function(obj) {
          var keys = [];
          for (var i in obj) {
            if (obj.hasOwnProperty(i)) {
              keys.push(i);
            }
          }
          return keys;
        };
      }
      var keys = Object.keys(obj);
      if (!this.isEmpty(keys[0])) {
        var fZero = true;
        keys.splice(0, 1);
      }
      var len = keys.length;
      var half = Math.round(len / 2 - 1);
      if (this.isNumeric(keys[half])) {
        if (mode == "asc") keys.sort(this.arrSortNumericKeysAsc);
        else if (mode == "desc") keys.sort(this.arrSortNumericKeysDesc);
        else if (mode == "rnd") keys = this.arrShuffle(keys);
      } else {
        if (mode == "asc") keys.sort();
        else if (mode == "desc") keys.reverse();
        else if (mode == "rnd") keys = this.arrShuffle(keys);
      }
      if (fZero) keys.unshift(0);
      return keys;
    },
    arrSortNumericKeysAsc: function(a, b) {
      return a - b;
    },
    arrSortNumericKeysDesc: function(a, b) {
      return b - a;
    },
    presetFromUrl: function() {
      this.write(this.msg.triggered, { method: "presetFromUrl" });
      var gender = this.grabParamFromUrl("gender");
      if (gender != "") {
        this.fGenderPrefilled = this.setParam("gender", gender);
      }
      var ownGender = this.grabParamFromUrl("ownGender");
      if (ownGender != "") {
        this.fOwnGenderPrefilled = this.setParam("ownGender", ownGender);
      }
    },
    addHook: function(type, func, fOnce, name) {
      this.write(this.msg.triggered + " (%el)", {
        method: "addHook",
        el: type
      });
      fOnce = fOnce || 0;
      if (this.hooks[type])
        this.hooks[type].push({ once: fOnce, name: name, fc: func });
      else this.hooks[type] = new Array({ once: fOnce, name: name, fc: func });
    },
    triggerHooks: function(type, objParams) {
      objParams = objParams || null;
      var clbName = "";
      this.write(this.msg.triggered + " (%type)", {
        method: "triggerHooks",
        type: type
      });
      if (!this.hooks[type]) {
        this.write("%method: no hooked functions to trigger", {
          method: "triggerHooks"
        });
        return;
      }
      for (var objCb in this.hooks[type]) {
        if (
          !this.isFunction(this.hooks[type][objCb].fc) ||
          this.hooks[type][objCb].once == -1
        ) {
          continue;
        }
        if (objParams) {
          this.hooks[type][objCb].fc(objParams);
        } else {
          this.hooks[type][objCb].fc();
        }
        clbName = this.hooks[type][objCb].name || "-";
        if (clbName != "-") {
          this.write("%method (%type): clb triggered (%name)", {
            method: "triggerHooks",
            type: type,
            name: clbName
          });
        }
        if (this.hooks[type][objCb].once == 1) {
          this.write("%method (%type): clb removed from queue (%name)", {
            method: "triggerHooks",
            type: type,
            name: clbName
          });
          this.hooks[type][objCb].once = -1;
        }
      }
      this.write("%method: done", { method: "triggerHooks" });
    },
    getTldCountryId: function() {
      return this.data.countries[this.data.cc];
    },
    sortCountries: function(arrCountries) {
      var countries = arrCountries || this.data.trCountries || null;
      if (!countries) return false;
      var sortable = [];
      var idx;
      for (idx in countries) {
        sortable.push([idx, countries[idx]]);
      }
      sortable.sort(function(a, b) {
        return a[1].localeCompare(b[1]);
      });
      return sortable;
    },
    setAttribute: function(el, attr, value) {
      el.setAttribute(attr, value);
      if (typeof el[attr] == "string") {
        el.setAttribute(attr, function() {
          eval(value);
        });
      }
    },
    log: function(name, val) {
      this.write(this.msg.triggered + " (%name = %val)", {
        method: "log",
        name: name,
        val: val
      });
      this.logData[name] = val;
    },
    getFormValue: function(element) {
      if (this.isInputText(element)) {
        return element.value;
      }
      if (this.isInputSelect(element)) {
        return this.inputGetSelected(element);
      }
      this.write("%method: type could not be determined for '%name'. Abort", {
        method: "getFormValue",
        name: element.name
      });
      return false;
    },
    setFormValue: function(element, value) {
      this.write(this.msg.triggered, { method: "setFormValue" });
      element = element || null;
      if (!element) {
        this.write("%method: no valid form element given. Abort", {
          method: "setFormValue"
        });
        return false;
      }
      if (this.isInputText(element)) {
        element.value = value;
        return true;
      }
      if (this.isInputSelect(element)) {
        this.inputSetSelected(element, value);
        return true;
      }
      this.write(
        "%method: no corresponding form element/type found for '%tag'. Abort",
        { method: "setFormValue", tag: element.name }
      );
      return false;
    },
    getEnv: function(fAddParamsToUrl) {
      this.write(this.msg.triggered, { method: "getEnv" });
      fAddParamsToUrl = fAddParamsToUrl || false;
      var objEnv = this.gEnv;
      objEnv.dlUrl +=
        fAddParamsToUrl && !objEnv.dlUrl.match(/target/)
          ? "&target=/memberhome.htm&firstLogin=true&dl="
          : "";
      this.write("%method: host = %host", {
        method: "getEnv",
        host: objEnv.dlHost
      });
      return objEnv;
    },
    getWhitelabel: function(isMobile) {
      var env = this.getEnv();
      isMobile = isMobile || false;
      if (
        !isMobile &&
        (window.screen.width < 768 || window.screen.height < 600)
      ) {
        isMobile = true;
      }
      if (env.isCdate && isMobile) return "cdate_mobile";
      if (env.isCdate && !isMobile) return "cdate";
      if (env.isErovie && isMobile) return "erovie_mobile";
      if (env.isErovie && !isMobile) return "erovie";
      if (env.isLisa18 && isMobile) return "erovie_mobile";
      if (env.isLisa18 && !isMobile) return "erovie";
      if (env.isNewhoney && isMobile) return "erovie_mobile";
      if (env.isNewhoney && !isMobile) return "erovie";
      return "-";
    },
    isMobile: function() {
      this.data.whitelabel = this.getWhitelabel(true);
    },
    write: function(strgFormat, objParams) {
      if (!this.debug) return;
      if (strgFormat.indexOf("%") == -1 || this.objIsEmpty(objParams)) {
        this.write2console(strgFormat);
        return;
      }
      this.write2console(this.printf(strgFormat, objParams));
    },
    writeArray: function(myarr) {
      var strg = "";
      for (var i in myarr) {
        strg += (strg != "" ? "," : "") + myarr[i];
      }
      return strg;
    },
    printf: function(strgFormat, objParams) {
      var result = strgFormat;
      for (var prop in objParams) {
        if (!objParams.hasOwnProperty(prop)) continue;
        result = result.replace("%" + prop, objParams[prop]);
      }
      return result;
    },
    write2console: function(txt) {
      if (!this.debug) return;
      if (this.fConsole) {
        console.log(txt);
        return;
      }
      if (!document.getElementById("console")) {
        alert(
          "write2console, Tag 'console' undefined. Document body load complete?\n" +
            txt
        );
        return;
      }
      var elTag = document.getElementById("console");
      var eTxt = document.createTextNode(txt);
      elTag.appendChild(eTxt);
      var br = document.createElement("br");
      elTag.appendChild(br);
    },
    obj2console: function(obj) {
      if (this.isObject(obj) == false) {
        if (this.isString(obj)) console.log(obj);
        return;
      }
      if (this.objIsEmpty(obj)) {
        console.log("Object empty, nothing to show.");
        return;
      }
      for (var prop in obj) {
        console.log(prop + ": " + obj[prop]);
      }
    },
    isValid: function(param) {
      return this.inArray(param, this.valid);
    },
    defined: function(variable) {
      return variable !== null;
    },
    isEmpty: function(variable) {
      if (this.isObject(variable)) return this.objIsEmpty(variable);
      return variable == null || !variable || variable == "";
    },
    notEmpty: function(variable) {
      return this.isEmpty(variable) == false;
    },
    lenObject: function(obj) {
      var size = 0,
        prop;
      for (prop in obj) {
        if (obj.hasOwnProperty(prop)) size++;
      }
      return size;
    },
    objIsEmpty: function(obj) {
      if (!this.isObject(obj)) return false;
      for (var prop in obj) {
        if (!obj.hasOwnProperty(prop)) continue;
        return false;
      }
      return true;
    },
    cloneObj: function(obj) {
      return JSON.parse(JSON.stringify(obj));
      var clone = {};
      for (var prop in obj) {
        if (typeof obj[prop] == "object" && obj[prop] != null) {
          clone[prop] = this.cloneObj(obj[prop]);
        } else {
          clone[prop] = obj[prop];
        }
      }
      return clone;
    },
    arrShuffle: function(srcArray) {
      if (srcArray.length <= 0) return srcArray;
      var idxCurrent = srcArray.length,
        valTmp,
        idxRandom;
      while (0 !== idxCurrent) {
        idxRandom = Math.floor(Math.random() * idxCurrent);
        idxCurrent -= 1;
        valTmp = srcArray[idxCurrent];
        srcArray[idxCurrent] = srcArray[idxRandom];
        srcArray[idxRandom] = valTmp;
      }
      return srcArray;
    },
    isInputSelect: function(objTag) {
      objTag = objTag || false;
      if (!objTag) return false;
      return objTag.type == "select-one" || objTag.type == "select-multiple";
    },
    inputGetSelected: function(objTag) {
      objTag = objTag || false;
      if (!objTag) return false;
      return objTag.options[objTag.selectedIndex].value;
    },
    inputSetSelected: function(objTag, value) {
      objTag = objTag || false;
      if (!objTag || !objTag.options) return false;
      for (var i = 0; i < objTag.options.length; i++) {
        if (objTag.options[i].value == value) {
          objTag.options[i].selected = true;
          return true;
        }
      }
      return false;
    },
    isInputText: function(objTag) {
      objTag = objTag || false;
      return (
        objTag &&
        this.inArray(objTag.type, ["text", "hidden", "email", "password"])
      );
    },
    isFunction: function(variable) {
      variable = variable || false;
      return variable && typeof variable === "function";
    },
    isObject: function(variable) {
      variable = variable || false;
      return variable && typeof variable === "object";
    },
    isString: function(variable) {
      variable = variable || false;
      return variable && typeof variable === "string";
    },
    isNumeric: function(variable) {
      variable = variable || false;
      return (
        variable &&
        (typeof variable === "number" || typeof parseInt(variable) === "number")
      );
    },
    inArray: function(needle, haystack) {
      for (var i = 0; i < haystack.length; i++) {
        if (haystack[i] == needle) return true;
      }
      return false;
    },
    inHashKeys: function(needle, haystack) {
      for (var el in haystack) {
        if (el == needle) return true;
      }
      return false;
    },
    hasClass: function(el, whichClass) {
      var elObj = null;
      elObj = document.getElementById(el);
      if (!elObj) {
        elObj = document.getElementsByTagName(el)[0];
      }
      if (!elObj) {
        return false;
      }
      var classNames = elObj.className.split(" ");
      for (var i = 0; i < classNames.length; i++) {
        if (classNames[i].toLowerCase() == whichClass.toLowerCase()) {
          return true;
        }
      }
      return false;
    },
    trim: function(strg) {
      if (this.isString(strg) == false) return strg;
      return strg.replace(/^\s+|\s+$/g, "");
    },
    readCookie: function(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(";");
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
      }
      return "";
    },
    setCookie: function(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
      var expires = "expires=" + d.toUTCString();
      document.cookie = cname + "=" + cvalue + "; " + expires;
    },
    deleteCookie: function(name) {
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    },
    talk: function(url, callback, objParams, postjson) {
      if (this.fLoading) {
        this.write(
          "%method: ano ajax req running atm. Putting request into queue for callback '%clb'",
          { method: "talk", clb: objParams.method }
        );
        this._talkWaitTillDone(url, callback, objParams, postjson);
        return true;
      }
      this.fLoading = true;
      objParams = objParams || null;
      postjson = postjson || false;
      var self = this;
      var xmlHttp = null;
      try {
        xmlHttp = new XMLHttpRequest();
      } catch (e) {
        try {
          xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
          try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
          } catch (e) {
            xmlHttp = null;
          }
        }
      }
      if (!xmlHttp) {
        this.write("%method: ajax object couldn't be initialized", {
          method: "talk"
        });
        return false;
      }
      var sendMethod = postjson ? "POST" : "GET";
      xmlHttp.open(sendMethod, url, true);
      xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4) {
          self._talk(callback, xmlHttp.responseText, objParams);
        }
      };
      if (!postjson) {
        xmlHttp.send(null);
      } else {
        xmlHttp.setRequestHeader("Content-Type", "application/json");
        xmlHttp.setRequestHeader("Accept", "application/json");
        xmlHttp.send(JSON.stringify(objParams));
      }
      return true;
    },
    _talkWaitTillDone: function(
      url,
      callback,
      objParams,
      postjson,
      numFailures
    ) {
      numFailures = numFailures || 1;
      var strgCallback = objParams.method || "";
      this.write(this.msg.triggered + " #%num (clb name: %clb)", {
        method: "_talkWaitTillDone",
        num: numFailures,
        clb: strgCallback
      });
      var self = this;
      if (this.fLoading) {
        if (numFailures > 10) {
          this.write("%method: aborting piled request for method '%clb'.", {
            method: "_talkWaitTillDone",
            clb: objParams.method
          });
          return;
        }
        window.setTimeout(function() {
          self._talkWaitTillDone(
            url,
            callback,
            objParams,
            postjson,
            numFailures + 1
          );
        }, 800);
        return;
      }
      this.write("%method: wait done for callback '%clb'. Triggering ...", {
        method: "_talkWaitTillDone",
        clb: strgCallback
      });
      this.talk(url, callback, objParams, postjson);
    },
    _talk: function(clb, response, objParams) {
      this.write(this.msg.triggered, { method: "_talk" });
      this.fLoading = false;
      if (!response || response == "-") {
        var strgService = objParams.service || "";
        this.write(
          "%method: no valid response from last Ajax request (service: %clb). Abort",
          { method: "_talk", clb: strgService }
        );
        return false;
      }
      if (
        response.indexOf("Warning") != -1 ||
        response.indexOf("Parse error") != -1
      ) {
        this.write(
          "%method: we got some error from backend, debug: " + response,
          { method: "_talk" }
        );
        return false;
      }
      clb.call(this, response, objParams);
      return true;
    }
  };
  var ipxBase64 = {
    alphabet:
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    lookup: null,
    ie: /MSIE /.test(navigator.userAgent),
    ieo: /MSIE [67]/.test(navigator.userAgent),
    encode: function(s) {
      var buffer = this.toUtf8(s),
        position = -1,
        len = buffer.length,
        nan0,
        nan1,
        nan2,
        enc = [, , ,];
      if (this.ie) {
        var result = [];
        while (++position < len) {
          nan0 = buffer[position];
          nan1 = buffer[++position];
          enc[0] = nan0 >> 2;
          enc[1] = ((nan0 & 3) << 4) | (nan1 >> 4);
          if (isNaN(nan1)) enc[2] = enc[3] = 64;
          else {
            nan2 = buffer[++position];
            enc[2] = ((nan1 & 15) << 2) | (nan2 >> 6);
            enc[3] = isNaN(nan2) ? 64 : nan2 & 63;
          }
          result.push(
            this.alphabet.charAt(enc[0]),
            this.alphabet.charAt(enc[1]),
            this.alphabet.charAt(enc[2]),
            this.alphabet.charAt(enc[3])
          );
        }
        return result.join("");
      } else {
        var result = "";
        while (++position < len) {
          nan0 = buffer[position];
          nan1 = buffer[++position];
          enc[0] = nan0 >> 2;
          enc[1] = ((nan0 & 3) << 4) | (nan1 >> 4);
          if (isNaN(nan1)) enc[2] = enc[3] = 64;
          else {
            nan2 = buffer[++position];
            enc[2] = ((nan1 & 15) << 2) | (nan2 >> 6);
            enc[3] = isNaN(nan2) ? 64 : nan2 & 63;
          }
          result +=
            this.alphabet[enc[0]] +
            this.alphabet[enc[1]] +
            this.alphabet[enc[2]] +
            this.alphabet[enc[3]];
        }
        return result;
      }
    },
    decode: function(s) {
      if (s.length % 4)
        throw new Error(
          "InvalidCharacterError: decode failed: The string to be decoded is not correctly encoded."
        );
      var buffer = this.fromUtf8(s),
        position = 0,
        len = buffer.length;
      if (this.ieo) {
        var result = [];
        while (position < len) {
          if (buffer[position] < 128)
            result.push(String.fromCharCode(buffer[position++]));
          else if (buffer[position] > 191 && buffer[position] < 224)
            result.push(
              String.fromCharCode(
                ((buffer[position++] & 31) << 6) | (buffer[position++] & 63)
              )
            );
          else
            result.push(
              String.fromCharCode(
                ((buffer[position++] & 15) << 12) |
                  ((buffer[position++] & 63) << 6) |
                  (buffer[position++] & 63)
              )
            );
        }
        return result.join("");
      } else {
        var result = "";
        while (position < len) {
          if (buffer[position] < 128)
            result += String.fromCharCode(buffer[position++]);
          else if (buffer[position] > 191 && buffer[position] < 224)
            result += String.fromCharCode(
              ((buffer[position++] & 31) << 6) | (buffer[position++] & 63)
            );
          else
            result += String.fromCharCode(
              ((buffer[position++] & 15) << 12) |
                ((buffer[position++] & 63) << 6) |
                (buffer[position++] & 63)
            );
        }
        return result;
      }
    },
    toUtf8: function(s) {
      var position = -1,
        len = s.length,
        chr,
        buffer = [];
      if (/^[\x00-\x7f]*$/.test(s))
        while (++position < len) buffer.push(s.charCodeAt(position));
      else
        while (++position < len) {
          chr = s.charCodeAt(position);
          if (chr < 128) buffer.push(chr);
          else if (chr < 2048) buffer.push((chr >> 6) | 192, (chr & 63) | 128);
          else
            buffer.push(
              (chr >> 12) | 224,
              ((chr >> 6) & 63) | 128,
              (chr & 63) | 128
            );
        }
      return buffer;
    },
    fromUtf8: function(s) {
      var position = -1,
        len,
        buffer = [],
        enc = [, , ,];
      if (!this.lookup) {
        len = this.alphabet.length;
        this.lookup = {};
        while (++position < len)
          this.lookup[this.alphabet.charAt(position)] = position;
        position = -1;
      }
      len = s.length;
      while (++position < len) {
        enc[0] = this.lookup[s.charAt(position)];
        enc[1] = this.lookup[s.charAt(++position)];
        buffer.push((enc[0] << 2) | (enc[1] >> 4));
        enc[2] = this.lookup[s.charAt(++position)];
        if (enc[2] == 64) break;
        buffer.push(((enc[1] & 15) << 4) | (enc[2] >> 2));
        enc[3] = this.lookup[s.charAt(++position)];
        if (enc[3] == 64) break;
        buffer.push(((enc[2] & 3) << 6) | enc[3]);
      }
      return buffer;
    }
  };
  ipxApi.setGlobalEnvironment(glObjEnv);
  return ipxApi;
})();
function initIpxRegAPI(fDebug) {
  ipxRegAPI.setDebug(fDebug);
  if (ipxRegAPI.init("en-AU", "2step") == false) return false;
  ipxRegAPI.setPresets({
    gender: { 1: glRegTexts.gender_1, 2: glRegTexts.gender_2 },
    ownGender: { 1: glRegTexts.ownGender_1, 2: glRegTexts.ownGender_2 },
    size1: loadSize(),
    monthOfBirth1: {
      0: glRegTexts.month_birth_selected,
      1: glRegTexts.month_birth_1,
      2: glRegTexts.month_birth_2,
      3: glRegTexts.month_birth_3,
      4: glRegTexts.month_birth_4,
      5: glRegTexts.month_birth_5,
      6: glRegTexts.month_birth_6,
      7: glRegTexts.month_birth_7,
      8: glRegTexts.month_birth_8,
      9: glRegTexts.month_birth_9,
      10: glRegTexts.month_birth_10,
      11: glRegTexts.month_birth_11,
      12: glRegTexts.month_birth_12
    },
    familyStatus1: {
      0: glRegTexts.family_status_selected,
      1: glRegTexts.family_status_1,
      2: glRegTexts.family_status_2,
      3: glRegTexts.family_status_3
    },
    shape1: {
      0: glRegTexts.shape_selected,
      1: glRegTexts.shape_1,
      2: glRegTexts.shape_2,
      3: glRegTexts.shape_3
    },
    hairColor1: {
      0: glRegTexts.hair_color_selected,
      1: glRegTexts.hair_color_1,
      2: glRegTexts.hair_color_2,
      3: glRegTexts.hair_color_3,
      4: glRegTexts.hair_color_4,
      5: glRegTexts.hair_color_5,
      6: glRegTexts.hair_color_6,
      7: glRegTexts.hair_color_7
    },
    hairLength1: {
      0: glRegTexts.hair_length_selected,
      1: glRegTexts.hair_length_1,
      2: glRegTexts.hair_length_2,
      3: glRegTexts.hair_length_3,
      4: glRegTexts.hair_length_4
    },
    eyesColor1: {
      0: glRegTexts.eyes_color_selected,
      1: glRegTexts.eyes_color_1,
      2: glRegTexts.eyes_color_2,
      3: glRegTexts.eyes_color_3,
      4: glRegTexts.eyes_color_4,
      5: glRegTexts.eyes_color_5,
      6: glRegTexts.eyes_color_6,
      7: glRegTexts.eyes_color_7,
      8: glRegTexts.eyes_color_8,
      9: glRegTexts.eyes_color_9
    },
    aboutUs: loadAbout(),
    defaults: { size1: 0 }
  });
  ipxRegAPI.trBulk({
    dayOfBirth1: glRegTexts.day_birth_selected,
    monthOfBirth1: glRegTexts.month_birth_selected,
    yearOfBirth1: glRegTexts.year_birth_selected
  });
  ipxRegAPI.setVar("regid", 20002);
  ipxRegAPI.setVar("validate_email_backend", true);
  ipxRegAPI.loadTranslatedCountriesToContainer("country", "region");
  ipxRegAPI.loadStuffToContainer();
  return true;
}
function loadSize() {
  var size1Values = [];
  var size1Object = ipxRegAPI.generateSize1Values();
  for (var key in size1Object) {
    if (!size1Object.hasOwnProperty(key)) continue;
    var obj = size1Object[key];
    size1Values[obj.code] = obj.translation;
  }
  size1Values[0] = glRegTexts.selected_size;
  return size1Values;
}
function loadAbout() {
  var about = {
    0: glRegTexts.about_selected,
    radio: glRegTexts.about_1,
    billboards: glRegTexts.about_2,
    comparison: glRegTexts.about_3,
    recommendation: glRegTexts.about_4,
    newspaper: glRegTexts.about_5,
    television: glRegTexts.about_6,
    online: glRegTexts.about_7,
    other: glRegTexts.about_8
  };
  if (
    ipxRegAPI.data.cc == "de" ||
    ipxRegAPI.data.cc == "de-AT" ||
    ipxRegAPI.data.cc == "de-CH"
  ) {
    about.truck = glRegTexts.about_9;
  }
  return about;
}
ipx_luxembourg_urlParam = function(name) {
  var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
    window.location.href
  );
  if (results == null) {
    return null;
  } else {
    return results[1] || 0;
  }
};
jQuery(document).ready(function($) {
  $(".open-overlay")
    .off("click touchstart")
    .on("click touchstart", function(e) {
      e.preventDefault();
      $overlay = $("#" + $(this).attr("data-overlay"));
      $box = $overlay.find(".box");
      $overlay.fadeIn(300), $box.fadeIn(300);
      $("body").addClass("noscroll");
      $overlay
        .find(".close-overlay")
        .off("click touchstart")
        .on("click touchstart", function(e) {
          e.preventDefault(), $box.fadeOut(300), $overlay.fadeOut(300);
          $("body").removeClass("noscroll");
        });
    });
  var myArg = ipx_luxembourg_urlParam("ip") || "";
  if ($("#luxembourg-overlay").length) {
    jQuery
      .get(
        "/sites/findedeinabenteuer/modules/ipx_luxembourg/ipx_luxembourg_ws.php?ip=" +
          myArg,
        function(data) {}
      )
      .done(function(data) {
        var objResult = JSON.parse(data);
        if (objResult.show) {
          var $luxOverlay = $("body").find("#luxembourg-overlay");
          $luxOverlay.find("#ok-btn").on("click", function(event) {
            event = event || null;
            if (event) event.preventDefault();
            $luxOverlay.find(".close-overlay").trigger("click");
          });
          $luxOverlay.find("#close-link").on("click", function(event) {
            event = event || null;
            if (event) event.preventDefault();
            $luxOverlay.find(".close-overlay").trigger("click");
          });
          $luxOverlay.find("#luxembourg").trigger("click");
        } else {
        }
      });
  }
});
/*!jquery.cookie v1.4.1 | MIT*/ !(function(a) {
  "function" == typeof define && define.amd
    ? define(["jquery"], a)
    : "object" == typeof exports
    ? a(require("jquery"))
    : a(jQuery);
})(function(a) {
  function b(a) {
    return h.raw ? a : encodeURIComponent(a);
  }
  function c(a) {
    return h.raw ? a : decodeURIComponent(a);
  }
  function d(a) {
    return b(h.json ? JSON.stringify(a) : String(a));
  }
  function e(a) {
    0 === a.indexOf('"') &&
      (a = a
        .slice(1, -1)
        .replace(/\\"/g, '"')
        .replace(/\\\\/g, "\\"));
    try {
      return (
        (a = decodeURIComponent(a.replace(g, " "))), h.json ? JSON.parse(a) : a
      );
    } catch (b) {}
  }
  function f(b, c) {
    var d = h.raw ? b : e(b);
    return a.isFunction(c) ? c(d) : d;
  }
  var g = /\+/g,
    h = (a.cookie = function(e, g, i) {
      if (void 0 !== g && !a.isFunction(g)) {
        if (((i = a.extend({}, h.defaults, i)), "number" == typeof i.expires)) {
          var j = i.expires,
            k = (i.expires = new Date());
          k.setTime(+k + 864e5 * j);
        }
        return (document.cookie = [
          b(e),
          "=",
          d(g),
          i.expires ? "; expires=" + i.expires.toUTCString() : "",
          i.path ? "; path=" + i.path : "",
          i.domain ? "; domain=" + i.domain : "",
          i.secure ? "; secure" : ""
        ].join(""));
      }
      for (
        var l = e ? void 0 : {},
          m = document.cookie ? document.cookie.split("; ") : [],
          n = 0,
          o = m.length;
        o > n;
        n++
      ) {
        var p = m[n].split("="),
          q = c(p.shift()),
          r = p.join("=");
        if (e && e === q) {
          l = f(r, g);
          break;
        }
        e || void 0 === (r = f(r)) || (l[q] = r);
      }
      return l;
    });
  (h.defaults = {}),
    (a.removeCookie = function(b, c) {
      return void 0 === a.cookie(b)
        ? !1
        : (a.cookie(b, "", a.extend({}, c, { expires: -1 })), !a.cookie(b));
    });
});
