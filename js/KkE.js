/*!formstone v1.4.3 [core.js] 2018-01-25 | GPL-3.0 License | formstone.it*/ !(function(
  e
) {
  "function" == typeof define && define.amd ? define(["jquery"], e) : e(jQuery);
})(function(e) {
  "use strict";
  function t(e, t, n, s) {
    var i,
      r = { raw: {} };
    s = s || {};
    for (i in s)
      s.hasOwnProperty(i) &&
        ("classes" === e
          ? ((r.raw[s[i]] = t + "-" + s[i]), (r[s[i]] = "." + t + "-" + s[i]))
          : ((r.raw[i] = s[i]), (r[i] = s[i] + "." + t)));
    for (i in n)
      n.hasOwnProperty(i) &&
        ("classes" === e
          ? ((r.raw[i] = n[i].replace(/{ns}/g, t)),
            (r[i] = n[i].replace(/{ns}/g, "." + t)))
          : ((r.raw[i] = n[i].replace(/.{ns}/g, "")),
            (r[i] = n[i].replace(/{ns}/g, t))));
    return r;
  }
  function n() {
    (p.windowWidth = p.$window.width()),
      (p.windowHeight = p.$window.height()),
      (g = f.startTimer(g, y, s));
  }
  function s() {
    for (var e in p.ResizeHandlers)
      p.ResizeHandlers.hasOwnProperty(e) &&
        p.ResizeHandlers[e].callback.call(
          window,
          p.windowWidth,
          p.windowHeight
        );
  }
  function i() {
    if (p.support.raf) {
      p.window.requestAnimationFrame(i);
      for (var e in p.RAFHandlers)
        p.RAFHandlers.hasOwnProperty(e) &&
          p.RAFHandlers[e].callback.call(window);
    }
  }
  function r(e, t) {
    return parseInt(e.priority) - parseInt(t.priority);
  }
  var o,
    a,
    c,
    l = "undefined" != typeof window ? window : this,
    u = l.document,
    d = function() {
      (this.Version = "@version"),
        (this.Plugins = {}),
        (this.DontConflict = !1),
        (this.Conflicts = { fn: {} }),
        (this.ResizeHandlers = []),
        (this.RAFHandlers = []),
        (this.window = l),
        (this.$window = e(l)),
        (this.document = u),
        (this.$document = e(u)),
        (this.$body = null),
        (this.windowWidth = 0),
        (this.windowHeight = 0),
        (this.fallbackWidth = 1024),
        (this.fallbackHeight = 768),
        (this.userAgent =
          window.navigator.userAgent ||
          window.navigator.vendor ||
          window.opera),
        (this.isFirefox = /Firefox/i.test(this.userAgent)),
        (this.isChrome = /Chrome/i.test(this.userAgent)),
        (this.isSafari = /Safari/i.test(this.userAgent) && !this.isChrome),
        (this.isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(
          this.userAgent
        )),
        (this.isIEMobile = /IEMobile/i.test(this.userAgent)),
        (this.isFirefoxMobile = this.isFirefox && this.isMobile),
        (this.transform = null),
        (this.transition = null),
        (this.support = {
          file: !!(window.File && window.FileList && window.FileReader),
          history: !!(
            window.history &&
            window.history.pushState &&
            window.history.replaceState
          ),
          matchMedia: !(!window.matchMedia && !window.msMatchMedia),
          pointer: !!window.PointerEvent,
          raf: !(!window.requestAnimationFrame || !window.cancelAnimationFrame),
          touch: !!(
            "ontouchstart" in window ||
            (window.DocumentTouch && document instanceof window.DocumentTouch)
          ),
          transition: !1,
          transform: !1
        });
    },
    f = {
      killEvent: function(e, t) {
        try {
          e.preventDefault(),
            e.stopPropagation(),
            t && e.stopImmediatePropagation();
        } catch (e) {}
      },
      killGesture: function(e) {
        try {
          e.preventDefault();
        } catch (e) {}
      },
      lockViewport: function(t) {
        (v[t] = !0),
          e.isEmptyObject(v) ||
            b ||
            (o.length
              ? o.attr("content", c)
              : (o = e("head").append(
                  '<meta name="viewport" content="' + c + '">'
                )),
            p.$body
              .on(m.gestureChange, f.killGesture)
              .on(m.gestureStart, f.killGesture)
              .on(m.gestureEnd, f.killGesture),
            (b = !0));
      },
      unlockViewport: function(t) {
        "undefined" !== e.type(v[t]) && delete v[t],
          e.isEmptyObject(v) &&
            b &&
            (o.length && (a ? o.attr("content", a) : o.remove()),
            p.$body
              .off(m.gestureChange)
              .off(m.gestureStart)
              .off(m.gestureEnd),
            (b = !1));
      },
      startTimer: function(e, t, n, s) {
        return f.clearTimer(e), s ? setInterval(n, t) : setTimeout(n, t);
      },
      clearTimer: function(e, t) {
        e && (t ? clearInterval(e) : clearTimeout(e), (e = null));
      },
      sortAsc: function(e, t) {
        return parseInt(e, 10) - parseInt(t, 10);
      },
      sortDesc: function(e, t) {
        return parseInt(t, 10) - parseInt(e, 10);
      },
      decodeEntities: function(e) {
        var t = p.document.createElement("textarea");
        return (t.innerHTML = e), t.value;
      },
      parseQueryString: function(e) {
        for (
          var t = {}, n = e.slice(e.indexOf("?") + 1).split("&"), s = 0;
          s < n.length;
          s++
        ) {
          var i = n[s].split("=");
          t[i[0]] = i[1];
        }
        return t;
      }
    },
    p = new d(),
    h = e.Deferred(),
    w = { base: "{ns}", element: "{ns}-element" },
    m = {
      namespace: ".{ns}",
      beforeUnload: "beforeunload.{ns}",
      blur: "blur.{ns}",
      change: "change.{ns}",
      click: "click.{ns}",
      dblClick: "dblclick.{ns}",
      drag: "drag.{ns}",
      dragEnd: "dragend.{ns}",
      dragEnter: "dragenter.{ns}",
      dragLeave: "dragleave.{ns}",
      dragOver: "dragover.{ns}",
      dragStart: "dragstart.{ns}",
      drop: "drop.{ns}",
      error: "error.{ns}",
      focus: "focus.{ns}",
      focusIn: "focusin.{ns}",
      focusOut: "focusout.{ns}",
      gestureChange: "gesturechange.{ns}",
      gestureStart: "gesturestart.{ns}",
      gestureEnd: "gestureend.{ns}",
      input: "input.{ns}",
      keyDown: "keydown.{ns}",
      keyPress: "keypress.{ns}",
      keyUp: "keyup.{ns}",
      load: "load.{ns}",
      mouseDown: "mousedown.{ns}",
      mouseEnter: "mouseenter.{ns}",
      mouseLeave: "mouseleave.{ns}",
      mouseMove: "mousemove.{ns}",
      mouseOut: "mouseout.{ns}",
      mouseOver: "mouseover.{ns}",
      mouseUp: "mouseup.{ns}",
      panStart: "panstart.{ns}",
      pan: "pan.{ns}",
      panEnd: "panend.{ns}",
      resize: "resize.{ns}",
      scaleStart: "scalestart.{ns}",
      scaleEnd: "scaleend.{ns}",
      scale: "scale.{ns}",
      scroll: "scroll.{ns}",
      select: "select.{ns}",
      swipe: "swipe.{ns}",
      touchCancel: "touchcancel.{ns}",
      touchEnd: "touchend.{ns}",
      touchLeave: "touchleave.{ns}",
      touchMove: "touchmove.{ns}",
      touchStart: "touchstart.{ns}"
    },
    g = null,
    y = 20,
    v = [],
    b = !1;
  return (
    (d.prototype.NoConflict = function() {
      p.DontConflict = !0;
      for (var t in p.Plugins)
        p.Plugins.hasOwnProperty(t) &&
          ((e[t] = p.Conflicts[t]), (e.fn[t] = p.Conflicts.fn[t]));
    }),
    (d.prototype.Ready = function(e) {
      "complete" === p.document.readyState ||
      ("loading" !== p.document.readyState &&
        !p.document.documentElement.doScroll)
        ? e()
        : p.document.addEventListener("DOMContentLoaded", e);
    }),
    (d.prototype.Plugin = function(n, s) {
      return (
        (p.Plugins[n] = (function(n, s) {
          function i(t) {
            var i,
              r,
              a,
              l = "object" === e.type(t),
              u = Array.prototype.slice.call(arguments, l ? 1 : 0),
              d = this,
              f = e();
            for (
              t = e.extend(!0, {}, s.defaults || {}, l ? t : {}),
                r = 0,
                a = d.length;
              r < a;
              r++
            )
              if (((i = d.eq(r)), !o(i))) {
                s.guid++;
                var p = "__" + s.guid,
                  h = s.classes.raw.base + p,
                  w = i.data(n + "-options"),
                  m = e.extend(
                    !0,
                    {
                      $el: i,
                      guid: p,
                      numGuid: s.guid,
                      rawGuid: h,
                      dotGuid: "." + h
                    },
                    t,
                    "object" === e.type(w) ? w : {}
                  );
                i.addClass(s.classes.raw.element).data(c, m),
                  s.methods._construct.apply(i, [m].concat(u)),
                  (f = f.add(i));
              }
            for (r = 0, a = f.length; r < a; r++)
              (i = f.eq(r)), s.methods._postConstruct.apply(i, [o(i)]);
            return d;
          }
          function o(e) {
            return e.data(c);
          }
          var a = "fs-" + n,
            c =
              "fs" +
              n.replace(/(^|\s)([a-z])/g, function(e, t, n) {
                return t + n.toUpperCase();
              });
          return (
            (s.initialized = !1),
            (s.priority = s.priority || 10),
            (s.classes = t("classes", a, w, s.classes)),
            (s.events = t("events", n, m, s.events)),
            (s.functions = e.extend(
              {
                getData: o,
                iterate: function(t) {
                  for (
                    var n = this,
                      s = Array.prototype.slice.call(arguments, 1),
                      i = 0,
                      r = n.length;
                    i < r;
                    i++
                  ) {
                    var a = n.eq(i),
                      c = o(a) || {};
                    "undefined" !== e.type(c.$el) && t.apply(a, [c].concat(s));
                  }
                  return n;
                }
              },
              f,
              s.functions
            )),
            (s.methods = e.extend(
              !0,
              {
                _construct: e.noop,
                _postConstruct: e.noop,
                _destruct: e.noop,
                _resize: !1,
                destroy: function(e) {
                  s.functions.iterate.apply(
                    this,
                    [s.methods._destruct].concat(
                      Array.prototype.slice.call(arguments, 1)
                    )
                  ),
                    this.removeClass(s.classes.raw.element).removeData(c);
                }
              },
              s.methods
            )),
            (s.utilities = e.extend(
              !0,
              {
                _initialize: !1,
                _delegate: !1,
                defaults: function(t) {
                  s.defaults = e.extend(!0, s.defaults, t || {});
                }
              },
              s.utilities
            )),
            s.widget &&
              ((p.Conflicts.fn[n] = e.fn[n]),
              (e.fn[c] = function(t) {
                if (this instanceof e) {
                  var n = s.methods[t];
                  if ("object" === e.type(t) || !t)
                    return i.apply(this, arguments);
                  if (n && 0 !== t.indexOf("_")) {
                    var r = [n].concat(
                      Array.prototype.slice.call(arguments, 1)
                    );
                    return s.functions.iterate.apply(this, r);
                  }
                  return this;
                }
              }),
              p.DontConflict || (e.fn[n] = e.fn[c])),
            (p.Conflicts[n] = e[n]),
            (e[c] =
              s.utilities._delegate ||
              function(t) {
                var n = s.utilities[t] || s.utilities._initialize || !1;
                if (n) {
                  var i = Array.prototype.slice.call(
                    arguments,
                    "object" === e.type(t) ? 0 : 1
                  );
                  return n.apply(window, i);
                }
              }),
            p.DontConflict || (e[n] = e[c]),
            (s.namespace = n),
            (s.namespaceClean = c),
            (s.guid = 0),
            s.methods._resize &&
              (p.ResizeHandlers.push({
                namespace: n,
                priority: s.priority,
                callback: s.methods._resize
              }),
              p.ResizeHandlers.sort(r)),
            s.methods._raf &&
              (p.RAFHandlers.push({
                namespace: n,
                priority: s.priority,
                callback: s.methods._raf
              }),
              p.RAFHandlers.sort(r)),
            s
          );
        })(n, s)),
        p.Plugins[n]
      );
    }),
    p.$window.on("resize.fs", n),
    n(),
    i(),
    p.Ready(function() {
      (p.$body = e("body")),
        e("html").addClass(p.support.touch ? "touchevents" : "no-touchevents"),
        (o = e('meta[name="viewport"]')),
        (a = !!o.length && o.attr("content")),
        (c =
          "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"),
        h.resolve();
    }),
    (m.clickTouchStart = m.click + " " + m.touchStart),
    (function() {
      var e,
        t = {
          WebkitTransition: "webkitTransitionEnd",
          MozTransition: "transitionend",
          OTransition: "otransitionend",
          transition: "transitionend"
        },
        n = ["transition", "-webkit-transition"],
        s = {
          transform: "transform",
          MozTransform: "-moz-transform",
          OTransform: "-o-transform",
          msTransform: "-ms-transform",
          webkitTransform: "-webkit-transform"
        },
        i = "transitionend",
        r = "",
        o = "",
        a = document.createElement("div");
      for (e in t)
        if (t.hasOwnProperty(e) && e in a.style) {
          (i = t[e]), (p.support.transition = !0);
          break;
        }
      m.transitionEnd = i + ".{ns}";
      for (e in n)
        if (n.hasOwnProperty(e) && n[e] in a.style) {
          r = n[e];
          break;
        }
      p.transition = r;
      for (e in s)
        if (s.hasOwnProperty(e) && s[e] in a.style) {
          (p.support.transform = !0), (o = s[e]);
          break;
        }
      p.transform = o;
    })(),
    (window.Formstone = p),
    p
  );
});
/*!formstone v1.4.3 [dropdown.js] 2018-02-02 | GPL-3.0 License | formstone.it*/ !(function(
  e
) {
  "function" == typeof define && define.amd
    ? define(["jquery", "./core", "./scrollbar", "./touch"], e)
    : e(jQuery, Formstone);
})(function(e, t) {
  "use strict";
  function l(t) {
    for (var l = "", i = 0, s = t.$allOptions.length; i < s; i++) {
      var o = t.$allOptions.eq(i),
        a = [];
      if ("OPTGROUP" === o[0].tagName)
        a.push(w.group),
          o.prop("disabled") && a.push(w.disabled),
          (l +=
            '<span class="' + a.join(" ") + '">' + o.attr("label") + "</span>");
      else {
        var d = o.val(),
          n = o.data("label"),
          r = t.links ? "a" : 'button type="button"';
        o.attr("value") || o.attr("value", d),
          a.push(w.item),
          o.hasClass(w.item_placeholder) &&
            (a.push(w.item_placeholder), (r = "span")),
          o.prop("selected") && a.push(w.item_selected),
          o.prop("disabled") && a.push(w.item_disabled),
          (l += "<" + r + ' class="' + a.join(" ") + '"'),
          t.links
            ? "span" === r
              ? (l += ' aria-hidden="true"')
              : ((l += ' href="' + d + '"'),
                t.external && (l += ' target="_blank"'))
            : (l += ' data-value="' + d + '"'),
          (l += ' role="option"'),
          o.prop("selected") && (l += ' "aria-selected"="true"'),
          (l += ">"),
          (l += n || C.decodeEntities(v(o.text(), t.trim))),
          (l += "</" + r + ">"),
          0;
      }
    }
    t.$items = t.$wrapper.html(e.parseHTML(l)).find(x.item);
  }
  function i(e) {
    C.killEvent(e);
    var t = e.data;
    t.disabled || t.useNative || (t.closed ? o(t) : a(t)), s(t);
  }
  function s(t) {
    e(x.base)
      .not(t.$dropdown)
      .trigger(g.close, [t]);
  }
  function o(e) {
    if (e.closed) {
      var t = k.height(),
        l = e.$wrapper.outerHeight(!0);
      e.$dropdown[0].getBoundingClientRect().bottom + l > t - e.bottomEdge &&
        e.$dropdown.addClass(w.bottom),
        _.on(g.click + e.dotGuid, ":not(" + x.options + ")", e, d),
        e.$dropdown.trigger(g.focusIn),
        e.$dropdown.addClass(w.open),
        b(e),
        (e.closed = !1);
    }
  }
  function a(e) {
    e &&
      !e.closed &&
      (_.off(g.click + e.dotGuid),
      e.$dropdown.removeClass([w.open, w.bottom].join(" ")),
      (e.closed = !0));
  }
  function d(t) {
    C.killEvent(t);
    var l = t.data;
    l &&
      0 === e(t.currentTarget).parents(x.base).length &&
      (a(l), l.$dropdown.trigger(g.focusOut));
  }
  function n(e) {
    var t = e.data;
    t && (a(t), t.$dropdown.trigger(g.focusOut));
  }
  function r(t) {
    var l = e(this),
      i = t.data;
    if ((C.killEvent(t), !i.disabled)) {
      var s = i.$items.index(l);
      (i.focusIndex = s),
        i.$wrapper.is(":visible") &&
          (m(s, i, t.shiftKey, t.metaKey || t.ctrlKey), $(i)),
        i.multiple || a(i),
        i.$dropdown.trigger(g.focus);
    }
  }
  function p(t, l) {
    e(this);
    var i = t.data;
    if (!l && !i.multiple) {
      var s = i.$options.index(i.$options.filter(":selected"));
      (i.focusIndex = s), m(s, i), $(i, !0);
    }
  }
  function c(t) {
    C.killEvent(t);
    e(t.currentTarget);
    var l = t.data;
    l.disabled ||
      l.multiple ||
      l.focused ||
      (s(l),
      (l.focused = !0),
      (l.focusIndex = l.index),
      (l.input = ""),
      l.$dropdown.addClass(w.focus).on(g.keyDown + l.dotGuid, l, f));
  }
  function u(t) {
    C.killEvent(t);
    e(t.currentTarget);
    var l = t.data;
    l.focused &&
      l.closed &&
      ((l.focused = !1),
      l.$dropdown.removeClass(w.focus).off(g.keyDown + l.dotGuid),
      l.multiple ||
        (a(l), l.index !== l.focusIndex && ($(l), (l.focusIndex = l.index))));
  }
  function f(l) {
    var i = l.data;
    if (
      ((i.keyTimer = C.startTimer(i.keyTimer, 1e3, function() {
        i.input = "";
      })),
      13 === l.keyCode)
    )
      i.closed || (a(i), m(i.index, i)), $(i);
    else if (
      !(9 === l.keyCode || l.metaKey || l.altKey || l.ctrlKey || l.shiftKey)
    ) {
      C.killEvent(l);
      var s = i.$items.length - 1,
        o = i.index < 0 ? 0 : i.index;
      if (e.inArray(l.keyCode, t.isFirefox ? [38, 40, 37, 39] : [38, 40]) > -1)
        (o += 38 === l.keyCode || (t.isFirefox && 37 === l.keyCode) ? -1 : 1) <
          0 && (o = 0),
          o > s && (o = s);
      else {
        var d,
          n = String.fromCharCode(l.keyCode).toUpperCase();
        for (i.input += n, d = i.index + 1; d <= s; d++)
          if (
            i.$options
              .eq(d)
              .text()
              .substr(0, i.input.length)
              .toUpperCase() === i.input
          ) {
            o = d;
            break;
          }
        if (o < 0 || o === i.index)
          for (d = 0; d <= s; d++)
            if (
              i.$options
                .eq(d)
                .text()
                .substr(0, i.input.length)
                .toUpperCase() === i.input
            ) {
              o = d;
              break;
            }
      }
      o >= 0 && (m(o, i), b(i));
    }
  }
  function m(e, t, l, i) {
    var s = t.$items.eq(e),
      o = t.$options.eq(e),
      a = s.hasClass(w.item_selected);
    if (!s.hasClass(w.item_disabled))
      if (t.multiple)
        if (t.useNative)
          a
            ? (o.prop("selected", null).attr("aria-selected", null),
              s.removeClass(w.item_selected))
            : (o.prop("selected", !0).attr("aria-selected", !0),
              s.addClass(w.item_selected));
        else if (l && !1 !== t.lastIndex) {
          var d = t.lastIndex > e ? e : t.lastIndex,
            n = (t.lastIndex > e ? t.lastIndex : e) + 1;
          t.$options.prop("selected", null).attr("aria-selected", null),
            t.$items.filter(x.item_selected).removeClass(w.item_selected),
            t.$options
              .slice(d, n)
              .not("[disabled]")
              .prop("selected", !0),
            t.$items
              .slice(d, n)
              .not(x.item_disabled)
              .addClass(w.item_selected);
        } else
          i || t.selectMultiple
            ? (a
                ? (o.prop("selected", null).attr("aria-selected", null),
                  s.removeClass(w.item_selected))
                : (o.prop("selected", !0).attr("aria-selected", !0),
                  s.addClass(w.item_selected)),
              (t.lastIndex = e))
            : (t.$options.prop("selected", null).attr("aria-selected", null),
              t.$items.filter(x.item_selected).removeClass(w.item_selected),
              o.prop("selected", !0).attr("aria-selected", !0),
              s.addClass(w.item_selected),
              (t.lastIndex = e));
      else if (e > -1 && e < t.$items.length) {
        if (e !== t.index) {
          var r = o.data("label") || s.html();
          t.$selected.html(r).removeClass(x.item_placeholder),
            t.$items.filter(x.item_selected).removeClass(w.item_selected),
            (t.$el[0].selectedIndex = e),
            s.addClass(w.item_selected),
            (t.index = e);
        }
      } else "" !== t.label && t.$selected.html(t.label);
  }
  function b(t) {
    var l = t.$items.eq(t.index),
      i =
        t.index >= 0 && !l.hasClass(w.item_placeholder)
          ? l.position()
          : { left: 0, top: 0 },
      s = (t.$wrapper.outerHeight() - l.outerHeight()) / 2;
    void 0 !== e.fn.fsScrollbar
      ? t.$wrapper
          .fsScrollbar("resize")
          .fsScrollbar(
            "scroll",
            t.$wrapper.find(".fs-scrollbar-content").scrollTop() + i.top
          )
      : t.$wrapper.scrollTop(t.$wrapper.scrollTop() + i.top - s);
  }
  function $(e, t) {
    var l, i;
    e.links
      ? ((i = (l = e).$el.val()),
        l.external ? I.open(i) : (I.location.href = i))
      : t || e.$el.trigger(g.raw.change, [!0]);
  }
  function v(e, t) {
    return 0 === t ? e : e.length > t ? e.substring(0, t) + "..." : e;
  }
  var h = t.Plugin("dropdown", {
      widget: !0,
      defaults: {
        bottomEdge: 0,
        cover: !1,
        customClass: "",
        label: "",
        external: !1,
        links: !1,
        mobile: !1,
        native: !1,
        theme: "fs-light",
        trim: 0,
        selectMultiple: !1
      },
      methods: {
        _construct: function(s) {
          (s.multiple = this.prop("multiple")),
            (s.disabled = this.prop("disabled") || this.is("[readonly]")),
            (s.lastIndex = !1),
            (s.native = s.mobile || s.native),
            (s.useNative = s.native || t.isMobile),
            s.multiple ? (s.links = !1) : s.external && (s.links = !0);
          var o = this.find("[selected]").not(":disabled"),
            a = this.find(":selected").not(":disabled"),
            d = a.text(),
            f = this.find("option").index(a);
          s.multiple || "" === s.label || o.length
            ? (s.label = "")
            : ((a = this.prepend(
                '<option value="" class="' +
                  w.item_placeholder +
                  '" selected>' +
                  s.label +
                  "</option>"
              )),
              (d = s.label),
              (f = 0));
          var b = this.find("option, optgroup"),
            $ = b.filter("option"),
            h = e('[for="' + this.attr("id") + '"]');
          (s.tabIndex = this[0].tabIndex),
            (this[0].tabIndex = -1),
            h.length && (h[0].tabIndex = -1);
          var C = [w.base, s.theme, s.customClass];
          s.useNative ? C.push(w.native) : s.cover && C.push(w.cover),
            s.multiple && C.push(w.multiple),
            s.disabled && C.push(w.disabled),
            (s.id = this.attr("id")),
            s.id ? (s.ariaId = s.id) : (s.ariaId = s.rawGuid),
            (s.ariaId += "-dropdown"),
            (s.selectedAriaId = s.ariaId + "-selected");
          var I = "",
            k = "";
          (I +=
            '<div class="' +
            C.join(" ") +
            '"id="' +
            s.ariaId +
            '" tabindex="' +
            s.tabIndex +
            '" role="listbox"'),
            s.multiple
              ? (I += ' aria-label="multi select"')
              : (I +=
                  ' aria-haspopup="true" aria-live="polite" aria-labelledby="' +
                  s.selectedAriaId +
                  '"'),
            (I += "></div>"),
            s.multiple ||
              ((k +=
                '<button type="button" class="' +
                w.selected +
                '" id="' +
                s.selectedAriaId +
                '" tabindex="-1">'),
              (k += e("<span></span>")
                .text(v(d, s.trim))
                .html()),
              (k += "</button>")),
            (k += '<div class="' + w.options + '">'),
            (k += "</div>"),
            this.wrap(I).after(k),
            (s.$dropdown = this.parent(x.base)),
            (s.$label = h),
            (s.$allOptions = b),
            (s.$options = $),
            (s.$selected = s.$dropdown.find(x.selected)),
            (s.$wrapper = s.$dropdown.find(x.options)),
            (s.$placeholder = s.$dropdown.find(x.placeholder)),
            (s.index = -1),
            (s.closed = !0),
            (s.focused = !1),
            l(s),
            s.multiple || m(f, s),
            void 0 !== e.fn.fsScrollbar &&
              s.$wrapper
                .fsScrollbar({ theme: s.theme })
                .find(".fs-scrollbar-content")
                .attr("tabindex", null),
            s.$dropdown.on(g.click, s, i),
            s.$selected.on(g.click, s, i),
            s.$dropdown.on(g.click, x.item, s, r).on(g.close, s, n),
            this.on(g.change, s, p),
            s.useNative ||
              (this.on(g.focusIn, s, function(e) {
                e.data.$dropdown.trigger(g.raw.focus);
              }),
              s.$dropdown.on(g.focusIn, s, c).on(g.focusOut, s, u));
        },
        _destruct: function(t) {
          t.$dropdown.hasClass(w.open) && t.$selected.trigger(g.click),
            void 0 !== e.fn.fsScrollbar && t.$wrapper.fsScrollbar("destroy"),
            (t.$el[0].tabIndex = t.tabIndex),
            t.$label.length && (t.$label[0].tabIndex = t.tabIndex),
            t.$dropdown.off(g.namespace),
            t.$options.off(g.namespace),
            t.$placeholder.remove(),
            t.$selected.remove(),
            t.$wrapper.remove(),
            t.$el
              .off(g.namespace)
              .show()
              .unwrap();
        },
        disable: function(e, t) {
          if (void 0 !== t) {
            var l = e.$items.index(e.$items.filter("[data-value=" + t + "]"));
            e.$items.eq(l).addClass(w.item_disabled),
              e.$options.eq(l).prop("disabled", !0);
          } else
            e.$dropdown.hasClass(w.open) && e.$selected.trigger(g.click),
              e.$dropdown.addClass(w.disabled),
              e.$el.prop("disabled", !0),
              (e.disabled = !0);
        },
        enable: function(e, t) {
          if (void 0 !== t) {
            var l = e.$items.index(e.$items.filter("[data-value=" + t + "]"));
            e.$items.eq(l).removeClass(w.item_disabled),
              e.$options.eq(l).prop("disabled", !1);
          } else
            e.$dropdown.removeClass(w.disabled),
              e.$el.prop("disabled", !1),
              (e.disabled = !1);
        },
        update: function(t) {
          void 0 !== e.fn.fsScrollbar && t.$wrapper.fsScrollbar("destroy");
          var i = t.index;
          (t.$allOptions = t.$el.find("option, optgroup")),
            (t.$options = t.$allOptions.filter("option")),
            (t.index = -1),
            (i = t.$options.index(t.$options.filter(":selected"))),
            l(t),
            t.multiple || m(i, t),
            void 0 !== e.fn.fsScrollbar &&
              t.$wrapper
                .fsScrollbar({ theme: t.theme })
                .find(".fs-scrollbar-content")
                .attr("tabindex", null);
        },
        open: o,
        close: a
      },
      classes: [
        "cover",
        "bottom",
        "multiple",
        "mobile",
        "native",
        "open",
        "disabled",
        "focus",
        "selected",
        "options",
        "group",
        "item",
        "item_disabled",
        "item_selected",
        "item_placeholder"
      ],
      events: { close: "close" }
    }),
    x = h.classes,
    w = x.raw,
    g = h.events,
    C = h.functions,
    I = t.window,
    k = t.$window,
    _ = (t.document, null);
  t.Ready(function() {
    _ = t.$body;
  });
});
$(document).ready(function() {
  var $desktopTopstage = $(".buffered").parent();
  if ($("body").hasClass("video-bg")) {
    var uploaded = function() {
      var upload = true;
      if (vidfiles[0] === "" || vidfiles[3] === "") {
        upload = false;
      }
      return upload;
    };
    if (uploaded()) {
      if (!$("html").hasClass("mobile")) {
        vidbg(
          $desktopTopstage,
          [{ src: mp4Src, type: "mp4" }],
          overlay,
          function() {
            var $vid = $("#bg-video"),
              vidElement = $vid[0];
            vidElement.pause();
            var network = {
              i: 0,
              intervalGap: 1,
              hasStarted: false,
              buffPercent: [],
              speed: function() {
                var d = this.buffPercent[1] - this.buffPercent[0];
                var t = 350 * this.intervalGap;
                var v =
                  Math.floor((d / t) * 10000) === 0
                    ? Math.floor(this.buffPercent[1])
                    : Math.floor((d / t) * 10000);
                return v;
              },
              optBuffering: function() {
                var optimal;
                if (this.i === 2) {
                  var speed = this.speed();
                  optimal = 100 - speed > 0 ? 100 - speed : 0;
                  if (speed === Math.floor(this.buffPercent[1]))
                    optimal = Math.floor(this.buffPercent[1]);
                  if (optimal > 100) optimal = 100;
                } else {
                  optimal = 200;
                }
                return optimal;
              }
            };
            var vid_progress = setInterval(function() {
              var bufferedEnd;
              try {
                bufferedEnd = vidElement.buffered.end(
                  vidElement.buffered.length - 1
                );
              } catch (err) {
                return;
              }
              var bufferedEnd = vidElement.buffered.end(
                vidElement.buffered.length - 1
              );
              var duration = vidElement.duration;
              var percentBuffered = (bufferedEnd / duration) * 100;
              var i = network.i;
              if (i <= 1 && !isNaN(percentBuffered)) {
                network.buffPercent[i] = Math.floor(percentBuffered);
                if (i === 1) {
                  if (
                    network.buffPercent[0] === network.buffPercent[1] &&
                    Math.floor(bufferedEnd) != Math.floor(duration)
                  ) {
                    if (network.intervalGap > 10) {
                      network.i = 2;
                    } else {
                      network.intervalGap++;
                      network.i = 1;
                    }
                  } else {
                    if (duration < 12) {
                      var shortVidAdjust =
                        Math.round((12 / duration) * network.intervalGap) +
                        network.intervalGap;
                      network.intervalGap = shortVidAdjust;
                      network.i++;
                    } else network.i++;
                  }
                } else network.i++;
              }
              if (duration > 0 && !isNaN(duration)) {
                document.getElementById("buffered-amount").style.width =
                  percentBuffered + "%";
              }
              if (
                percentBuffered >= network.optBuffering() &&
                !network.hasStarted
              ) {
                vidElement.play();
                network.hasStarted = true;
              }
            }, 350);
            vidElement.addEventListener("onloadeddata", function() {
              vid_progress;
            });
            vidElement.addEventListener("timeupdate", function() {
              var duration = vidElement.duration;
              if (duration > 0) {
                document.getElementById("progress-amount").style.width =
                  (vidElement.currentTime / duration) * 100 + "%";
              }
            });
            vidElement.addEventListener("ended", function() {
              clearInterval(vid_progress);
              if (isLooped === "") {
              }
            });
            var debugVideo =
              window.location.search.indexOf("debug-video") != -1;
            if (debugVideo) {
              $(".buffered, .progress").show();
              debugNetwork = network;
            }
          }
        );
      }
    } else {
      $("body").removeClass("video-bg");
    }
  }
  function vidbg(element, sources, overlay, callback) {
    var container,
      item,
      html = [],
      index = sources.length;
    if (!document.createElement("video").canPlayType) {
      return;
    }
    html.push(
      '<video id="bg-video" autoplay="true" ' +
        isLooped +
        '  muted="muted" volume="0" poster="' +
        vidfiles[3] +
        '" >'
    );
    sources.reverse();
    while (index--) {
      item = sources[index];
      html.push('<source src="', item.src, '" type="video/', item.type, '" />');
    }
    html.push("</video>");
    if (overlay) {
      html.push('<div class="vidbg-overlay"></div>');
    }
    container = document.createElement("div");
    container.setAttribute("class", "vidbg");
    container.innerHTML = html.join("");
    element.append(container);
    if (callback && typeof callback === "function") {
      callback();
    }
  }
});
!(function(a, b, c, d) {
  function e(b, c) {
    (this.settings = null),
      (this.options = a.extend({}, e.Defaults, c)),
      (this.$element = a(b)),
      (this._handlers = {}),
      (this._plugins = {}),
      (this._supress = {}),
      (this._current = null),
      (this._speed = null),
      (this._coordinates = []),
      (this._breakpoint = null),
      (this._width = null),
      (this._items = []),
      (this._clones = []),
      (this._mergers = []),
      (this._widths = []),
      (this._invalidated = {}),
      (this._pipe = []),
      (this._drag = {
        time: null,
        target: null,
        pointer: null,
        stage: { start: null, current: null },
        direction: null
      }),
      (this._states = {
        current: {},
        tags: {
          initializing: ["busy"],
          animating: ["busy"],
          dragging: ["interacting"]
        }
      }),
      a.each(
        ["onResize", "onThrottledResize"],
        a.proxy(function(b, c) {
          this._handlers[c] = a.proxy(this[c], this);
        }, this)
      ),
      a.each(
        e.Plugins,
        a.proxy(function(a, b) {
          this._plugins[a.charAt(0).toLowerCase() + a.slice(1)] = new b(this);
        }, this)
      ),
      a.each(
        e.Workers,
        a.proxy(function(b, c) {
          this._pipe.push({ filter: c.filter, run: a.proxy(c.run, this) });
        }, this)
      ),
      this.setup(),
      this.initialize();
  }
  (e.Defaults = {
    items: 3,
    loop: !1,
    center: !1,
    rewind: !1,
    checkVisibility: !0,
    mouseDrag: !0,
    touchDrag: !0,
    pullDrag: !0,
    freeDrag: !1,
    margin: 0,
    stagePadding: 0,
    merge: !1,
    mergeFit: !0,
    autoWidth: !1,
    startPosition: 0,
    rtl: !1,
    smartSpeed: 250,
    fluidSpeed: !1,
    dragEndSpeed: !1,
    responsive: {},
    responsiveRefreshRate: 200,
    responsiveBaseElement: b,
    fallbackEasing: "swing",
    slideTransition: "",
    info: !1,
    nestedItemSelector: !1,
    itemElement: "div",
    stageElement: "div",
    refreshClass: "owl-refresh",
    loadedClass: "owl-loaded",
    loadingClass: "owl-loading",
    rtlClass: "owl-rtl",
    responsiveClass: "owl-responsive",
    dragClass: "owl-drag",
    itemClass: "owl-item",
    stageClass: "owl-stage",
    stageOuterClass: "owl-stage-outer",
    grabClass: "owl-grab"
  }),
    (e.Width = { Default: "default", Inner: "inner", Outer: "outer" }),
    (e.Type = { Event: "event", State: "state" }),
    (e.Plugins = {}),
    (e.Workers = [
      {
        filter: ["width", "settings"],
        run: function() {
          this._width = this.$element.width();
        }
      },
      {
        filter: ["width", "items", "settings"],
        run: function(a) {
          a.current = this._items && this._items[this.relative(this._current)];
        }
      },
      {
        filter: ["items", "settings"],
        run: function() {
          this.$stage.children(".cloned").remove();
        }
      },
      {
        filter: ["width", "items", "settings"],
        run: function(a) {
          var b = this.settings.margin || "",
            c = !this.settings.autoWidth,
            d = this.settings.rtl,
            e = {
              width: "auto",
              "margin-left": d ? b : "",
              "margin-right": d ? "" : b
            };
          !c && this.$stage.children().css(e), (a.css = e);
        }
      },
      {
        filter: ["width", "items", "settings"],
        run: function(a) {
          var b =
              (this.width() / this.settings.items).toFixed(3) -
              this.settings.margin,
            c = null,
            d = this._items.length,
            e = !this.settings.autoWidth,
            f = [];
          for (a.items = { merge: !1, width: b }; d--; )
            (c = this._mergers[d]),
              (c =
                (this.settings.mergeFit && Math.min(c, this.settings.items)) ||
                c),
              (a.items.merge = c > 1 || a.items.merge),
              (f[d] = e ? b * c : this._items[d].width());
          this._widths = f;
        }
      },
      {
        filter: ["items", "settings"],
        run: function() {
          var b = [],
            c = this._items,
            d = this.settings,
            e = Math.max(2 * d.items, 4),
            f = 2 * Math.ceil(c.length / 2),
            g = d.loop && c.length ? (d.rewind ? e : Math.max(e, f)) : 0,
            h = "",
            i = "";
          for (g /= 2; g > 0; )
            b.push(this.normalize(b.length / 2, !0)),
              (h += c[b[b.length - 1]][0].outerHTML),
              b.push(this.normalize(c.length - 1 - (b.length - 1) / 2, !0)),
              (i = c[b[b.length - 1]][0].outerHTML + i),
              (g -= 1);
          (this._clones = b),
            a(h)
              .addClass("cloned")
              .appendTo(this.$stage),
            a(i)
              .addClass("cloned")
              .prependTo(this.$stage);
        }
      },
      {
        filter: ["width", "items", "settings"],
        run: function() {
          for (
            var a = this.settings.rtl ? 1 : -1,
              b = this._clones.length + this._items.length,
              c = -1,
              d = 0,
              e = 0,
              f = [];
            ++c < b;

          )
            (d = f[c - 1] || 0),
              (e = this._widths[this.relative(c)] + this.settings.margin),
              f.push(d + e * a);
          this._coordinates = f;
        }
      },
      {
        filter: ["width", "items", "settings"],
        run: function() {
          var a = this.settings.stagePadding,
            b = this._coordinates,
            c = {
              width: Math.ceil(Math.abs(b[b.length - 1])) + 2 * a,
              "padding-left": a || "",
              "padding-right": a || ""
            };
          this.$stage.css(c);
        }
      },
      {
        filter: ["width", "items", "settings"],
        run: function(a) {
          var b = this._coordinates.length,
            c = !this.settings.autoWidth,
            d = this.$stage.children();
          if (c && a.items.merge)
            for (; b--; )
              (a.css.width = this._widths[this.relative(b)]),
                d.eq(b).css(a.css);
          else c && ((a.css.width = a.items.width), d.css(a.css));
        }
      },
      {
        filter: ["items"],
        run: function() {
          this._coordinates.length < 1 && this.$stage.removeAttr("style");
        }
      },
      {
        filter: ["width", "items", "settings"],
        run: function(a) {
          (a.current = a.current ? this.$stage.children().index(a.current) : 0),
            (a.current = Math.max(
              this.minimum(),
              Math.min(this.maximum(), a.current)
            )),
            this.reset(a.current);
        }
      },
      {
        filter: ["position"],
        run: function() {
          this.animate(this.coordinates(this._current));
        }
      },
      {
        filter: ["width", "position", "items", "settings"],
        run: function() {
          var a,
            b,
            c,
            d,
            e = this.settings.rtl ? 1 : -1,
            f = 2 * this.settings.stagePadding,
            g = this.coordinates(this.current()) + f,
            h = g + this.width() * e,
            i = [];
          for (c = 0, d = this._coordinates.length; c < d; c++)
            (a = this._coordinates[c - 1] || 0),
              (b = Math.abs(this._coordinates[c]) + f * e),
              ((this.op(a, "<=", g) && this.op(a, ">", h)) ||
                (this.op(b, "<", g) && this.op(b, ">", h))) &&
                i.push(c);
          this.$stage.children(".active").removeClass("active"),
            this.$stage
              .children(":eq(" + i.join("), :eq(") + ")")
              .addClass("active"),
            this.$stage.children(".center").removeClass("center"),
            this.settings.center &&
              this.$stage
                .children()
                .eq(this.current())
                .addClass("center");
        }
      }
    ]),
    (e.prototype.initializeStage = function() {
      (this.$stage = this.$element.find("." + this.settings.stageClass)),
        this.$stage.length ||
          (this.$element.addClass(this.options.loadingClass),
          (this.$stage = a("<" + this.settings.stageElement + ">", {
            class: this.settings.stageClass
          }).wrap(a("<div/>", { class: this.settings.stageOuterClass }))),
          this.$element.append(this.$stage.parent()));
    }),
    (e.prototype.initializeItems = function() {
      var b = this.$element.find(".owl-item");
      if (b.length)
        return (
          (this._items = b.get().map(function(b) {
            return a(b);
          })),
          (this._mergers = this._items.map(function() {
            return 1;
          })),
          void this.refresh()
        );
      this.replace(this.$element.children().not(this.$stage.parent())),
        this.isVisible() ? this.refresh() : this.invalidate("width"),
        this.$element
          .removeClass(this.options.loadingClass)
          .addClass(this.options.loadedClass);
    }),
    (e.prototype.initialize = function() {
      if (
        (this.enter("initializing"),
        this.trigger("initialize"),
        this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl),
        this.settings.autoWidth && !this.is("pre-loading"))
      ) {
        var a, b, c;
        (a = this.$element.find("img")),
          (b = this.settings.nestedItemSelector
            ? "." + this.settings.nestedItemSelector
            : d),
          (c = this.$element.children(b).width()),
          a.length && c <= 0 && this.preloadAutoWidthImages(a);
      }
      this.initializeStage(),
        this.initializeItems(),
        this.registerEventHandlers(),
        this.leave("initializing"),
        this.trigger("initialized");
    }),
    (e.prototype.isVisible = function() {
      return !this.settings.checkVisibility || this.$element.is(":visible");
    }),
    (e.prototype.setup = function() {
      var b = this.viewport(),
        c = this.options.responsive,
        d = -1,
        e = null;
      c
        ? (a.each(c, function(a) {
            a <= b && a > d && (d = Number(a));
          }),
          (e = a.extend({}, this.options, c[d])),
          "function" == typeof e.stagePadding &&
            (e.stagePadding = e.stagePadding()),
          delete e.responsive,
          e.responsiveClass &&
            this.$element.attr(
              "class",
              this.$element
                .attr("class")
                .replace(
                  new RegExp(
                    "(" + this.options.responsiveClass + "-)\\S+\\s",
                    "g"
                  ),
                  "$1" + d
                )
            ))
        : (e = a.extend({}, this.options)),
        this.trigger("change", { property: { name: "settings", value: e } }),
        (this._breakpoint = d),
        (this.settings = e),
        this.invalidate("settings"),
        this.trigger("changed", {
          property: { name: "settings", value: this.settings }
        });
    }),
    (e.prototype.optionsLogic = function() {
      this.settings.autoWidth &&
        ((this.settings.stagePadding = !1), (this.settings.merge = !1));
    }),
    (e.prototype.prepare = function(b) {
      var c = this.trigger("prepare", { content: b });
      return (
        c.data ||
          (c.data = a("<" + this.settings.itemElement + "/>")
            .addClass(this.options.itemClass)
            .append(b)),
        this.trigger("prepared", { content: c.data }),
        c.data
      );
    }),
    (e.prototype.update = function() {
      for (
        var b = 0,
          c = this._pipe.length,
          d = a.proxy(function(a) {
            return this[a];
          }, this._invalidated),
          e = {};
        b < c;

      )
        (this._invalidated.all || a.grep(this._pipe[b].filter, d).length > 0) &&
          this._pipe[b].run(e),
          b++;
      (this._invalidated = {}), !this.is("valid") && this.enter("valid");
    }),
    (e.prototype.width = function(a) {
      switch ((a = a || e.Width.Default)) {
        case e.Width.Inner:
        case e.Width.Outer:
          return this._width;
        default:
          return (
            this._width - 2 * this.settings.stagePadding + this.settings.margin
          );
      }
    }),
    (e.prototype.refresh = function() {
      this.enter("refreshing"),
        this.trigger("refresh"),
        this.setup(),
        this.optionsLogic(),
        this.$element.addClass(this.options.refreshClass),
        this.update(),
        this.$element.removeClass(this.options.refreshClass),
        this.leave("refreshing"),
        this.trigger("refreshed");
    }),
    (e.prototype.onThrottledResize = function() {
      b.clearTimeout(this.resizeTimer),
        (this.resizeTimer = b.setTimeout(
          this._handlers.onResize,
          this.settings.responsiveRefreshRate
        ));
    }),
    (e.prototype.onResize = function() {
      return (
        !!this._items.length &&
        this._width !== this.$element.width() &&
          !!this.isVisible() &&
            (this.enter("resizing"),
            this.trigger("resize").isDefaultPrevented()
              ? (this.leave("resizing"), !1)
              : (this.invalidate("width"),
                this.refresh(),
                this.leave("resizing"),
                void this.trigger("resized")))
      );
    }),
    (e.prototype.registerEventHandlers = function() {
      a.support.transition &&
        this.$stage.on(
          a.support.transition.end + ".owl.core",
          a.proxy(this.onTransitionEnd, this)
        ),
        !1 !== this.settings.responsive &&
          this.on(b, "resize", this._handlers.onThrottledResize),
        this.settings.mouseDrag &&
          (this.$element.addClass(this.options.dragClass),
          this.$stage.on("mousedown.owl.core", a.proxy(this.onDragStart, this)),
          this.$stage.on("dragstart.owl.core selectstart.owl.core", function() {
            return !1;
          })),
        this.settings.touchDrag &&
          (this.$stage.on(
            "touchstart.owl.core",
            a.proxy(this.onDragStart, this)
          ),
          this.$stage.on(
            "touchcancel.owl.core",
            a.proxy(this.onDragEnd, this)
          ));
    }),
    (e.prototype.onDragStart = function(b) {
      var d = null;
      3 !== b.which &&
        (a.support.transform
          ? ((d = this.$stage
              .css("transform")
              .replace(/.*\(|\)| /g, "")
              .split(",")),
            (d = {
              x: d[16 === d.length ? 12 : 4],
              y: d[16 === d.length ? 13 : 5]
            }))
          : ((d = this.$stage.position()),
            (d = {
              x: this.settings.rtl
                ? d.left +
                  this.$stage.width() -
                  this.width() +
                  this.settings.margin
                : d.left,
              y: d.top
            })),
        this.is("animating") &&
          (a.support.transform ? this.animate(d.x) : this.$stage.stop(),
          this.invalidate("position")),
        this.$element.toggleClass(
          this.options.grabClass,
          "mousedown" === b.type
        ),
        this.speed(0),
        (this._drag.time = new Date().getTime()),
        (this._drag.target = a(b.target)),
        (this._drag.stage.start = d),
        (this._drag.stage.current = d),
        (this._drag.pointer = this.pointer(b)),
        a(c).on(
          "mouseup.owl.core touchend.owl.core",
          a.proxy(this.onDragEnd, this)
        ),
        a(c).one(
          "mousemove.owl.core touchmove.owl.core",
          a.proxy(function(b) {
            var d = this.difference(this._drag.pointer, this.pointer(b));
            a(c).on(
              "mousemove.owl.core touchmove.owl.core",
              a.proxy(this.onDragMove, this)
            ),
              (Math.abs(d.x) < Math.abs(d.y) && this.is("valid")) ||
                (b.preventDefault(),
                this.enter("dragging"),
                this.trigger("drag"));
          }, this)
        ));
    }),
    (e.prototype.onDragMove = function(a) {
      var b = null,
        c = null,
        d = null,
        e = this.difference(this._drag.pointer, this.pointer(a)),
        f = this.difference(this._drag.stage.start, e);
      this.is("dragging") &&
        (a.preventDefault(),
        this.settings.loop
          ? ((b = this.coordinates(this.minimum())),
            (c = this.coordinates(this.maximum() + 1) - b),
            (f.x = ((((f.x - b) % c) + c) % c) + b))
          : ((b = this.settings.rtl
              ? this.coordinates(this.maximum())
              : this.coordinates(this.minimum())),
            (c = this.settings.rtl
              ? this.coordinates(this.minimum())
              : this.coordinates(this.maximum())),
            (d = this.settings.pullDrag ? (-1 * e.x) / 5 : 0),
            (f.x = Math.max(Math.min(f.x, b + d), c + d))),
        (this._drag.stage.current = f),
        this.animate(f.x));
    }),
    (e.prototype.onDragEnd = function(b) {
      var d = this.difference(this._drag.pointer, this.pointer(b)),
        e = this._drag.stage.current,
        f = (d.x > 0) ^ this.settings.rtl ? "left" : "right";
      a(c).off(".owl.core"),
        this.$element.removeClass(this.options.grabClass),
        ((0 !== d.x && this.is("dragging")) || !this.is("valid")) &&
          (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed),
          this.current(this.closest(e.x, 0 !== d.x ? f : this._drag.direction)),
          this.invalidate("position"),
          this.update(),
          (this._drag.direction = f),
          (Math.abs(d.x) > 3 || new Date().getTime() - this._drag.time > 300) &&
            this._drag.target.one("click.owl.core", function() {
              return !1;
            })),
        this.is("dragging") &&
          (this.leave("dragging"), this.trigger("dragged"));
    }),
    (e.prototype.closest = function(b, c) {
      var e = -1,
        f = 30,
        g = this.width(),
        h = this.coordinates();
      return (
        this.settings.freeDrag ||
          a.each(
            h,
            a.proxy(function(a, i) {
              return (
                "left" === c && b > i - f && b < i + f
                  ? (e = a)
                  : "right" === c && b > i - g - f && b < i - g + f
                  ? (e = a + 1)
                  : this.op(b, "<", i) &&
                    this.op(b, ">", h[a + 1] !== d ? h[a + 1] : i - g) &&
                    (e = "left" === c ? a + 1 : a),
                -1 === e
              );
            }, this)
          ),
        this.settings.loop ||
          (this.op(b, ">", h[this.minimum()])
            ? (e = b = this.minimum())
            : this.op(b, "<", h[this.maximum()]) && (e = b = this.maximum())),
        e
      );
    }),
    (e.prototype.animate = function(b) {
      var c = this.speed() > 0;
      this.is("animating") && this.onTransitionEnd(),
        c && (this.enter("animating"), this.trigger("translate")),
        a.support.transform3d && a.support.transition
          ? this.$stage.css({
              transform: "translate3d(" + b + "px,0px,0px)",
              transition:
                this.speed() / 1e3 +
                "s" +
                (this.settings.slideTransition
                  ? " " + this.settings.slideTransition
                  : "")
            })
          : c
          ? this.$stage.animate(
              { left: b + "px" },
              this.speed(),
              this.settings.fallbackEasing,
              a.proxy(this.onTransitionEnd, this)
            )
          : this.$stage.css({ left: b + "px" });
    }),
    (e.prototype.is = function(a) {
      return this._states.current[a] && this._states.current[a] > 0;
    }),
    (e.prototype.current = function(a) {
      if (a === d) return this._current;
      if (0 === this._items.length) return d;
      if (((a = this.normalize(a)), this._current !== a)) {
        var b = this.trigger("change", {
          property: { name: "position", value: a }
        });
        b.data !== d && (a = this.normalize(b.data)),
          (this._current = a),
          this.invalidate("position"),
          this.trigger("changed", {
            property: { name: "position", value: this._current }
          });
      }
      return this._current;
    }),
    (e.prototype.invalidate = function(b) {
      return (
        "string" === a.type(b) &&
          ((this._invalidated[b] = !0),
          this.is("valid") && this.leave("valid")),
        a.map(this._invalidated, function(a, b) {
          return b;
        })
      );
    }),
    (e.prototype.reset = function(a) {
      (a = this.normalize(a)) !== d &&
        ((this._speed = 0),
        (this._current = a),
        this.suppress(["translate", "translated"]),
        this.animate(this.coordinates(a)),
        this.release(["translate", "translated"]));
    }),
    (e.prototype.normalize = function(a, b) {
      var c = this._items.length,
        e = b ? 0 : this._clones.length;
      return (
        !this.isNumeric(a) || c < 1
          ? (a = d)
          : (a < 0 || a >= c + e) &&
            (a = ((((a - e / 2) % c) + c) % c) + e / 2),
        a
      );
    }),
    (e.prototype.relative = function(a) {
      return (a -= this._clones.length / 2), this.normalize(a, !0);
    }),
    (e.prototype.maximum = function(a) {
      var b,
        c,
        d,
        e = this.settings,
        f = this._coordinates.length;
      if (e.loop) f = this._clones.length / 2 + this._items.length - 1;
      else if (e.autoWidth || e.merge) {
        if ((b = this._items.length))
          for (
            c = this._items[--b].width(), d = this.$element.width();
            b-- && !((c += this._items[b].width() + this.settings.margin) > d);

          );
        f = b + 1;
      } else
        f = e.center ? this._items.length - 1 : this._items.length - e.items;
      return a && (f -= this._clones.length / 2), Math.max(f, 0);
    }),
    (e.prototype.minimum = function(a) {
      return a ? 0 : this._clones.length / 2;
    }),
    (e.prototype.items = function(a) {
      return a === d
        ? this._items.slice()
        : ((a = this.normalize(a, !0)), this._items[a]);
    }),
    (e.prototype.mergers = function(a) {
      return a === d
        ? this._mergers.slice()
        : ((a = this.normalize(a, !0)), this._mergers[a]);
    }),
    (e.prototype.clones = function(b) {
      var c = this._clones.length / 2,
        e = c + this._items.length,
        f = function(a) {
          return a % 2 == 0 ? e + a / 2 : c - (a + 1) / 2;
        };
      return b === d
        ? a.map(this._clones, function(a, b) {
            return f(b);
          })
        : a.map(this._clones, function(a, c) {
            return a === b ? f(c) : null;
          });
    }),
    (e.prototype.speed = function(a) {
      return a !== d && (this._speed = a), this._speed;
    }),
    (e.prototype.coordinates = function(b) {
      var c,
        e = 1,
        f = b - 1;
      return b === d
        ? a.map(
            this._coordinates,
            a.proxy(function(a, b) {
              return this.coordinates(b);
            }, this)
          )
        : (this.settings.center
            ? (this.settings.rtl && ((e = -1), (f = b + 1)),
              (c = this._coordinates[b]),
              (c += ((this.width() - c + (this._coordinates[f] || 0)) / 2) * e))
            : (c = this._coordinates[f] || 0),
          (c = Math.ceil(c)));
    }),
    (e.prototype.duration = function(a, b, c) {
      return 0 === c
        ? 0
        : Math.min(Math.max(Math.abs(b - a), 1), 6) *
            Math.abs(c || this.settings.smartSpeed);
    }),
    (e.prototype.to = function(a, b) {
      var c = this.current(),
        d = null,
        e = a - this.relative(c),
        f = (e > 0) - (e < 0),
        g = this._items.length,
        h = this.minimum(),
        i = this.maximum();
      this.settings.loop
        ? (!this.settings.rewind && Math.abs(e) > g / 2 && (e += -1 * f * g),
          (a = c + e),
          (d = ((((a - h) % g) + g) % g) + h) !== a &&
            d - e <= i &&
            d - e > 0 &&
            ((c = d - e), (a = d), this.reset(c)))
        : this.settings.rewind
        ? ((i += 1), (a = ((a % i) + i) % i))
        : (a = Math.max(h, Math.min(i, a))),
        this.speed(this.duration(c, a, b)),
        this.current(a),
        this.isVisible() && this.update();
    }),
    (e.prototype.next = function(a) {
      (a = a || !1), this.to(this.relative(this.current()) + 1, a);
    }),
    (e.prototype.prev = function(a) {
      (a = a || !1), this.to(this.relative(this.current()) - 1, a);
    }),
    (e.prototype.onTransitionEnd = function(a) {
      if (
        a !== d &&
        (a.stopPropagation(),
        (a.target || a.srcElement || a.originalTarget) !== this.$stage.get(0))
      )
        return !1;
      this.leave("animating"), this.trigger("translated");
    }),
    (e.prototype.viewport = function() {
      var d;
      return (
        this.options.responsiveBaseElement !== b
          ? (d = a(this.options.responsiveBaseElement).width())
          : b.innerWidth
          ? (d = b.innerWidth)
          : c.documentElement && c.documentElement.clientWidth
          ? (d = c.documentElement.clientWidth)
          : console.warn("Can not detect viewport width."),
        d
      );
    }),
    (e.prototype.replace = function(b) {
      this.$stage.empty(),
        (this._items = []),
        b && (b = b instanceof jQuery ? b : a(b)),
        this.settings.nestedItemSelector &&
          (b = b.find("." + this.settings.nestedItemSelector)),
        b
          .filter(function() {
            return 1 === this.nodeType;
          })
          .each(
            a.proxy(function(a, b) {
              (b = this.prepare(b)),
                this.$stage.append(b),
                this._items.push(b),
                this._mergers.push(
                  1 *
                    b
                      .find("[data-merge]")
                      .addBack("[data-merge]")
                      .attr("data-merge") || 1
                );
            }, this)
          ),
        this.reset(
          this.isNumeric(this.settings.startPosition)
            ? this.settings.startPosition
            : 0
        ),
        this.invalidate("items");
    }),
    (e.prototype.add = function(b, c) {
      var e = this.relative(this._current);
      (c = c === d ? this._items.length : this.normalize(c, !0)),
        (b = b instanceof jQuery ? b : a(b)),
        this.trigger("add", { content: b, position: c }),
        (b = this.prepare(b)),
        0 === this._items.length || c === this._items.length
          ? (0 === this._items.length && this.$stage.append(b),
            0 !== this._items.length && this._items[c - 1].after(b),
            this._items.push(b),
            this._mergers.push(
              1 *
                b
                  .find("[data-merge]")
                  .addBack("[data-merge]")
                  .attr("data-merge") || 1
            ))
          : (this._items[c].before(b),
            this._items.splice(c, 0, b),
            this._mergers.splice(
              c,
              0,
              1 *
                b
                  .find("[data-merge]")
                  .addBack("[data-merge]")
                  .attr("data-merge") || 1
            )),
        this._items[e] && this.reset(this._items[e].index()),
        this.invalidate("items"),
        this.trigger("added", { content: b, position: c });
    }),
    (e.prototype.remove = function(a) {
      (a = this.normalize(a, !0)) !== d &&
        (this.trigger("remove", { content: this._items[a], position: a }),
        this._items[a].remove(),
        this._items.splice(a, 1),
        this._mergers.splice(a, 1),
        this.invalidate("items"),
        this.trigger("removed", { content: null, position: a }));
    }),
    (e.prototype.preloadAutoWidthImages = function(b) {
      b.each(
        a.proxy(function(b, c) {
          this.enter("pre-loading"),
            (c = a(c)),
            a(new Image())
              .one(
                "load",
                a.proxy(function(a) {
                  c.attr("src", a.target.src),
                    c.css("opacity", 1),
                    this.leave("pre-loading"),
                    !this.is("pre-loading") &&
                      !this.is("initializing") &&
                      this.refresh();
                }, this)
              )
              .attr(
                "src",
                c.attr("src") || c.attr("data-src") || c.attr("data-src-retina")
              );
        }, this)
      );
    }),
    (e.prototype.destroy = function() {
      this.$element.off(".owl.core"),
        this.$stage.off(".owl.core"),
        a(c).off(".owl.core"),
        !1 !== this.settings.responsive &&
          (b.clearTimeout(this.resizeTimer),
          this.off(b, "resize", this._handlers.onThrottledResize));
      for (var d in this._plugins) this._plugins[d].destroy();
      this.$stage.children(".cloned").remove(),
        this.$stage.unwrap(),
        this.$stage
          .children()
          .contents()
          .unwrap(),
        this.$stage.children().unwrap(),
        this.$stage.remove(),
        this.$element
          .removeClass(this.options.refreshClass)
          .removeClass(this.options.loadingClass)
          .removeClass(this.options.loadedClass)
          .removeClass(this.options.rtlClass)
          .removeClass(this.options.dragClass)
          .removeClass(this.options.grabClass)
          .attr(
            "class",
            this.$element
              .attr("class")
              .replace(
                new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"),
                ""
              )
          )
          .removeData("owl.carousel");
    }),
    (e.prototype.op = function(a, b, c) {
      var d = this.settings.rtl;
      switch (b) {
        case "<":
          return d ? a > c : a < c;
        case ">":
          return d ? a < c : a > c;
        case ">=":
          return d ? a <= c : a >= c;
        case "<=":
          return d ? a >= c : a <= c;
      }
    }),
    (e.prototype.on = function(a, b, c, d) {
      a.addEventListener
        ? a.addEventListener(b, c, d)
        : a.attachEvent && a.attachEvent("on" + b, c);
    }),
    (e.prototype.off = function(a, b, c, d) {
      a.removeEventListener
        ? a.removeEventListener(b, c, d)
        : a.detachEvent && a.detachEvent("on" + b, c);
    }),
    (e.prototype.trigger = function(b, c, d, f, g) {
      var h = { item: { count: this._items.length, index: this.current() } },
        i = a.camelCase(
          a
            .grep(["on", b, d], function(a) {
              return a;
            })
            .join("-")
            .toLowerCase()
        ),
        j = a.Event(
          [b, "owl", d || "carousel"].join(".").toLowerCase(),
          a.extend({ relatedTarget: this }, h, c)
        );
      return (
        this._supress[b] ||
          (a.each(this._plugins, function(a, b) {
            b.onTrigger && b.onTrigger(j);
          }),
          this.register({ type: e.Type.Event, name: b }),
          this.$element.trigger(j),
          this.settings &&
            "function" == typeof this.settings[i] &&
            this.settings[i].call(this, j)),
        j
      );
    }),
    (e.prototype.enter = function(b) {
      a.each(
        [b].concat(this._states.tags[b] || []),
        a.proxy(function(a, b) {
          this._states.current[b] === d && (this._states.current[b] = 0),
            this._states.current[b]++;
        }, this)
      );
    }),
    (e.prototype.leave = function(b) {
      a.each(
        [b].concat(this._states.tags[b] || []),
        a.proxy(function(a, b) {
          this._states.current[b]--;
        }, this)
      );
    }),
    (e.prototype.register = function(b) {
      if (b.type === e.Type.Event) {
        if (
          (a.event.special[b.name] || (a.event.special[b.name] = {}),
          !a.event.special[b.name].owl)
        ) {
          var c = a.event.special[b.name]._default;
          (a.event.special[b.name]._default = function(a) {
            return !c ||
              !c.apply ||
              (a.namespace && -1 !== a.namespace.indexOf("owl"))
              ? a.namespace && a.namespace.indexOf("owl") > -1
              : c.apply(this, arguments);
          }),
            (a.event.special[b.name].owl = !0);
        }
      } else
        b.type === e.Type.State &&
          (this._states.tags[b.name]
            ? (this._states.tags[b.name] = this._states.tags[b.name].concat(
                b.tags
              ))
            : (this._states.tags[b.name] = b.tags),
          (this._states.tags[b.name] = a.grep(
            this._states.tags[b.name],
            a.proxy(function(c, d) {
              return a.inArray(c, this._states.tags[b.name]) === d;
            }, this)
          )));
    }),
    (e.prototype.suppress = function(b) {
      a.each(
        b,
        a.proxy(function(a, b) {
          this._supress[b] = !0;
        }, this)
      );
    }),
    (e.prototype.release = function(b) {
      a.each(
        b,
        a.proxy(function(a, b) {
          delete this._supress[b];
        }, this)
      );
    }),
    (e.prototype.pointer = function(a) {
      var c = { x: null, y: null };
      return (
        (a = a.originalEvent || a || b.event),
        (a =
          a.touches && a.touches.length
            ? a.touches[0]
            : a.changedTouches && a.changedTouches.length
            ? a.changedTouches[0]
            : a),
        a.pageX
          ? ((c.x = a.pageX), (c.y = a.pageY))
          : ((c.x = a.clientX), (c.y = a.clientY)),
        c
      );
    }),
    (e.prototype.isNumeric = function(a) {
      return !isNaN(parseFloat(a));
    }),
    (e.prototype.difference = function(a, b) {
      return { x: a.x - b.x, y: a.y - b.y };
    }),
    (a.fn.owlCarousel = function(b) {
      var c = Array.prototype.slice.call(arguments, 1);
      return this.each(function() {
        var d = a(this),
          f = d.data("owl.carousel");
        f ||
          ((f = new e(this, "object" == typeof b && b)),
          d.data("owl.carousel", f),
          a.each(
            [
              "next",
              "prev",
              "to",
              "destroy",
              "refresh",
              "replace",
              "add",
              "remove"
            ],
            function(b, c) {
              f.register({ type: e.Type.Event, name: c }),
                f.$element.on(
                  c + ".owl.carousel.core",
                  a.proxy(function(a) {
                    a.namespace &&
                      a.relatedTarget !== this &&
                      (this.suppress([c]),
                      f[c].apply(this, [].slice.call(arguments, 1)),
                      this.release([c]));
                  }, f)
                );
            }
          )),
          "string" == typeof b && "_" !== b.charAt(0) && f[b].apply(f, c);
      });
    }),
    (a.fn.owlCarousel.Constructor = e);
})(window.Zepto || window.jQuery, window, document),
  (function(a, b, c, d) {
    var e = function(b) {
      (this._core = b),
        (this._interval = null),
        (this._visible = null),
        (this._handlers = {
          "initialized.owl.carousel": a.proxy(function(a) {
            a.namespace && this._core.settings.autoRefresh && this.watch();
          }, this)
        }),
        (this._core.options = a.extend({}, e.Defaults, this._core.options)),
        this._core.$element.on(this._handlers);
    };
    (e.Defaults = { autoRefresh: !0, autoRefreshInterval: 500 }),
      (e.prototype.watch = function() {
        this._interval ||
          ((this._visible = this._core.isVisible()),
          (this._interval = b.setInterval(
            a.proxy(this.refresh, this),
            this._core.settings.autoRefreshInterval
          )));
      }),
      (e.prototype.refresh = function() {
        this._core.isVisible() !== this._visible &&
          ((this._visible = !this._visible),
          this._core.$element.toggleClass("owl-hidden", !this._visible),
          this._visible &&
            this._core.invalidate("width") &&
            this._core.refresh());
      }),
      (e.prototype.destroy = function() {
        var a, c;
        b.clearInterval(this._interval);
        for (a in this._handlers) this._core.$element.off(a, this._handlers[a]);
        for (c in Object.getOwnPropertyNames(this))
          "function" != typeof this[c] && (this[c] = null);
      }),
      (a.fn.owlCarousel.Constructor.Plugins.AutoRefresh = e);
  })(window.Zepto || window.jQuery, window, document),
  (function(a, b, c, d) {
    var e = function(b) {
      (this._core = b),
        (this._loaded = []),
        (this._handlers = {
          "initialized.owl.carousel change.owl.carousel resized.owl.carousel": a.proxy(
            function(b) {
              if (
                b.namespace &&
                this._core.settings &&
                this._core.settings.lazyLoad &&
                ((b.property && "position" == b.property.name) ||
                  "initialized" == b.type)
              ) {
                var c = this._core.settings,
                  e = (c.center && Math.ceil(c.items / 2)) || c.items,
                  f = (c.center && -1 * e) || 0,
                  g =
                    (b.property && b.property.value !== d
                      ? b.property.value
                      : this._core.current()) + f,
                  h = this._core.clones().length,
                  i = a.proxy(function(a, b) {
                    this.load(b);
                  }, this);
                for (
                  c.lazyLoadEager > 0 &&
                  ((e += c.lazyLoadEager),
                  c.loop && ((g -= c.lazyLoadEager), e++));
                  f++ < e;

                )
                  this.load(h / 2 + this._core.relative(g)),
                    h && a.each(this._core.clones(this._core.relative(g)), i),
                    g++;
              }
            },
            this
          )
        }),
        (this._core.options = a.extend({}, e.Defaults, this._core.options)),
        this._core.$element.on(this._handlers);
    };
    (e.Defaults = { lazyLoad: !1, lazyLoadEager: 0 }),
      (e.prototype.load = function(c) {
        var d = this._core.$stage.children().eq(c),
          e = d && d.find(".owl-lazy");
        !e ||
          a.inArray(d.get(0), this._loaded) > -1 ||
          (e.each(
            a.proxy(function(c, d) {
              var e,
                f = a(d),
                g =
                  (b.devicePixelRatio > 1 && f.attr("data-src-retina")) ||
                  f.attr("data-src") ||
                  f.attr("data-srcset");
              this._core.trigger("load", { element: f, url: g }, "lazy"),
                f.is("img")
                  ? f
                      .one(
                        "load.owl.lazy",
                        a.proxy(function() {
                          f.css("opacity", 1),
                            this._core.trigger(
                              "loaded",
                              { element: f, url: g },
                              "lazy"
                            );
                        }, this)
                      )
                      .attr("src", g)
                  : f.is("source")
                  ? f
                      .one(
                        "load.owl.lazy",
                        a.proxy(function() {
                          this._core.trigger(
                            "loaded",
                            { element: f, url: g },
                            "lazy"
                          );
                        }, this)
                      )
                      .attr("srcset", g)
                  : ((e = new Image()),
                    (e.onload = a.proxy(function() {
                      f.css({
                        "background-image": 'url("' + g + '")',
                        opacity: "1"
                      }),
                        this._core.trigger(
                          "loaded",
                          { element: f, url: g },
                          "lazy"
                        );
                    }, this)),
                    (e.src = g));
            }, this)
          ),
          this._loaded.push(d.get(0)));
      }),
      (e.prototype.destroy = function() {
        var a, b;
        for (a in this.handlers) this._core.$element.off(a, this.handlers[a]);
        for (b in Object.getOwnPropertyNames(this))
          "function" != typeof this[b] && (this[b] = null);
      }),
      (a.fn.owlCarousel.Constructor.Plugins.Lazy = e);
  })(window.Zepto || window.jQuery, window, document),
  (function(a, b, c, d) {
    var e = function(c) {
      (this._core = c),
        (this._previousHeight = null),
        (this._handlers = {
          "initialized.owl.carousel refreshed.owl.carousel": a.proxy(function(
            a
          ) {
            a.namespace && this._core.settings.autoHeight && this.update();
          },
          this),
          "changed.owl.carousel": a.proxy(function(a) {
            a.namespace &&
              this._core.settings.autoHeight &&
              "position" === a.property.name &&
              this.update();
          }, this),
          "loaded.owl.lazy": a.proxy(function(a) {
            a.namespace &&
              this._core.settings.autoHeight &&
              a.element.closest("." + this._core.settings.itemClass).index() ===
                this._core.current() &&
              this.update();
          }, this)
        }),
        (this._core.options = a.extend({}, e.Defaults, this._core.options)),
        this._core.$element.on(this._handlers),
        (this._intervalId = null);
      var d = this;
      a(b).on("load", function() {
        d._core.settings.autoHeight && d.update();
      }),
        a(b).resize(function() {
          d._core.settings.autoHeight &&
            (null != d._intervalId && clearTimeout(d._intervalId),
            (d._intervalId = setTimeout(function() {
              d.update();
            }, 250)));
        });
    };
    (e.Defaults = { autoHeight: !1, autoHeightClass: "owl-height" }),
      (e.prototype.update = function() {
        var b = this._core._current,
          c = b + this._core.settings.items,
          d = this._core.settings.lazyLoad,
          e = this._core.$stage
            .children()
            .toArray()
            .slice(b, c),
          f = [],
          g = 0;
        a.each(e, function(b, c) {
          f.push(a(c).height());
        }),
          (g = Math.max.apply(null, f)),
          g <= 1 && d && this._previousHeight && (g = this._previousHeight),
          (this._previousHeight = g),
          this._core.$stage
            .parent()
            .height(g)
            .addClass(this._core.settings.autoHeightClass);
      }),
      (e.prototype.destroy = function() {
        var a, b;
        for (a in this._handlers) this._core.$element.off(a, this._handlers[a]);
        for (b in Object.getOwnPropertyNames(this))
          "function" != typeof this[b] && (this[b] = null);
      }),
      (a.fn.owlCarousel.Constructor.Plugins.AutoHeight = e);
  })(window.Zepto || window.jQuery, window, document),
  (function(a, b, c, d) {
    var e = function(b) {
      (this._core = b),
        (this._videos = {}),
        (this._playing = null),
        (this._handlers = {
          "initialized.owl.carousel": a.proxy(function(a) {
            a.namespace &&
              this._core.register({
                type: "state",
                name: "playing",
                tags: ["interacting"]
              });
          }, this),
          "resize.owl.carousel": a.proxy(function(a) {
            a.namespace &&
              this._core.settings.video &&
              this.isInFullScreen() &&
              a.preventDefault();
          }, this),
          "refreshed.owl.carousel": a.proxy(function(a) {
            a.namespace &&
              this._core.is("resizing") &&
              this._core.$stage.find(".cloned .owl-video-frame").remove();
          }, this),
          "changed.owl.carousel": a.proxy(function(a) {
            a.namespace &&
              "position" === a.property.name &&
              this._playing &&
              this.stop();
          }, this),
          "prepared.owl.carousel": a.proxy(function(b) {
            if (b.namespace) {
              var c = a(b.content).find(".owl-video");
              c.length &&
                (c.css("display", "none"), this.fetch(c, a(b.content)));
            }
          }, this)
        }),
        (this._core.options = a.extend({}, e.Defaults, this._core.options)),
        this._core.$element.on(this._handlers),
        this._core.$element.on(
          "click.owl.video",
          ".owl-video-play-icon",
          a.proxy(function(a) {
            this.play(a);
          }, this)
        );
    };
    (e.Defaults = { video: !1, videoHeight: !1, videoWidth: !1 }),
      (e.prototype.fetch = function(a, b) {
        var c = (function() {
            return a.attr("data-vimeo-id")
              ? "vimeo"
              : a.attr("data-vzaar-id")
              ? "vzaar"
              : "youtube";
          })(),
          d =
            a.attr("data-vimeo-id") ||
            a.attr("data-youtube-id") ||
            a.attr("data-vzaar-id"),
          e = a.attr("data-width") || this._core.settings.videoWidth,
          f = a.attr("data-height") || this._core.settings.videoHeight,
          g = a.attr("href");
        if (!g) throw new Error("Missing video URL.");
        if (
          ((d = g.match(
            /(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com|be\-nocookie\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/
          )),
          d[3].indexOf("youtu") > -1)
        )
          c = "youtube";
        else if (d[3].indexOf("vimeo") > -1) c = "vimeo";
        else {
          if (!(d[3].indexOf("vzaar") > -1))
            throw new Error("Video URL not supported.");
          c = "vzaar";
        }
        (d = d[6]),
          (this._videos[g] = { type: c, id: d, width: e, height: f }),
          b.attr("data-video", g),
          this.thumbnail(a, this._videos[g]);
      }),
      (e.prototype.thumbnail = function(b, c) {
        var d,
          e,
          f,
          g =
            c.width && c.height
              ? "width:" + c.width + "px;height:" + c.height + "px;"
              : "",
          h = b.find("img"),
          i = "src",
          j = "",
          k = this._core.settings,
          l = function(c) {
            (e = '<div class="owl-video-play-icon"></div>'),
              (d = k.lazyLoad
                ? a("<div/>", { class: "owl-video-tn " + j, srcType: c })
                : a("<div/>", {
                    class: "owl-video-tn",
                    style: "opacity:1;background-image:url(" + c + ")"
                  })),
              b.after(d),
              b.after(e);
          };
        if (
          (b.wrap(a("<div/>", { class: "owl-video-wrapper", style: g })),
          this._core.settings.lazyLoad && ((i = "data-src"), (j = "owl-lazy")),
          h.length)
        )
          return l(h.attr(i)), h.remove(), !1;
        "youtube" === c.type
          ? ((f = "//img.youtube.com/vi/" + c.id + "/hqdefault.jpg"), l(f))
          : "vimeo" === c.type
          ? a.ajax({
              type: "GET",
              url: "//vimeo.com/api/v2/video/" + c.id + ".json",
              jsonp: "callback",
              dataType: "jsonp",
              success: function(a) {
                (f = a[0].thumbnail_large), l(f);
              }
            })
          : "vzaar" === c.type &&
            a.ajax({
              type: "GET",
              url: "//vzaar.com/api/videos/" + c.id + ".json",
              jsonp: "callback",
              dataType: "jsonp",
              success: function(a) {
                (f = a.framegrab_url), l(f);
              }
            });
      }),
      (e.prototype.stop = function() {
        this._core.trigger("stop", null, "video"),
          this._playing.find(".owl-video-frame").remove(),
          this._playing.removeClass("owl-video-playing"),
          (this._playing = null),
          this._core.leave("playing"),
          this._core.trigger("stopped", null, "video");
      }),
      (e.prototype.play = function(b) {
        var c,
          d = a(b.target),
          e = d.closest("." + this._core.settings.itemClass),
          f = this._videos[e.attr("data-video")],
          g = f.width || "100%",
          h = f.height || this._core.$stage.height();
        this._playing ||
          (this._core.enter("playing"),
          this._core.trigger("play", null, "video"),
          (e = this._core.items(this._core.relative(e.index()))),
          this._core.reset(e.index()),
          (c = a(
            '<iframe frameborder="0" allowfullscreen mozallowfullscreen webkitAllowFullScreen ></iframe>'
          )),
          c.attr("height", h),
          c.attr("width", g),
          "youtube" === f.type
            ? c.attr(
                "src",
                "//www.youtube.com/embed/" +
                  f.id +
                  "?autoplay=1&rel=0&v=" +
                  f.id
              )
            : "vimeo" === f.type
            ? c.attr("src", "//player.vimeo.com/video/" + f.id + "?autoplay=1")
            : "vzaar" === f.type &&
              c.attr(
                "src",
                "//view.vzaar.com/" + f.id + "/player?autoplay=true"
              ),
          a(c)
            .wrap('<div class="owl-video-frame" />')
            .insertAfter(e.find(".owl-video")),
          (this._playing = e.addClass("owl-video-playing")));
      }),
      (e.prototype.isInFullScreen = function() {
        var b =
          c.fullscreenElement ||
          c.mozFullScreenElement ||
          c.webkitFullscreenElement;
        return (
          b &&
          a(b)
            .parent()
            .hasClass("owl-video-frame")
        );
      }),
      (e.prototype.destroy = function() {
        var a, b;
        this._core.$element.off("click.owl.video");
        for (a in this._handlers) this._core.$element.off(a, this._handlers[a]);
        for (b in Object.getOwnPropertyNames(this))
          "function" != typeof this[b] && (this[b] = null);
      }),
      (a.fn.owlCarousel.Constructor.Plugins.Video = e);
  })(window.Zepto || window.jQuery, window, document),
  (function(a, b, c, d) {
    var e = function(b) {
      (this.core = b),
        (this.core.options = a.extend({}, e.Defaults, this.core.options)),
        (this.swapping = !0),
        (this.previous = d),
        (this.next = d),
        (this.handlers = {
          "change.owl.carousel": a.proxy(function(a) {
            a.namespace &&
              "position" == a.property.name &&
              ((this.previous = this.core.current()),
              (this.next = a.property.value));
          }, this),
          "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": a.proxy(
            function(a) {
              a.namespace && (this.swapping = "translated" == a.type);
            },
            this
          ),
          "translate.owl.carousel": a.proxy(function(a) {
            a.namespace &&
              this.swapping &&
              (this.core.options.animateOut || this.core.options.animateIn) &&
              this.swap();
          }, this)
        }),
        this.core.$element.on(this.handlers);
    };
    (e.Defaults = { animateOut: !1, animateIn: !1 }),
      (e.prototype.swap = function() {
        if (
          1 === this.core.settings.items &&
          a.support.animation &&
          a.support.transition
        ) {
          this.core.speed(0);
          var b,
            c = a.proxy(this.clear, this),
            d = this.core.$stage.children().eq(this.previous),
            e = this.core.$stage.children().eq(this.next),
            f = this.core.settings.animateIn,
            g = this.core.settings.animateOut;
          this.core.current() !== this.previous &&
            (g &&
              ((b =
                this.core.coordinates(this.previous) -
                this.core.coordinates(this.next)),
              d
                .one(a.support.animation.end, c)
                .css({ left: b + "px" })
                .addClass("animated owl-animated-out")
                .addClass(g)),
            f &&
              e
                .one(a.support.animation.end, c)
                .addClass("animated owl-animated-in")
                .addClass(f));
        }
      }),
      (e.prototype.clear = function(b) {
        a(b.target)
          .css({ left: "" })
          .removeClass("animated owl-animated-out owl-animated-in")
          .removeClass(this.core.settings.animateIn)
          .removeClass(this.core.settings.animateOut),
          this.core.onTransitionEnd();
      }),
      (e.prototype.destroy = function() {
        var a, b;
        for (a in this.handlers) this.core.$element.off(a, this.handlers[a]);
        for (b in Object.getOwnPropertyNames(this))
          "function" != typeof this[b] && (this[b] = null);
      }),
      (a.fn.owlCarousel.Constructor.Plugins.Animate = e);
  })(window.Zepto || window.jQuery, window, document),
  (function(a, b, c, d) {
    var e = function(b) {
      (this._core = b),
        (this._call = null),
        (this._time = 0),
        (this._timeout = 0),
        (this._paused = !0),
        (this._handlers = {
          "changed.owl.carousel": a.proxy(function(a) {
            a.namespace && "settings" === a.property.name
              ? this._core.settings.autoplay
                ? this.play()
                : this.stop()
              : a.namespace &&
                "position" === a.property.name &&
                this._paused &&
                (this._time = 0);
          }, this),
          "initialized.owl.carousel": a.proxy(function(a) {
            a.namespace && this._core.settings.autoplay && this.play();
          }, this),
          "play.owl.autoplay": a.proxy(function(a, b, c) {
            a.namespace && this.play(b, c);
          }, this),
          "stop.owl.autoplay": a.proxy(function(a) {
            a.namespace && this.stop();
          }, this),
          "mouseover.owl.autoplay": a.proxy(function() {
            this._core.settings.autoplayHoverPause &&
              this._core.is("rotating") &&
              this.pause();
          }, this),
          "mouseleave.owl.autoplay": a.proxy(function() {
            this._core.settings.autoplayHoverPause &&
              this._core.is("rotating") &&
              this.play();
          }, this),
          "touchstart.owl.core": a.proxy(function() {
            this._core.settings.autoplayHoverPause &&
              this._core.is("rotating") &&
              this.pause();
          }, this),
          "touchend.owl.core": a.proxy(function() {
            this._core.settings.autoplayHoverPause && this.play();
          }, this)
        }),
        this._core.$element.on(this._handlers),
        (this._core.options = a.extend({}, e.Defaults, this._core.options));
    };
    (e.Defaults = {
      autoplay: !1,
      autoplayTimeout: 5e3,
      autoplayHoverPause: !1,
      autoplaySpeed: !1
    }),
      (e.prototype._next = function(d) {
        (this._call = b.setTimeout(
          a.proxy(this._next, this, d),
          this._timeout * (Math.round(this.read() / this._timeout) + 1) -
            this.read()
        )),
          this._core.is("interacting") ||
            c.hidden ||
            this._core.next(d || this._core.settings.autoplaySpeed);
      }),
      (e.prototype.read = function() {
        return new Date().getTime() - this._time;
      }),
      (e.prototype.play = function(c, d) {
        var e;
        this._core.is("rotating") || this._core.enter("rotating"),
          (c = c || this._core.settings.autoplayTimeout),
          (e = Math.min(this._time % (this._timeout || c), c)),
          this._paused
            ? ((this._time = this.read()), (this._paused = !1))
            : b.clearTimeout(this._call),
          (this._time += (this.read() % c) - e),
          (this._timeout = c),
          (this._call = b.setTimeout(a.proxy(this._next, this, d), c - e));
      }),
      (e.prototype.stop = function() {
        this._core.is("rotating") &&
          ((this._time = 0),
          (this._paused = !0),
          b.clearTimeout(this._call),
          this._core.leave("rotating"));
      }),
      (e.prototype.pause = function() {
        this._core.is("rotating") &&
          !this._paused &&
          ((this._time = this.read()),
          (this._paused = !0),
          b.clearTimeout(this._call));
      }),
      (e.prototype.destroy = function() {
        var a, b;
        this.stop();
        for (a in this._handlers) this._core.$element.off(a, this._handlers[a]);
        for (b in Object.getOwnPropertyNames(this))
          "function" != typeof this[b] && (this[b] = null);
      }),
      (a.fn.owlCarousel.Constructor.Plugins.autoplay = e);
  })(window.Zepto || window.jQuery, window, document),
  (function(a, b, c, d) {
    "use strict";
    var e = function(b) {
      (this._core = b),
        (this._initialized = !1),
        (this._pages = []),
        (this._controls = {}),
        (this._templates = []),
        (this.$element = this._core.$element),
        (this._overrides = {
          next: this._core.next,
          prev: this._core.prev,
          to: this._core.to
        }),
        (this._handlers = {
          "prepared.owl.carousel": a.proxy(function(b) {
            b.namespace &&
              this._core.settings.dotsData &&
              this._templates.push(
                '<div class="' +
                  this._core.settings.dotClass +
                  '">' +
                  a(b.content)
                    .find("[data-dot]")
                    .addBack("[data-dot]")
                    .attr("data-dot") +
                  "</div>"
              );
          }, this),
          "added.owl.carousel": a.proxy(function(a) {
            a.namespace &&
              this._core.settings.dotsData &&
              this._templates.splice(a.position, 0, this._templates.pop());
          }, this),
          "remove.owl.carousel": a.proxy(function(a) {
            a.namespace &&
              this._core.settings.dotsData &&
              this._templates.splice(a.position, 1);
          }, this),
          "changed.owl.carousel": a.proxy(function(a) {
            a.namespace && "position" == a.property.name && this.draw();
          }, this),
          "initialized.owl.carousel": a.proxy(function(a) {
            a.namespace &&
              !this._initialized &&
              (this._core.trigger("initialize", null, "navigation"),
              this.initialize(),
              this.update(),
              this.draw(),
              (this._initialized = !0),
              this._core.trigger("initialized", null, "navigation"));
          }, this),
          "refreshed.owl.carousel": a.proxy(function(a) {
            a.namespace &&
              this._initialized &&
              (this._core.trigger("refresh", null, "navigation"),
              this.update(),
              this.draw(),
              this._core.trigger("refreshed", null, "navigation"));
          }, this)
        }),
        (this._core.options = a.extend({}, e.Defaults, this._core.options)),
        this.$element.on(this._handlers);
    };
    (e.Defaults = {
      nav: !1,
      navText: [
        '<span aria-label="Previous">&#x2039;</span>',
        '<span aria-label="Next">&#x203a;</span>'
      ],
      navSpeed: !1,
      navElement: 'button type="button" role="presentation"',
      navContainer: !1,
      navContainerClass: "owl-nav",
      navClass: ["owl-prev", "owl-next"],
      slideBy: 1,
      dotClass: "owl-dot",
      dotsClass: "owl-dots",
      dots: !0,
      dotsEach: !1,
      dotsData: !1,
      dotsSpeed: !1,
      dotsContainer: !1
    }),
      (e.prototype.initialize = function() {
        var b,
          c = this._core.settings;
        (this._controls.$relative = (c.navContainer
          ? a(c.navContainer)
          : a("<div>")
              .addClass(c.navContainerClass)
              .appendTo(this.$element)
        ).addClass("disabled")),
          (this._controls.$previous = a("<" + c.navElement + ">")
            .addClass(c.navClass[0])
            .html(c.navText[0])
            .prependTo(this._controls.$relative)
            .on(
              "click",
              a.proxy(function(a) {
                this.prev(c.navSpeed);
              }, this)
            )),
          (this._controls.$next = a("<" + c.navElement + ">")
            .addClass(c.navClass[1])
            .html(c.navText[1])
            .appendTo(this._controls.$relative)
            .on(
              "click",
              a.proxy(function(a) {
                this.next(c.navSpeed);
              }, this)
            )),
          c.dotsData ||
            (this._templates = [
              a('<button role="button">')
                .addClass(c.dotClass)
                .append(a("<span>"))
                .prop("outerHTML")
            ]),
          (this._controls.$absolute = (c.dotsContainer
            ? a(c.dotsContainer)
            : a("<div>")
                .addClass(c.dotsClass)
                .appendTo(this.$element)
          ).addClass("disabled")),
          this._controls.$absolute.on(
            "click",
            "button",
            a.proxy(function(b) {
              var d = a(b.target)
                .parent()
                .is(this._controls.$absolute)
                ? a(b.target).index()
                : a(b.target)
                    .parent()
                    .index();
              b.preventDefault(), this.to(d, c.dotsSpeed);
            }, this)
          );
        for (b in this._overrides) this._core[b] = a.proxy(this[b], this);
      }),
      (e.prototype.destroy = function() {
        var a, b, c, d, e;
        e = this._core.settings;
        for (a in this._handlers) this.$element.off(a, this._handlers[a]);
        for (b in this._controls)
          "$relative" === b && e.navContainer
            ? this._controls[b].html("")
            : this._controls[b].remove();
        for (d in this.overides) this._core[d] = this._overrides[d];
        for (c in Object.getOwnPropertyNames(this))
          "function" != typeof this[c] && (this[c] = null);
      }),
      (e.prototype.update = function() {
        var a,
          b,
          c,
          d = this._core.clones().length / 2,
          e = d + this._core.items().length,
          f = this._core.maximum(!0),
          g = this._core.settings,
          h = g.center || g.autoWidth || g.dotsData ? 1 : g.dotsEach || g.items;
        if (
          ("page" !== g.slideBy && (g.slideBy = Math.min(g.slideBy, g.items)),
          g.dots || "page" == g.slideBy)
        )
          for (this._pages = [], a = d, b = 0, c = 0; a < e; a++) {
            if (b >= h || 0 === b) {
              if (
                (this._pages.push({
                  start: Math.min(f, a - d),
                  end: a - d + h - 1
                }),
                Math.min(f, a - d) === f)
              )
                break;
              (b = 0), ++c;
            }
            b += this._core.mergers(this._core.relative(a));
          }
      }),
      (e.prototype.draw = function() {
        var b,
          c = this._core.settings,
          d = this._core.items().length <= c.items,
          e = this._core.relative(this._core.current()),
          f = c.loop || c.rewind;
        this._controls.$relative.toggleClass("disabled", !c.nav || d),
          c.nav &&
            (this._controls.$previous.toggleClass(
              "disabled",
              !f && e <= this._core.minimum(!0)
            ),
            this._controls.$next.toggleClass(
              "disabled",
              !f && e >= this._core.maximum(!0)
            )),
          this._controls.$absolute.toggleClass("disabled", !c.dots || d),
          c.dots &&
            ((b =
              this._pages.length - this._controls.$absolute.children().length),
            c.dotsData && 0 !== b
              ? this._controls.$absolute.html(this._templates.join(""))
              : b > 0
              ? this._controls.$absolute.append(
                  new Array(b + 1).join(this._templates[0])
                )
              : b < 0 &&
                this._controls.$absolute
                  .children()
                  .slice(b)
                  .remove(),
            this._controls.$absolute.find(".active").removeClass("active"),
            this._controls.$absolute
              .children()
              .eq(a.inArray(this.current(), this._pages))
              .addClass("active"));
      }),
      (e.prototype.onTrigger = function(b) {
        var c = this._core.settings;
        b.page = {
          index: a.inArray(this.current(), this._pages),
          count: this._pages.length,
          size:
            c &&
            (c.center || c.autoWidth || c.dotsData ? 1 : c.dotsEach || c.items)
        };
      }),
      (e.prototype.current = function() {
        var b = this._core.relative(this._core.current());
        return a
          .grep(
            this._pages,
            a.proxy(function(a, c) {
              return a.start <= b && a.end >= b;
            }, this)
          )
          .pop();
      }),
      (e.prototype.getPosition = function(b) {
        var c,
          d,
          e = this._core.settings;
        return (
          "page" == e.slideBy
            ? ((c = a.inArray(this.current(), this._pages)),
              (d = this._pages.length),
              b ? ++c : --c,
              (c = this._pages[((c % d) + d) % d].start))
            : ((c = this._core.relative(this._core.current())),
              (d = this._core.items().length),
              b ? (c += e.slideBy) : (c -= e.slideBy)),
          c
        );
      }),
      (e.prototype.next = function(b) {
        a.proxy(this._overrides.to, this._core)(this.getPosition(!0), b);
      }),
      (e.prototype.prev = function(b) {
        a.proxy(this._overrides.to, this._core)(this.getPosition(!1), b);
      }),
      (e.prototype.to = function(b, c, d) {
        var e;
        !d && this._pages.length
          ? ((e = this._pages.length),
            a.proxy(this._overrides.to, this._core)(
              this._pages[((b % e) + e) % e].start,
              c
            ))
          : a.proxy(this._overrides.to, this._core)(b, c);
      }),
      (a.fn.owlCarousel.Constructor.Plugins.Navigation = e);
  })(window.Zepto || window.jQuery, window, document),
  (function(a, b, c, d) {
    "use strict";
    var e = function(c) {
      (this._core = c),
        (this._hashes = {}),
        (this.$element = this._core.$element),
        (this._handlers = {
          "initialized.owl.carousel": a.proxy(function(c) {
            c.namespace &&
              "URLHash" === this._core.settings.startPosition &&
              a(b).trigger("hashchange.owl.navigation");
          }, this),
          "prepared.owl.carousel": a.proxy(function(b) {
            if (b.namespace) {
              var c = a(b.content)
                .find("[data-hash]")
                .addBack("[data-hash]")
                .attr("data-hash");
              if (!c) return;
              this._hashes[c] = b.content;
            }
          }, this),
          "changed.owl.carousel": a.proxy(function(c) {
            if (c.namespace && "position" === c.property.name) {
              var d = this._core.items(
                  this._core.relative(this._core.current())
                ),
                e = a
                  .map(this._hashes, function(a, b) {
                    return a === d ? b : null;
                  })
                  .join();
              if (!e || b.location.hash.slice(1) === e) return;
              b.location.hash = e;
            }
          }, this)
        }),
        (this._core.options = a.extend({}, e.Defaults, this._core.options)),
        this.$element.on(this._handlers),
        a(b).on(
          "hashchange.owl.navigation",
          a.proxy(function(a) {
            var c = b.location.hash.substring(1),
              e = this._core.$stage.children(),
              f = this._hashes[c] && e.index(this._hashes[c]);
            f !== d &&
              f !== this._core.current() &&
              this._core.to(this._core.relative(f), !1, !0);
          }, this)
        );
    };
    (e.Defaults = { URLhashListener: !1 }),
      (e.prototype.destroy = function() {
        var c, d;
        a(b).off("hashchange.owl.navigation");
        for (c in this._handlers) this._core.$element.off(c, this._handlers[c]);
        for (d in Object.getOwnPropertyNames(this))
          "function" != typeof this[d] && (this[d] = null);
      }),
      (a.fn.owlCarousel.Constructor.Plugins.Hash = e);
  })(window.Zepto || window.jQuery, window, document),
  (function(a, b, c, d) {
    function e(b, c) {
      var e = !1,
        f = b.charAt(0).toUpperCase() + b.slice(1);
      return (
        a.each((b + " " + h.join(f + " ") + f).split(" "), function(a, b) {
          if (g[b] !== d) return (e = !c || b), !1;
        }),
        e
      );
    }
    function f(a) {
      return e(a, !0);
    }
    var g = a("<support>").get(0).style,
      h = "Webkit Moz O ms".split(" "),
      i = {
        transition: {
          end: {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd",
            transition: "transitionend"
          }
        },
        animation: {
          end: {
            WebkitAnimation: "webkitAnimationEnd",
            MozAnimation: "animationend",
            OAnimation: "oAnimationEnd",
            animation: "animationend"
          }
        }
      },
      j = {
        csstransforms: function() {
          return !!e("transform");
        },
        csstransforms3d: function() {
          return !!e("perspective");
        },
        csstransitions: function() {
          return !!e("transition");
        },
        cssanimations: function() {
          return !!e("animation");
        }
      };
    j.csstransitions() &&
      ((a.support.transition = new String(f("transition"))),
      (a.support.transition.end = i.transition.end[a.support.transition])),
      j.cssanimations() &&
        ((a.support.animation = new String(f("animation"))),
        (a.support.animation.end = i.animation.end[a.support.animation])),
      j.csstransforms() &&
        ((a.support.transform = new String(f("transform"))),
        (a.support.transform3d = j.csstransforms3d()));
  })(window.Zepto || window.jQuery, window, document);
jQuery(document).ready(function($) {
  designerpage.init();
});
var designerpage = (function() {
  var isAnimating = false;
  var print2console = function(msg) {
    var showConsoleMsg = window.location.search.indexOf("console") != -1;
    if (showConsoleMsg) console.log(msg);
  };
  var loadingIcon = (function() {
    var private = {
      config: { color: "255, 255, 255", size: "10px", width: "3px" },
      exists: function() {
        var eleExisting = $("body").find(".loading_11051983");
        if (eleExisting) {
          this.destroy();
          this.config = { color: "255, 255, 255", size: "10px", width: "3px" };
        }
      },
      destroy: function() {
        $(".loader-icon").remove();
        $("#loader_11051983").remove();
        $(".loading_11051983").removeClass("loading_11051983");
      },
      loaderObj: null,
      loaderHTML:
        '<div class="loader-icon" style="width: 100%;height: 100%;position: absolute;display:block!important;top:50%;left:50%;-webkit-transform: translate(-50%,-50%);transform: translate(-50%,-50%);z-index: 30000000;"><div class="loader-icon-inner" style="height:100%;display: flex;-webkit-box-align: center;-ms-flex-align: center;align-items: center;-webkit-box-pack: center;-ms-flex-pack: center;justify-content: center;"></div></div>',
      buildCSS: function() {
        return (
          '<style type="text/css" id="loader_11051983">.loading_11051983{position:relative!important;color:transparent!important;text-shadow:none!important;}.loading_11051983 > * {display:none!important;}.loader-icon .loader-icon-inner:after{display:block!important;content:"";width:' +
          this.config.size +
          ";height:" +
          this.config.size +
          ";border-width:" +
          this.config.width +
          ";border-top-color:rgb(" +
          this.config.color +
          ");border-bottom-color:rgba(" +
          this.config.color +
          ", 0.28);border-left-color:rgba(" +
          this.config.color +
          ", 0.28);border-right-color:rgba(" +
          this.config.color +
          ", 0.28);border-radius:100%;background-clip:padding-box;border-style:solid;opacity:1;-webkit-animation:rotate .6s linear infinite;animation:rotate .6s linear infinite}@keyframes rotate{0%{-webkit-transform:rotateZ(-360deg);transform:rotateZ(-360deg)}100%{-webkit-transform:rotateZ(0);transform:rotateZ(0)}}@-webkit-keyframes rotate{0%{-webkit-transform:rotateZ(-360deg);transform:rotateZ(-360deg)}100%{-webkit-transform:rotateZ(0);transform:rotateZ(0)}}</style>"
        );
      },
      print: function(ele) {
        console.log(ele + ": " + this[ele]);
      }
    };
    return {
      start: function(ele, newConfig) {
        newConfig = newConfig || false;
        if (private.exists());
        if (typeof newConfig == "object") {
          private.config = newConfig;
        }
        $("head").append(private.buildCSS(ele));
        ele.addClass("loading_11051983");
        ele.prepend(private.loaderHTML);
      },
      stop: function(ele) {
        ele.removeClass("loading_11051983");
        ele.find(".loader-icon").remove();
        if ($("#loader_11051983").length > 0) $("#loader_11051983").remove();
      }
    };
  })();
  var loginBoxShow = function(target, targetBox) {
    isAnimating = true;
    targetBox.css({
      opacity: 0,
      scale: 0,
      top: "50%",
      left: "50%",
      translate: "-50%, -50%",
      margin: 0
    });
    target.css("display", "block").transition({ opacity: 1 }, 300, "ease");
    targetBox.transition(
      { opacity: 1, scale: 1, delay: 100 },
      400,
      "snap",
      function() {
        $("body").addClass("noscroll");
        isAnimating = false;
      }
    );
  };
  var loginBoxhide = function(target, targetBox) {
    isAnimating = true;
    targetBox.transition({ opacity: 0, scale: 0 }, 400, "snap");
    target.transition({ opacity: 0, delay: 100 }, 300, "ease", function() {
      target.css("display", "none");
      $("body").removeClass("noscroll");
      isAnimating = false;
    });
  };
  var scrollTo = function(yPos, callbackEle, callbackClass) {
    callbackEle = callbackEle || false;
    callbackClass = callbackClass || false;
    $("html, body, #main").animate({ scrollTop: yPos }, "slow", function() {
      if (!callbackEle || !callbackClass) return;
      callbackEle.addClass(callbackClass);
    });
  };
  var isFactsUsingCurrentYear = function() {
    var d = new Date();
    var m = d.getMonth();
    var y = d.getFullYear();
    var $txtContainer = $("#facts").find(".footer-note");
    $txtContainer.each(function(index, element) {
      var str = $(this).text();
      var matchOriginalStringYear = str.search("2018") != -1;
      if (matchOriginalStringYear) {
        if (m >= 2 && y != 2018) str = str.replace("2018", y);
        else return false;
      } else {
        print2console(
          "Cannot find string '2018' in one footernote of facts bar "
        );
        return;
      }
      $(this).text(str);
    });
  };
  var setBelowFoldContentOrder = function() {
    var $body = $("body");
    var $hotfacts = $("#hotfacts");
    var $facts = $("#facts");
    var $testimonials = $("#testimonials");
    var list = {
      hotfacts: hotfactsOrder,
      facts: factsOrder,
      testimonials: testimonialsOrder
    };
    var listOrdered = [];
    if ($body.hasClass("no-seo")) return;
    if (
      ($body.is(".no-hotfacts-mob") || $body.is(".no-hotfacts-dsktp")) &&
      ($body.is(".no-facts-mob") || $body.is(".no-facts-dsktp"))
    ) {
      list.testimonials = 0;
      list.hotfacts = 1;
      list.facts = 2;
    } else if (
      ($body.is(".no-hotfacts-mob") || $body.is(".no-hotfacts-dsktp")) &&
      ($body.is(".no-testimonials-mob") || $body.is(".no-testimonials-dsktp"))
    ) {
      list.facts = 0;
      list.testimonials = 1;
      list.hotfacts = 2;
    } else if ($body.is(".no-hotfacts-mob") || $body.is(".no-hotfacts-dsktp")) {
      list.facts = 0;
      list.testimonials = 1;
      list.hotfacts = 2;
    }
    if (hotfactsOrder == "" || factsOrder == "" || testimonialsOrder == "") {
      list.hotfacts = 0;
      list.facts = 1;
      list.testimonials = 2;
    }
    listOrdered = Object.keys(list).sort(function(a, b) {
      return list[a] - list[b];
    });
    $(listOrdered).each(function(index) {
      $("#" + listOrdered[index]).addClass("order_" + index);
    });
  };
  var checkAndSetSlider = function() {
    var winWidth = $(window).width();
    var $body = $("body");
    var slidesContainer = winWidth >= 1000 ? topstageSlides : mobTopstageSlides;
    var platformFirst = winWidth >= 1000 ? "dsktp" : "mob";
    var $sliderEle =
      winWidth >= 1000
        ? $("#desktop-topstage-slider")
        : $("#mobile-topstage-slider");
    var slides = slidesContainer.filter(validateSlides);
    function validateSlides(slide) {
      return slide != "";
    }
    if ($body.hasClass("img-fullscreen-mob") && $body.hasClass("slide-show"))
      $body.removeClass("img-fullscreen-mob");
    if ($body.hasClass("slide-show") && slides.length != 0) {
      var output = "";
      for (var i = 0; i < slides.length; i++) {
        output +=
          '<div class="owl-lazy" data-src="' +
          slides[i] +
          '" alt="slide' +
          i +
          '"></div>';
      }
      $sliderEle.prepend(output);
      $body.addClass(platformFirst);
      return true;
    } else if ($body.hasClass("slide-show") && slides.length == 0) {
      $body.removeClass("slide-show");
      return false;
    } else return false;
  };
  var mobileFullscreenMode = function() {
    var $body = $("body");
    var $win = $(window);
    var winWidth = $win.width();
    var winHeight = $win.height();
    var $regbox = $("#regbox");
    var $mobileStage = $("#mobile-stage");
    var $header = $("#header");
    var conditionsIdeal =
      $body.is(".img-fullscreen-mob.min-reg-mob") && winWidth < 1000;
    var viewportExcess = winHeight - ($regbox.outerHeight() + $header.height());
    if (conditionsIdeal) {
      if (viewportExcess <= $mobileStage.height()) {
        return;
      } else {
        $mobileStage.css("padding-bottom", "0").height(viewportExcess);
      }
    }
  };
  var setCustomTestimonialImgs = function() {
    var $testimonials = $("#testimonials");
    var imgOutput = [
      $testimonials.find(".user-01"),
      $testimonials.find(".user-02"),
      $testimonials.find(".user-03")
    ];
    for (i = 0; i < userImgs.length; i++) {
      if (userImgs[i] == "") continue;
      else {
        imgOutput[i].css("background-image", "url(" + userImgs[i] + ")");
      }
    }
  };
  var isDesktop = function() {
    var desktop = 1000;
    var winWidth = $(window).width();
    var $body = $("body");
    if (winWidth < desktop) {
      if ($body.hasClass("desktop-layout"))
        $body.removeClass("desktop-layout").addClass("tablet-layout");
      else if (!$body.hasClass("tablet-layout"))
        $body.addClass("tablet-layout");
      return false;
    } else if (winWidth >= desktop) {
      if ($body.hasClass("tablet-layout"))
        $body.removeClass("tablet-layout").addClass("desktop-layout");
      else if (!$body.hasClass("desktop-layout"))
        $body.addClass("desktop-layout");
      return true;
    }
  };
  var requestLogin = function() {
    var $loginBtn = $("#login-submit");
    var $email = $("#login-overlay input[name=email]");
    var $password = $("#login-overlay input[name=password]");
    var $output = $(".login-err");
    var result = true;
    loadingIcon.start($loginBtn);
    $.post("/ws/login", { email: $email.val(), pwd: $password.val() }, function(
      data
    ) {
      var data = JSON.parse(data);
      if (data.success) {
        top.location = data.redirect;
        return;
      } else {
        loadingIcon.stop($loginBtn);
        print2console("requestLogin: Failed \n Status: " + data.status);
        showTimedErr($email.add($password), $output, 1500, data.msg);
      }
    });
  };
  var forgotPassword = function() {
    var $forgotPasswordBtn = $("#forgot-pw-btn");
    var $email = $("#login-overlay input[name=email]");
    var $output = $(".login-err");
    loadingIcon.start($forgotPasswordBtn, {
      width: "2px",
      size: "10px",
      color: "152, 98, 167"
    });
    $.post("/ws/pwdforgotten", { email: $email.val() }, function(data) {
      var data = JSON.parse(data);
      loadingIcon.stop($forgotPasswordBtn);
      print2console("forgotPassword: Status: " + data.status);
      showTimedErr($email, $output, 5500, data.msg);
    });
    return true;
  };
  var showTimedErr = function(target, output, delay, msg) {
    target.addClass("errBorder");
    output
      .text(msg)
      .show()
      .delay(delay)
      .fadeOut(500, function() {
        $(this).text();
        target.removeClass("errBorder");
      });
  };
  var setGenderPreselection = function(preset) {
    switch (preset) {
      case "":
      case "0":
        $("#ownGender")
          .val(2)
          .trigger("change");
        $("#gender")
          .val(1)
          .trigger("change");
        ipxRegAPI.setParam("ownGender", $("#ownGender").val());
        ipxRegAPI.setParam("gender", $("#gender").val());
        break;
      case "1":
        $("#ownGender")
          .val(1)
          .trigger("change");
        $("#gender")
          .val(2)
          .trigger("change");
        ipxRegAPI.setParam("ownGender", 1);
        ipxRegAPI.setParam("gender", 2);
        break;
      case "2":
        $("#ownGender")
          .val(1)
          .trigger("change");
        $("#gender")
          .val(1)
          .trigger("change");
        ipxRegAPI.setParam("ownGender", 1);
        ipxRegAPI.setParam("gender", 1);
        break;
      case "3":
        $("#ownGender")
          .val(2)
          .trigger("change");
        $("#gender")
          .val(2)
          .trigger("change");
        ipxRegAPI.setParam("ownGender", 2);
        ipxRegAPI.setParam("gender", 2);
        break;
    }
  };
  var switchGender = function(ownGenderVal) {
    var $genderSelect = $("#gender");
    var genderVal = ownGenderVal == 2 ? 1 : 2;
    ownGenderVal = ownGenderVal || false;
    if (!ownGenderVal)
      return print2console("switchGender: Missing ownGender value param");
    $genderSelect.val(genderVal).trigger("change");
  };
  var setTabIndex = function(elem, index) {
    elem.attr("tabindex", index);
  };
  var setPageTracking = function(regType, nextStep, totalSteps) {
    var trEvent;
    regType = regType || "flexReg";
    nextStep = nextStep || false;
    totalSteps = totalSteps || false;
    switch (regType) {
      case "lp":
        trEvent = { event: "lp" };
        break;
      case "flexReg":
        trEvent = {
          event: "regStep" + nextStep,
          step: nextStep,
          total: totalSteps
        };
    }
    ipxRegAPI.tracking(trEvent);
  };
  var addRegAPIHooks = function() {
    ipxRegAPI.initDone = true;
  };
  var initRegAPI = function() {
    var debugMode = window.location.search.indexOf("xdebug") != -1;
    if (typeof ipxRegAPI != "undefined") {
      if (initIpxRegAPI(debugMode)) {
        addRegAPIHooks();
        if (idReg) ipxRegAPI.setVar("regid", idReg);
      } else print2console("initRegAPI: Failed");
    } else print2console("checkRegAPIExists: Failed");
  };
  var loadExternalPlugins = function() {
    $("#ownGender").dropdown();
    $("#gender").dropdown();
    if (!$.support.transition) $.fn.transition = $.fn.animate;
    (function() {
      if (checkAndSetSlider()) {
        var $sliderEle =
          $(window).width() >= 1000
            ? $("#desktop-topstage-slider")
            : $("#mobile-topstage-slider");
        $sliderEle.owlCarousel({
          animateOut: "fadeOut",
          lazyLoad: true,
          lazyLoadEager: 1,
          loop: false,
          rewind: true,
          dots: false,
          margin: 0,
          mouseDrag: false,
          autoplay: true,
          touchDrag: false,
          autoplayHoverPause: false,
          items: 1,
          smartSpeed: 800,
          autoplayTimeout: 5000,
          responsiveRefreshRate: 50
        });
      }
      $("#hotfacts .owl-carousel, #testimonials .owl-carousel").owlCarousel({
        nav: false,
        dots: true,
        touchDrag: true,
        autoplay: true,
        autoplayHoverPause: true,
        smartSpeed: 600,
        autoplayTimeout: 4000,
        mouseDrag: false,
        responsive: {
          0: { items: 1, margin: 30, loop: true },
          695: { items: 2, margin: 30, loop: true },
          1000: { items: 3, margin: 60, loop: false }
        }
      });
      $("#facts .owl-carousel").owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        touchDrag: true,
        autoplay: true,
        autoplayHoverPause: true,
        smartSpeed: 1000,
        autoplayTimeout: 5000,
        mouseDrag: false,
        responsive: {
          0: { items: 1, margin: 30, loop: true },
          695: { items: 2, margin: 30, loop: true },
          1000: { items: 4, margin: 60, loop: false }
        }
      });
    })();
  };
  var setEventHandlers = function() {
    (function() {
      var $loginForm = $("#login-overlay form");
      $loginForm.on("submit", function(e) {
        e.preventDefault();
        requestLogin();
      });
    })();
    (function() {
      var $forgotPasswordBtn = $("#forgot-pw-btn");
      $forgotPasswordBtn.on("click", function(e) {
        e.preventDefault();
        forgotPassword();
      });
    })();
    (function() {
      var $email = $("input[name=email]");
      $email.on("blur", function() {
        $email.val($.trim($email.val()));
      });
    })();
    (function() {
      var $openLoginBtn = $(".open-overlay");
      $(".open-overlay")
        .off("click touchstart")
        .on("click touchstart", function(e) {
          var overlayID = $(this).attr("data-overlay");
          var $overlay = $("#" + overlayID);
          var $box = $overlay.find(".box");
          e.preventDefault();
          if (isAnimating) return false;
          loginBoxShow($overlay, $box);
          $box
            .find(".close-overlay")
            .off("click touchstart")
            .on("click touchstart", function(e) {
              e.preventDefault();
              loginBoxhide($overlay, $box);
            });
        });
    })();
    (function() {
      var $ownGender = $("#ownGender");
      $ownGender.on("change", function() {
        switchGender($(this).val());
      });
    })();
    (function() {
      var $stage = $("#stage");
      var $regBox = $("#regbox");
      var $scrollWindow = $("#main");
      var $stickyHeader = $("#scrolling-sticky-top");
      var $focusRegBtns = $(".focus-reg");
      var scrollXPos = $scrollWindow.scrollTop();
      var trigger = $(window).height();
      $scrollWindow.on("scroll", function() {
        scrollXPos = $scrollWindow.scrollTop();
        if ($stage.is(".focus")) $stage.removeClass("focus");
        if (scrollXPos >= trigger)
          if ($stickyHeader.is(":hidden")) $stickyHeader.slideDown("fast");
        if (scrollXPos < trigger)
          if ($stickyHeader.is(":visible")) $stickyHeader.slideUp("fast");
      });
      $stickyHeader.on("click", function(event) {
        event.preventDefault();
        scrollTo($regBox.offset().top, $stage, "focus");
      });
    })();
    (function() {
      var $body = $("body");
      var $regbox = $("#regbox");
      var $regForm = $("#reg-form");
      var $regSubmit = $("#reg-submit");
      $regSubmit.on("click", function(e) {
        e.preventDefault();
        var isRegMinified = $(".fields").height() === 0 ? true : false;
        if (isRegMinified) {
          if ($body.hasClass("min-reg") && isDesktop()) {
            $body.removeClass("min-reg");
          } else if ($body.hasClass("min-reg-mob") && !isDesktop()) {
            $body.removeClass("min-reg-mob");
          }
          if (!isDesktop()) scrollTo($regbox.offset().top);
        } else {
          loadingIcon.start($regSubmit);
          $regForm.submit();
        }
      });
    })();
    (function() {
      var $window = $(window);
      $window.on("unload", function() {
        $window.scrollTop(0);
      });
      window.onunload = function() {
        $window.scrollTop(0);
      };
      if ("scrollRestoration" in history) {
        history.scrollRestoration = "manual";
      }
    })();
  };
  return {
    init: function() {
      initRegAPI();
      loadExternalPlugins();
      isDesktop();
      setPageTracking("lp");
      setTabIndex($("#ownGender, #gender"), 0);
      setBelowFoldContentOrder();
      mobileFullscreenMode();
      setCustomTestimonialImgs();
      setGenderPreselection(genderSelection);
      setEventHandlers();
    },
    render: function() {}
  };
})();
